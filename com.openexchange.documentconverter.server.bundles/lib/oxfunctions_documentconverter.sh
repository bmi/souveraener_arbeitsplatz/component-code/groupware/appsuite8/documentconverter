#
#  @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
#  @license AGPL-3.0
#
#  This code is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
#
#  Any use of the work other than as authorized under this license or copyright law is prohibited.
#

# debian postinst is going to fail when not set'ting +e
set +e

ox_stop_daemon_documentconverter() {
    local name="$1"
    local nonox="$2"
    test -z "$name" && die "ox_stop_daemon: missing name argument (arg 1)"
    ox_system_type
    local type=$?

    if [ ! -f /var/run/${name}.pid ]; then
        return 0
    fi
    read PID < /var/run/${name}.pid
    test -z "$PID" && { echo "No process in pidfile '/var/run/${name}.pid' found running; none killed."; return 1; }
    if [ -z "$nonox" ]; then
        ps $PID > /dev/null && /opt/open-xchange/sbin/shutdown -w > /dev/null 2>&1
    fi
    ps $PID > /dev/null && kill -QUIT $PID
    # sleeping for 2s before calling 'kill -TERM' 
    sleep 3
    ps $PID > /dev/null && kill -TERM $PID
    rm -f /var/run/${name}.pid
}
