#!/bin/bash -x

#
#  @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
#  @license AGPL-3.0
#
#  This code is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
#
#  Any use of the work other than as authorized under this license or copyright law is prohibited.
#

if [ "$(id -un)" != "open-xchange" ]; then
    echo "The Open-Xchange backend start script must only be run as user open-xchange. Executing this script as root breaks the installation."
    exit 1
fi

SERVICE_NAME=
OXFUNCTIONS=@oxfunctions@
OXPREFIX=/opt/open-xchange/${SERVICE_NAME}
OXFUNCTIONS_DC=${OXPREFIX}/lib/oxfunctions_documentconverter.sh
OXCONFIG=${OXPREFIX}/etc/process-conf.sh
CONSOLELOG_FILE=@consolelogfile@
OSGI_JAR=/opt/open-xchange/bundles/org.eclipse.osgi_3.18.400.v20230509-2241.jar

test -f $OXFUNCTIONS || {
    echo "missing common shell functions file"
    exit 1
}

. $OXFUNCTIONS

test -f $OXFUNCTIONS_DC || {
    echo "missing DocumentConverter shell functions file"
    exit 1
}

. $OXFUNCTIONS_DC

test -f $OXCONFIG && . $OXCONFIG

ox_kill_readerengine_instances

ox_set_JAVA_BIN

ox_update_cloud_config_ini @configini@ @configini@.template @bundle.d@

if [ -n "$DC_JVM_HEAP_SIZE_MB" ]; then
    JAVA_XTRAOPTS="${JAVA_XTRAOPTS} -Xms${DC_JVM_HEAP_SIZE_MB}m -Xmx${DC_JVM_HEAP_SIZE_MB}m"
elif [ -n "$DC_JVM_HEAP_MIN_RAM_PERCENTAGE" ] && [ -n "$DC_JVM_HEAP_MAX_RAM_PERCENTAGE" ]; then
    JAVA_XTRAOPTS="${JAVA_XTRAOPTS} -XX:MinRAMPercentage=${DC_JVM_HEAP_MIN_RAM_PERCENTAGE} -XX:MaxRAMPercentage=${DC_JVM_HEAP_MAX_RAM_PERCENTAGE}"
else
    JAVA_XTRAOPTS="${JAVA_XTRAOPTS} -Xmx1024m"
fi

JAVA_XTRAOPTS="${JAVA_XTRAOPTS} -XshowSettings:vm"

# Remove MaxPermSize to avoid warning with Java 8.
# TODO remove this code and MaxPermSize option from JAVA_XTRAOPTS once Java 8 is minimum requirement.
JAVA_VERSION=$($JAVA_BIN -version 2>&1 | awk '/version/ {print $3}' | sed -n -E 's/"//gp')
JAVA_MAJOR=$(echo $JAVA_VERSION | sed -n -E 's/^"?([0-9]+).*"?/\1/gp')
JAVA_MINOR=$(echo $JAVA_VERSION | sed -n -E 's/^"?[0-9]+\.([0-9]+).*"?/\1/gp')
JAVA_PATCH=$(echo $JAVA_VERSION | sed -n -E 's/^"?[0-9]+\.[0-9]+\.([0-9]+).*"?/\1/gp')
if [ 1 -eq "$JAVA_MAJOR" -a 7 -lt "$JAVA_MINOR" ]; then
    JAVA_XTRAOPTS=$(echo "$JAVA_XTRAOPTS" | sed -e 's/-XX:MaxPermSize \?= \?[^ ]\+//')
fi
if [ 1 -eq "$JAVA_MAJOR" -a 8 -lt "$JAVA_MINOR" ]; then
    JAVA_XTRAOPTS=$(echo "$JAVA_XTRAOPTS" | sed -e 's/-XX:+UseParNewGC//')
fi

JAVA_OPTS="${JAVA_XTRAOPTS} \
-Djava.awt.headless=true \
-Dopenexchange.propdir=/opt/open-xchange/etc \
-Dopenexchange.propdir2=/opt/open-xchange/${SERVICE_NAME}/etc \
--add-opens=java.base/java.util=ALL-UNNAMED \
--add-opens=java.base/java.util.concurrent=ALL-UNNAMED \
--add-opens=java.management/javax.management.openmbean=ALL-UNNAMED \
--add-opens=java.base/jdk.internal.misc=ALL-UNNAMED \
--add-opens=java.base/java.lang=ALL-UNNAMED \
--add-opens=java.management/javax.management=ALL-UNNAMED \
--add-opens=java.base/java.nio.charset=ALL-UNNAMED \
--add-opens=java.base/java.io=ALL-UNNAMED \
--add-opens=java.base/sun.util.calendar=ALL-UNNAMED \
--add-modules=jdk.unsupported"

EXTRA_CONFIGDIR=/opt/open-xchange/${SERVICE_NAME}/etc2

if [ -d "${EXTRA_CONFIGDIR}" ]; then
    JAVA_OPTS="${JAVA_OPTS} \
    -Dopenexchange.propdir3=${EXTRA_CONFIGDIR}"
fi

CLASSPATH=""

if [ -n "$CLASSPATH" ]; then
    CLASSPATH="-classpath $CLASSPATH"
fi

ox_save_backup $CONSOLELOG_FILE

umask 066

if [ "${CONSOLELOG}" = "/dev/tty" ];  then
    exec ${JAVA_BIN} ${JAVA_OPTS} ${CLASSPATH} -jar ${OSGI_JAR} -configuration file:/opt/open-xchange/osgi 2>&1
else
    if [ -n "${CONSOLELOG}" ]; then
        CONSOLELOG_FILE="${CONSOLELOG}"
    fi

    exec ${JAVA_BIN} ${JAVA_OPTS} ${CLASSPATH} -jar ${OSGI_JAR} -configuration file:/opt/open-xchange/osgi >> ${CONSOLELOG_FILE} 2>&1
fi
