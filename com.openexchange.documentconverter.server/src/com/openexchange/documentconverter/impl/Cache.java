/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.math.NumberUtils;
import com.google.common.base.Throwables;
import com.openexchange.documentconverter.DocumentConverterUtil;
import com.openexchange.documentconverter.LogData;
import com.openexchange.documentconverter.MutableWrapper;
import com.openexchange.documentconverter.NonNull;
import com.openexchange.documentconverter.Nullable;
import com.openexchange.documentconverter.impl.api.CacheDescriptor;
import com.openexchange.documentconverter.impl.api.CacheHashFileMapper;
import com.openexchange.documentconverter.impl.api.ICacheEntry;
import com.openexchange.documentconverter.impl.api.ICacheHashFileMapper;
import com.openexchange.documentconverter.impl.api.IdLocker;
import com.openexchange.documentconverter.impl.api.MutableLong;

/**
 * {@link Cache}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public class Cache  {

    final protected static File SYSTEM_TEMP_DIR = FileUtils.getTempDirectory();

    final protected static String CACHE_LOG_PREFIX = "DC cache ";

    final protected static int CACHE_DEFAULT_COLLECTION_CAPACITY = 262144;

    final protected static long CACHE_CLEANUP_PERIOD_MILLIS = 60000;

    final protected static long CACHE_LOG_PERIOD_MILLIS = 15000;

    final protected static long CACHE_CLEAR_CHUNK_ENTRY_COUNT = 100000;

    final protected static long CACHESERVICE_MAX_LOCAL_ENTRY_COUNT = 10000;

    // 256 MiB min. free size in case of usage of CacheService for local cache
    final protected static long CACHESERVICE_MIN_LOCAL_FREE_SIZE = 268435456;

    final protected static long CACHESERVICE_LOCAL_ENTRY_TIMEOUT_MILLIS = 10 * 60 * 1000;

    final protected static long CACHE_INFO_TIMEOUT_MILLIS = 15000;

    final protected static long BLOCK_SIZE_DEFAULT = 4096;

    final protected static Pattern BLOCK_SIZE_PATTERN = Pattern.compile("^\\s*([0-9]+)");

    final protected static String STR_MIB = "MiB";

    final protected static String STR_SECONDS = "s";

    final protected static String STR_UNLIMITED = "unlimited";

    /**
     * {@link OrderedHashContainer}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v7.10.5
     */
    private static class OrderedHashContainer {

        /**
         * Initializes a new {@link OrderedHashContainer}.
         */
        public OrderedHashContainer() {
            super();
        }

        /**
         * @return
         */
        public long size() {
            synchronized (this) {
                return m_orderedMap.size();
            }
        }

        /**
         *
         */
        public void clear() {
            synchronized (this) {
                m_orderedMap.clear();
            }
        }

        /**
         * @return
         */
        Iterator<CacheEntryReference> iterator() {
            synchronized (this) {
                return m_orderedMap.keySet().iterator();
            }
        }

        /**
         * @param hash
         * @return
         */
        boolean contains(final String hash) {
            if (null != hash) {
                synchronized (this) {
                    return m_orderedMap.containsKey(new CacheEntryReference(hash));
                }
            }

            return false;
        }

        /**
         * @return
         */
        CacheEntryReference get(final String hash) {
            if (null != hash) {
                synchronized (this) {
                    return m_orderedMap.get(new CacheEntryReference(hash));
                }
            }

            return null;
        }

        CacheEntryReference getFirstEntry() {
            synchronized (this) {
                final Iterator<CacheEntryReference> iter = m_orderedMap.keySet().iterator();

                return ((null != iter) && iter.hasNext()) ? iter.next() : null;
            }
        }

        /**
         * @return
         */
        void add(final CacheEntryReference cacheEntryReference) {
            if (null != cacheEntryReference) {
                synchronized (this) {
                    m_orderedMap.put(cacheEntryReference, cacheEntryReference);
                }
            }
        }

        /**
         * @return
         */
        CacheEntryReference remove(final String hash) {
            if (null != hash) {
                synchronized (this) {
                    return m_orderedMap.remove(new CacheEntryReference(hash));
                }
            }

            return null;
        }

        // - Members -----------------------------------------------------------

        private LinkedHashMap<CacheEntryReference, CacheEntryReference> m_orderedMap = new LinkedHashMap<>(CACHE_DEFAULT_COLLECTION_CAPACITY, 0.99F);
    }

    /**
     * {@link CacheWorker}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v7.10.2
     */
    private class CacheWorker extends Thread {

        /**
         * Initializes a new {@link CacheWorker}.
         *
         * @param cache
         * @param remoteUrls
         */
        public CacheWorker() {
            super("DC Cache worker");

            start();
        }

        /*
         * (non-Javadoc)
         *
         * @see java.lang.Thread#run()
         */
        @Override
        public void run() {
            final boolean trace = ServerManager.isLogTrace();

            if (trace) {
                ServerManager.logTrace("DC cache worker started");
            }

            // read persistent cache entries and fill runtime cache
            implFillRuntimeCache();

            // start the cache worker loop
            implRunWorker();

            // worker thread inished
            if (trace) {
                ServerManager.logTrace("DC cache worker finished");
            }
        }

        /**
         *
         */
        protected void shutdown() {
            if (m_cacheWorkerRunning.compareAndSet(true, false)) {
                if (ServerManager.isLogTrace()) {
                    ServerManager.logTrace("DC cache worker started finishing");
                }

                interrupt();
            }
        }

        // - Implementation ---------------------------------------------------------

        private void implFillRuntimeCache() {
            // read persistent cache entries and fill runtime cache
            final Runtime runtime = Runtime.getRuntime();

            implGC();

            final long VMMemMaxAvailableBytes = runtime.maxMemory();
            final long VMMemUsedBeforeBytes = runtime.totalMemory() - runtime.freeMemory();

            try {
                final long startFillMillis = System.currentTimeMillis();
                final boolean trace = ServerManager.isLogTrace();

                // read all available persistent entries in a smart way
                if (trace) {
                    ServerManager.logTrace(implGetCacheStrBuilder("worker finding, validating and adding local persistent entries to runtime cache...").toString());
                }

                // read valid CacheHashEntries into the runtime cache
                implReadCacheEntryReferences();

                implGC();

                final long fillDurationMillis = System.currentTimeMillis() - startFillMillis;
                final long fillDurationSeconds = fillDurationMillis / 1000;
                final long addedEntryCount = getLocalEntryCount();
                final LogData[] fillLogData = new LogData[] {
                    new LogData("local added entry count", Long.toString(addedEntryCount)),
                    new LogData("local add duration", fillDurationSeconds + "s"),
                    new LogData("local add throughput", implGetThroughputPerSecond(addedEntryCount, fillDurationMillis) + "/s")
                };

                // log initial cache overview after adding all entries
                if (trace) {
                    ServerManager.logTrace("DC cache worker added valid entries to local runtime cache", fillLogData);
                }

                // log runtime memory metrics after adding all entries
                final long VMMemCurAvailableBytes = runtime.totalMemory();
                final long VMMemUsedAfterBytes = VMMemCurAvailableBytes - runtime.freeMemory();

                ServerManager.logInfo("DC runtime memory overview", new LogData[] {
                    new LogData("max available", implGetMiBSize(VMMemMaxAvailableBytes) + STR_MIB),
                    new LogData("cur available", implGetMiBSize(VMMemCurAvailableBytes) + STR_MIB),
                    new LogData("initial use", implGetMiBSize(VMMemUsedBeforeBytes) + STR_MIB),
                    new LogData("current use", implGetMiBSize(VMMemUsedAfterBytes) + STR_MIB)
                });

                implLogCacheGeometry("DC cache local memory overview", fillDurationMillis, false);

                cleanup();
            } catch (Exception e) {
                ServerManager.logExcp(e);
            } finally {
                // enable cache
                enable();
            }
        }

        /**
         *
         */
        private void implGC() {
            final Runtime runtime = Runtime.getRuntime();

            runtime.runFinalization();
            runtime.gc();
        }

        /**
         *
         */
        private void implReadCacheEntryReferences() {
            final ICacheHashFileMapper hashFileMapper = getHashFileMapper();

            if (null == hashFileMapper) {
                return;
            }

            // if cacheDir is set, it has already been checked, that it is a readable directory
            final long curTimeMillis = System.currentTimeMillis();
            final long timeoutMillis = m_localCacheEntryTimeoutMillis;
            final File cacheRootDir = hashFileMapper.getRootDir();
            final boolean trace = ServerManager.isLogTrace();
            CacheEntryReference[] cacheEntryReferenceArray = null;

            {
                // use own scope around foundFileList to destroy intermediate cacheEntryReference
                // list immediately after obtaining the cacheEntryReferenceArray from it
                final List<CacheEntryReference> cacheEntryReferenceList = new ArrayList<>(CACHE_DEFAULT_COLLECTION_CAPACITY);
                final MutableLong foundEntryCount = new MutableLong(0);

                try {
                    Files.walkFileTree(cacheRootDir.toPath(), EnumSet.of(FileVisitOption.FOLLOW_LINKS), m_cacheHashFileMapper.getDepth() + 1, new SimpleFileVisitor<Path>() {
                        @Override
                        public FileVisitResult visitFile(Path curPath, BasicFileAttributes curPathAttr) throws IOException {
                            final FileVisitResult ret = super.visitFile(curPath, curPathAttr);

                            if (curPathAttr.isDirectory()) {
                                final String curDirectoryName = curPath.getFileName().toString();

                                if (CacheEntry.isValid(hashFileMapper, curDirectoryName)) {
                                    final CacheEntryReference curCacheEntryReference = new CacheEntryReference(
                                        hashFileMapper, curDirectoryName, curPathAttr.lastModifiedTime().toMillis());

                                    // add entry if timestamp is valid, else remove persistent entry completely
                                    if (curCacheEntryReference.isValidTimestamp(curTimeMillis, timeoutMillis)) {
                                        cacheEntryReferenceList.add(curCacheEntryReference);
                                    } else {
                                        curCacheEntryReference.clear(hashFileMapper);
                                    }

                                    if (trace && ((foundEntryCount.incrementAndGet() % 100000) == 0)) {
                                        ServerManager.logTrace(implGetCacheStrBuilder("found ").
                                            append(foundEntryCount.get()).
                                            append(" entries to add to cache. Continuing search...").toString());
                                    }
                                }
                            }

                            return ret;
                        }
                    });
                } catch (IOException e) {
                    ServerManager.logExcp(new Exception("DC cache worker excepted while reading local cache entries: " + Throwables.getRootCause(e)));
                }

                cacheEntryReferenceArray = cacheEntryReferenceList.toArray(new CacheEntryReference[cacheEntryReferenceList.size()]);
            }

            // log and return if no entries were foundmaxLocalCacheSizeCleanupStart
            if (ArrayUtils.isEmpty(cacheEntryReferenceArray)) {
                if (trace) {
                    ServerManager.logTrace(implGetCacheStrBuilder("did not find any local entries to add to cache: ").append(hashFileMapper.getRootDir().getAbsolutePath()).toString());
                }

                return;
            }

            // process each found entry by checking for valid entry and adding to cache container
            final int foundCount = cacheEntryReferenceArray.length;

            if (trace) {
                ServerManager.logTrace(implGetCacheStrBuilder("found ").append(foundCount).append(" local entries. Sorting...").toString());
            }

            // sort found cacheEntryReferenceArray in ascending order (based on timestamp)
            Arrays.sort(cacheEntryReferenceArray);

            for (int processed = 0, startPos = (((m_maxLocalCacheCount > 0) && (m_maxLocalCacheCount < foundCount)) ? (int) (foundCount - m_maxLocalCacheCount) : 0); processed < foundCount;) {
                final CacheEntryReference curCacheEntryReference = cacheEntryReferenceArray[processed];

                if (processed >= startPos) {
                    implAddToCache(curCacheEntryReference);
                } else {
                    // clear entry in case of:
                    // - outside of number of m_maxLocalCacheCount
                    // - persistent entry invalid
                    curCacheEntryReference.clear(hashFileMapper);
                }

                ++processed;

                // finally increment processed count and log every 100.000 processed entries
                if (trace && ((processed % 100000) == 0)) {
                    ServerManager.logTrace(implGetCacheStrBuilder("processed ").
                        append(processed).append(" / ").append(foundCount).
                        append(" local entries (").
                        append(String.format ("%.2f", processed * 100.0 / foundCount)).
                        append("%).").toString());
                }
            }
        }

        /**
         *
         */
        private void implRunWorker() {
            long lastCleanupTimestampMillis = System.currentTimeMillis();

            while (m_cacheWorkerRunning.get()) {
                // cleanup cache every CACHE_CLEANUP_PERIOD_MILLIS
                long curTimestampMillis = System.currentTimeMillis();

                if (isEnabled()) {
                    if ((curTimestampMillis - lastCleanupTimestampMillis) >= CACHE_CLEANUP_PERIOD_MILLIS) {
                        cleanup();
                        lastCleanupTimestampMillis = curTimestampMillis = System.currentTimeMillis();
                    }
                } else {
                    lastCleanupTimestampMillis = curTimestampMillis;
                }

                // sleep thread until next cache cleanup should happen

                ServerManager.sleepThread(Math.max(1L, (lastCleanupTimestampMillis + CACHE_CLEANUP_PERIOD_MILLIS) - curTimestampMillis), false);
            }
        }

        // - Members ---------------------------------------------------------------

        @NonNull final private AtomicBoolean m_cacheWorkerRunning = new AtomicBoolean(true);
    }

    /**
     * Initializes a new {@link Cache}.
     *
     * @param cacheDescriptor The cache descriptor
     * @param serverManager The connected manager
     */
    public Cache(@NonNull ServerManager serverManager, @NonNull CacheDescriptor cacheDescriptor) {
        super();

        File cacheDir = null;

        // initialize basic cache
        if ((null != cacheDescriptor.cacheDirectory) && (cacheDescriptor.cacheDirectory.length() > 0)) {
            cacheDir = new File(cacheDescriptor.cacheDirectory);
        }

        // try to create/use fallback directory in case of an error with the configured cache directory
        if ((null == cacheDir) || !ServerManager.validateOrMkdir(cacheDir)) {
            for (int i = 1; i <= 100; ++i) {
                cacheDir = new File(SYSTEM_TEMP_DIR, "oxdc.cache" + i);

                if (ServerManager.validateOrMkdir(cacheDir)) {
                    m_usingFallbackCacheDir = true;
                    ServerManager.logWarn(implGetCacheStrBuilder("has no valid writeable local cache directory to work on: ").append(cacheDir).append(". Using cache fallback directory instead: ").append(cacheDir.getAbsolutePath()).toString());

                    break;
                }

                cacheDir = null;
            }
        }

        if (null == cacheDir) {
            cacheDir = SYSTEM_TEMP_DIR;
            ServerManager.logError(implGetCacheStrBuilder("has no valid writeable local cache directory to work on! Using default temp. directory as last effort: ").append(cacheDir.getAbsolutePath()).toString());
        }

        implCalculateBlockSizes(cacheDir);

        m_serverManager = serverManager;
        m_cacheHashFileMapper = new CacheHashFileMapper(cacheDir, cacheDescriptor.cacheDirectoryDepth);

        m_localVolumeSize = cacheDir.getTotalSpace();
        m_curLocalFreeSize = implGetFreeVolumeSize(m_cacheHashFileMapper.getRootDir());

        final String[] remoteCacheUrls = cacheDescriptor.remoteCacheUrls;
        CacheServiceClient cacheServiceClient = null;

        if (ArrayUtils.isNotEmpty(remoteCacheUrls)) {
            final String remoteCacheURLStr = remoteCacheUrls[0];
            final ServerConfig serverConfig = serverManager.getServerConfig();

            try {
                cacheServiceClient = new CacheServiceClient(cacheDescriptor);

                ServerManager.logInfo(implGetCacheStrBuilder("is configured to use CacheService server at ").
                    append(remoteCacheURLStr).toString());
            } catch (Exception e) {
                cacheServiceClient = null;

                ServerManager.logInfo(implGetCacheStrBuilder("encountered errors when connecting to CacheService server at ").
                    append(remoteCacheURLStr).
                    append(Throwables.getRootCause(e)).toString());
            }
        }

        if (null != (m_cacheServiceClient = cacheServiceClient)) {
            // we don't want to keep just created cache entries in case of CacheService
            // usage for a long time, but instead keep them just for a short period
            // of time (5 minutes) in local cache for possible further requests coming in short after
            m_maxLocalCacheCount = CACHESERVICE_MAX_LOCAL_ENTRY_COUNT;
            m_maxRemoteCacheCount = cacheDescriptor.maximumEntryCount;
            m_maxLocalCacheSize = -1;
            m_maxRemoteCacheSize = cacheDescriptor.maximumPersistSize;
            m_minLocalFreeSize = CACHESERVICE_MIN_LOCAL_FREE_SIZE;
            m_localCacheEntryTimeoutMillis = CACHESERVICE_LOCAL_ENTRY_TIMEOUT_MILLIS;
            m_remoteCacheEntryTimeoutMillis = cacheDescriptor.timeoutMillis;
        } else {
            // using local cache with given cache constraints
            m_maxLocalCacheCount = cacheDescriptor.maximumEntryCount;
            m_maxRemoteCacheCount = 0;
            m_maxLocalCacheSize = cacheDescriptor.maximumPersistSize;
            m_maxRemoteCacheSize = 0;
            m_minLocalFreeSize = cacheDescriptor.minimumFreeVolumeSize;
            m_localCacheEntryTimeoutMillis = cacheDescriptor.timeoutMillis;
            m_remoteCacheEntryTimeoutMillis = 0;
        }

        m_cacheThresholdUpperPercentage = cacheDescriptor.cleanupThresholdUpperPercentage;
        m_cacheThresholdLowerPercentage = cacheDescriptor.cleanupThresholdLowerPercentage;

        m_cacheWorker = new CacheWorker();
    }

    /**
     * @return
     */
    public String getUID() {
        return m_cacheUID;
    }

   /**
    * @return
    */
   public boolean isCacheServerConfigured() {
       return (null != m_cacheServiceClient);
   }

    /**
     * @return
     */
    public boolean isCacheServerEnabled() {
        return ((null != m_cacheServiceClient) && m_cacheServiceClient.isCacheServerEnabled());
    }

    /**
     * @return
     */
    public boolean isCacheServerAvailable() {
        return ((null != m_cacheServiceClient) && m_cacheServiceClient.isCacheServerAvailable());
    }

    /**
     * @return
     */
    public boolean isCacheServerRecoverPeriodNotReached() {
        return ((null != m_cacheServiceClient) && m_cacheServiceClient.isCacheServerRecoverPeriodNotReached());
    }

    /**
     * @return
     */
    public long getLocalEntryCount() {
        return m_orderedContainer.size();
    }

    /**
     * @return
     */
    public long getRemoteEntryCount() {
        return isCacheServerEnabled() ?
            implGetCacheInfo().getKeyCount() :
                -1;
    }

    /**
     * @return
     */
    public long getLocalPersistentSize() {
        return m_curLocalCacheSize;
    }

    /**
     * @return
     */
    public synchronized long getRemotePersistentSize() {
        return isCacheServerEnabled() ?
            implGetCacheInfo().getGroupLength() :
                -1;
    }

    /**
     * @return
     */
    public synchronized long getLocalFreeVolumeSize() {
        return m_curLocalFreeSize;
    }

    /**
     * @return
     */
    public long getLocalOldestEntrySeconds() {
        final CacheEntryReference oldestEntry = m_orderedContainer.getFirstEntry();

        if (null != oldestEntry) {
            return (System.currentTimeMillis() - oldestEntry.getLastAccessTimeMillis()) / 1000L;
        }

        return 0;
    }

    /**
     * @return
     */
    public long getRemoteOldestEntrySeconds() {
        return (isCacheServerEnabled()) ?
            (implGetCacheInfo().getMaxKeyAgeMillis() / 1000L) :
                -1;
    }

    /**
     * @param count
     * @return
     */
    public synchronized long clear(final long count) {
        final long clearStartTimeMillis = System.currentTimeMillis();
        final long initialCount = getLocalEntryCount();
        final boolean trace = ServerManager.isLogTrace();
        long ret = 0;

        if (initialCount > 0) {
            final long countToRemove = (count < 0) ? initialCount : Math.min(count, initialCount);

            if (trace) {
                ServerManager.logTrace(implGetCacheStrBuilder("is clearing local cache...: ").append(countToRemove).append(" / ").append(initialCount).toString());
            }

            final long maxCountToRemoveInChunk = Math.min(countToRemove, CACHE_CLEAR_CHUNK_ENTRY_COUNT);
            final int chunkCount = (int) (countToRemove / CACHE_CLEAR_CHUNK_ENTRY_COUNT + (((countToRemove % CACHE_CLEAR_CHUNK_ENTRY_COUNT) > 0) ? 1 : 0));

            long lastChunkStartTimeMillis = clearStartTimeMillis;

            for (int i = 0; i < chunkCount; ++i) {
                final MutableLong curRemovedCounter = new MutableLong(0);
                final long curRemovedCount = implCleanup((curCacheEntry) -> {
                    return (curRemovedCounter.incrementAndGet() <= maxCountToRemoveInChunk);
                }, "CacheClear");

                ret += curRemovedCount;

                final long curNewTime = System.currentTimeMillis();
                final long curLeftCount = countToRemove - ret;
                final long curDurationMillis = curNewTime - lastChunkStartTimeMillis;
                final long curClearThroughput = implGetThroughputPerSecond(curRemovedCount, curDurationMillis);

                lastChunkStartTimeMillis = curNewTime;

                if (trace) {
                    ServerManager.logTrace(implGetCacheStrBuilder("cleared chunk of entries from local cache: ").append(curRemovedCount).append(", ").append(curLeftCount).append(" left to remove").append(" (clear chunk duration: ").append(curDurationMillis).append("ms").append(", clear chunk throughput: ").append(curClearThroughput).append("/s)").toString());
                }
            }
        }

        if (trace) {
            final long leftCount = getLocalEntryCount();
            final long durationMillis = System.currentTimeMillis() - clearStartTimeMillis;
            final long clearThroughput = implGetThroughputPerSecond(ret, durationMillis);

            ServerManager.logTrace(implGetCacheStrBuilder("cleared local entries: ").append(ret).append(" / ").append(initialCount).append(", ").append(leftCount).append(" left").append(" (clear duration: ").append(durationMillis).append("ms").append(", clear throughput: ").append(clearThroughput).append("/s)").toString());
        }

        return ret;
    }

    /**
     *
     */
    public synchronized void cleanup() {
        // use stack variables instead of member for faster access in loops
        final long cleanupStartTimeMillis = System.currentTimeMillis();
        final ICacheHashFileMapper hashFileMapper = getHashFileMapper();
        final long cacheEntryTimeoutMillisToUse = m_localCacheEntryTimeoutMillis;
        final long maxLocalCacheCountToUse = m_maxLocalCacheCount;
        final long minLocalFreeSizeToUse = m_minLocalFreeSize;
        final long maxLocalCacheSizeCleanupStart = (long) Math.ceil(m_maxLocalCacheSize * m_cacheThresholdUpperPercentage * 0.01);
        final long maxLocalCacheSizeToUse = (m_maxLocalCacheSize > -1) ? (long) Math.ceil(m_maxLocalCacheSize * m_cacheThresholdLowerPercentage * 0.01) : -1;
        final boolean cacheSizeLimited = (maxLocalCacheSizeToUse > -1);
        boolean entriesRemoved = false;

        m_curLocalFreeSize = implGetFreeVolumeSize(hashFileMapper.getRootDir());

        final boolean startCacheSizeBasedCleanup =
            (m_curLocalFreeSize < minLocalFreeSizeToUse) ||
            (cacheSizeLimited && (m_curLocalCacheSize >= maxLocalCacheSizeCleanupStart));


        // remove entries on a timeout basis, if timeout value is > 0
         if ((cacheEntryTimeoutMillisToUse > 0) && implCleanup((curCashEntryReference) -> {
                return (cleanupStartTimeMillis - curCashEntryReference.getLastAccessTimeMillis()) >= cacheEntryTimeoutMillisToUse;
            }, "CacheTimeout") > 0) {

            entriesRemoved = true;
        }

        // remove entries on a count basis, if count value is > -1
        if ((maxLocalCacheCountToUse > -1) && implCleanup((curCachEntry) -> {
                return (m_orderedContainer.size() > maxLocalCacheCountToUse);
            }, "CacheCount") > 0) {

            entriesRemoved = true;
        }

        // remove cache entries on a free space available or maximum cache size basis, if max. local cache size is reached
        if (startCacheSizeBasedCleanup && implCleanup((curCacheEntry) -> {
                return ((cacheSizeLimited && (m_curLocalCacheSize > maxLocalCacheSizeToUse)) || (m_curLocalFreeSize < minLocalFreeSizeToUse));
            }, "CacheSize") > 0) {

            entriesRemoved = true;
        }

        if (entriesRemoved) {
            m_curLocalFreeSize = implGetFreeVolumeSize(hashFileMapper.getRootDir());

            final long durationMillis = System.currentTimeMillis() - cleanupStartTimeMillis;

            if (entriesRemoved && m_localConstraintLimitNotReached) {
                implLogCacheGeometry("DC cache reached local constraint limit for the first time", durationMillis, true);
                m_localConstraintLimitNotReached = false;
            } else {
                final boolean cacheEnabled = (maxLocalCacheSizeToUse != 0) && (maxLocalCacheCountToUse != 0);
                implLogCacheGeometry("DC cache " + (cacheEnabled ? "enabled " : "disabled " ) + "local cache instance", durationMillis, false);
            }
        }
    }

    /**
     * @return
     */
    public ICacheHashFileMapper getHashFileMapper() {
        return m_cacheHashFileMapper;
    }

    /**
     * @param hash
     * @return
     */
    public ICacheEntry startNewEntry(String hash) {
        if (isValidHash(hash)) {
            IdLocker.lock(hash);

            try {
                // ensure, that startEntry is not called twice for one hash
                if (!implContains(hash)) {
                    synchronized (m_newEntryDeque) {
                        final Iterator<ICacheEntry> newEntryIter = m_newEntryDeque.iterator();

                        while (newEntryIter.hasNext()) {
                            if (hash.equals(newEntryIter.next().getHash())) {
                                return null;
                            }
                        }
                    }
                }

                // m_newEntryDeque doesn't contain this hash => create new one and add to queue
                final ICacheEntry newCacheEntry = CacheEntry.createAndMakePersistent(getHashFileMapper(), hash, new HashMap<>(8), new byte[] {});

                if (null != newCacheEntry) {
                    synchronized (m_newEntryDeque) {
                        m_newEntryDeque.push(newCacheEntry);
                    }

                    return newCacheEntry;
                }
            } finally {
                IdLocker.unlock(hash);
            }
        }

        return null;
    }

    /**
     * @param newCacheEntry
     */
    public void endNewEntry(ICacheEntry newCacheEntry) {
        if (null != newCacheEntry) {
            synchronized (m_newEntryDeque) {
                m_newEntryDeque.remove(newCacheEntry);
            }
        }
    }

    /**
     * @param hash
     * @param filename
     * @param excludeRemoteURLStrings
     * @return
     */
    public ICacheEntry getEntry(@NonNull final String hash, @Nullable final String filename) {
        if (isEnabled() && isValidHash(hash)) {
            final boolean trace = ServerManager.isLogTrace();

            IdLocker.lock(hash);

            try {
                // try to get from local cache first in every case
                ICacheEntry ret = implContains(hash) ? implCreateCacheEntry(hash) : null;

                if (null != ret) {
                    synchronized (m_orderedContainer) {
                        // touch accessed entry
                        final CacheEntryReference cacheEntryReference = m_orderedContainer.remove(hash);

                        if (null != cacheEntryReference) {
                            cacheEntryReference.touch(getHashFileMapper());
                            m_orderedContainer.add(cacheEntryReference);
                        }
                    }

                    if (trace) {
                        ServerManager.logTrace(implGetCacheStrBuilder("read entry from local cache: ").append(hash).toString());
                    }
                } else {
                    // try to get entry from configured CacheService, if possible
                    if (isCacheServerEnabled()) {
                        // try to get from CacheService server, using the HashFileMapper from this cache
                        // in order to download and create a new  entry within this cache directory
                        ret = m_cacheServiceClient.getCacheEntry(hash, getHashFileMapper(), filename);

                        if (trace && (null != ret)) {
                            ServerManager.logTrace(implGetCacheStrBuilder("read entry from CacheService server").toString(),
                                new LogData("hash", hash),
                                new LogData("filename", filename));
                        }
                    }

                    // add valid entry from one of the remote caches to
                    // local cache without uploading to remote cache again
                    if (null != ret) {
                        addEntry(ret, false, null);
                    }
                }

                return ret;
            } finally {
                IdLocker.unlock(hash);
            }
        }

        return null;
    }

    /**
     * @param entry
     * @param remoteTransfer
     * @param filename
     * @return
     */
    public boolean addEntry(final ICacheEntry entry, boolean remoteTransfer, @Nullable String filename) {
        if (isEnabled() && (null != entry) && entry.isValid()) {
            final String hash = entry.getHash();
            final boolean trace = ServerManager.isLogTrace();

            IdLocker.lock(hash);

            try {
                // add to local cache, if not already cached locally
                if (!implContains(hash)) {
                    implAddToCache(new CacheEntryReference(getHashFileMapper(), hash, entry.getAccessTime().getTime()));

                    if (trace) {
                        ServerManager.logTrace(implGetCacheStrBuilder("added entry to local cache: ").append(hash).toString());
                    }
                }

                // add to CacheService, if not already cached there
                if (remoteTransfer && isCacheServerEnabled() && !m_cacheServiceClient.hasCacheEntry(hash, filename)) {
                    final boolean addedToCacheService = m_cacheServiceClient.addCacheEntry(entry, filename);

                    if (trace) {
                        ServerManager.logTrace(implGetCacheStrBuilder(addedToCacheService ?
                            "added entry to CacheService server: " :
                                "not able to add entry to CacheService server: ").append(hash).toString());
                    }
                }

                return true;
            } finally {
                IdLocker.unlock(hash);
            }
        }

        return false;
    }

    /**
     * @param hash
     */
    public void removeEntry(final String hash) {
        if (isEnabled() && isValidHash(hash)) {
            IdLocker.lock(hash);

            try {
                final CacheEntryReference cacheEntryReference = m_orderedContainer.get(hash);

                if (null != cacheEntryReference) {
                    synchronized (this) {
                        final CacheEntryReference entryToRemove = m_orderedContainer.remove(hash);

                        if (null != entryToRemove) {
                            implRemovedFromCache(cacheEntryReference);
                        }
                    }
                }
            } finally {
                IdLocker.unlock(hash);
            }
        }
    }

    /**
     *
     */
    public void shutdown() {
        if (m_cacheRunning.compareAndSet(true, false)) {
            m_enabledFlag.set(false);

            if (null != m_cacheServiceClient) {
                m_cacheServiceClient.shutdown();
            }

            m_cacheWorker.shutdown();

            while (m_cacheWorker.isAlive()) {
                try {
                    m_cacheWorker.join();
                } catch (@SuppressWarnings("unused") InterruptedException e) {
                    // ok
                }
            }

            // clear all new entries that were currently created
            synchronized (m_newEntryDeque) {
                m_newEntryDeque.clear();
            }

            m_orderedContainer.clear();

            // delete cache directory, if it is the fallback one
            final File cacheDir = getHashFileMapper().getRootDir();

            if (m_usingFallbackCacheDir) {
                ServerManager.logInfo(implGetCacheStrBuilder("removing temporary local fallback cache directory: ").append(cacheDir.getAbsolutePath()).toString());

                try {
                    FileUtils.forceDelete(cacheDir);
                } catch (IOException e) {
                    ServerManager.logExcp(e);
                }
            }
        }
    }

    /**
     *
     */
    public void enable() {
        m_enabledFlag.compareAndSet(false, true);
    }

    /**
     * @return
     */
    public boolean isEnabled() {
        return m_enabledFlag.get();
    }

    /**
     * @param hash
     * @return
     */
    public static boolean isValidHash(String hash) {
        return ((null != hash) && (hash.length() > 1));
    }

    /**
     * @param directory
     * @return
     */
    public static long calculateDirectorySize(final File directory) {
        return implCalculateDirectorySize(directory, 0);
    }

    /**
     *
     */
    public @Nullable String getRemoteCacheURL() {
        return (null != m_cacheServiceClient) ? m_cacheServiceClient.getCacheServiceURL() : null;
    }

    // - Implementation --------------------------------------------------------

    /**
     * !!! Method is thread safe wrt. instance members but unsynchronized wrt. cacheEntry and needs synchronization for used hash by caller !!!
     *
     * Creates a cache entry from persistent data within a directory.
     * The properties file as well as the result content file must be
     * readable to reconstruct the cache entry. If the entry is not
     * readable, it will be removed from the cache
     *
     * @param resultDirectory The directory to read the entries' data from.
     * @param cache The cache to which the new object should be assigned to. Might be null.
     * @return The new cache entry or null in case of a failure.
     */
    protected ICacheEntry implCreateCacheEntry(@NonNull final String hash) {
        final CacheEntry cacheEntry = CacheEntry.createFrom(getHashFileMapper(), hash);
        final boolean trace = ServerManager.isLogTrace();

        // check for null or invalid cache entry
        if (null == cacheEntry) {
            if (trace) {
                ServerManager.logTrace(implGetCacheStrBuilder("could not find cache entry in local cache: ").append(hash).toString());
            }

            return null;
        } else if (cacheEntry.isValid()) {
            return cacheEntry;
        }

        // try to find reason why cache cache entry is invalid
        final HashMap<String, Object> resultProperties = cacheEntry.getResultProperties();
        String removeReason = null;

        if (null == resultProperties) {
            removeReason = "cache entry properties file could not be read locally : " + hash;
        } else {
            final byte[] resultBuffer = cacheEntry.getResultContent();
            removeReason = (null == resultBuffer) ? "cache entry directory doesn't contain a readable local result file" : ("unkown: hash=" + hash);
        }

        cacheEntry.clear();

        if (trace) {
            ServerManager.logTrace(implGetCacheStrBuilder("found requested entry locally but removes it due to: ").append(removeReason).append(" (hash: ").append(hash).append(')').toString());
        }

        return null;
    }

    /**
     * !!! Method is thread safe wrt. instance members but unsynchronized wrt. cacheEntry and needs synchronization for used hash by caller !!!
     *
     * @param cacheEntry
     * @param synchronize
     */
    protected synchronized void implAddToCache(@NonNull final CacheEntryReference cacheEntryReference) {
        // put entry at the tail of the list, making it the newest entry
        m_orderedContainer.add(cacheEntryReference);

        // update cache relevant sizes
        final long cacheEntrySize = cacheEntryReference.getPersistentSize();

        m_curLocalCacheSize += cacheEntrySize;
        m_curLocalFreeSize -= cacheEntrySize;
    }

    /**
     * !!! Method is thread safe wrt. instance members but unsynchronized wrt. cacheEntry and needs synchronization for used hash by caller !!!
     *
     * @param cacheEntry
     */
    protected synchronized void implRemovedFromCache(CacheEntryReference cacheEntryReference) {
        if (null != cacheEntryReference) {
            final long cacheEntrySize = cacheEntryReference.getPersistentSize();

            m_curLocalCacheSize -= cacheEntrySize;
            m_curLocalFreeSize += cacheEntrySize;

            cacheEntryReference.clear(getHashFileMapper());
        }
    }

    /**
     * @param hash
     * @return
     */
    protected boolean implContains(@NonNull final String hash) {
        return m_orderedContainer.contains(hash);
    }

    /**
     * @param count
     * @param durationMillis
     * @return
     */
    protected static long implGetThroughputPerSecond(final long count, final long durationMillis) {
        return (long) ((double) (1000L * count) / (Math.max(durationMillis, 1L)));
    }

    /**
     * @param logTitle The text after the prefix, no first space is needed.
     * @return
     */
    protected static StringBuilder implGetCacheStrBuilder(@NonNull final String logTitle) {
        return new StringBuilder(256).append(CACHE_LOG_PREFIX).append(logTitle);
    }

    /**
     * !!! Method needs synchronization for instance members if called!!!
     *
     * @param removalPredicate
     * @param cleanupReason
     */
    private long implCleanup(@NonNull final Predicate<CacheEntryReference> removePredicate, @NonNull final String cleanupReason) {
        final LinkedList<CacheEntryReference> addEntries = new LinkedList<>();
        final long oldEntryCount = getLocalEntryCount();
        final long oldCacheSize = getLocalPersistentSize();
        long removedEntryCount = 0;

        synchronized (m_orderedContainer) {
            for (final Iterator<CacheEntryReference> iter = m_orderedContainer.iterator(); iter.hasNext();) {
                final CacheEntryReference curCashEntryReference = iter.next();
                final String curHash = curCashEntryReference.getHash();

                // !!! don't use a waiting lock here since we're in an instance synchronized block
                if (IdLocker.lock(curHash, IdLocker.Mode.TRY_LOCK)) {
                    try {
                        if (removePredicate.test(curCashEntryReference)) {
                            iter.remove();


                            // subtract current entry data and clear persistent data
                            implRemovedFromCache(curCashEntryReference);
                            ++removedEntryCount;
                        } else {
                            // we can stop, if the current entry didn't reach its
                            // timeout value, since all following entries are newer
                            break;
                        }
                    } finally {
                        IdLocker.unlock(curHash);
                    }
                }
            }

            // add touched entries with 'no removal' flag to entry map again
            for (final CacheEntryReference curEntry : addEntries) {
                m_orderedContainer.add(curEntry);
            }
        }

        implTraceCacheRemoval(oldEntryCount, oldCacheSize, cleanupReason);

        return removedEntryCount;
    }

    /**
     * @param logText
     * @param durationMillis
     * @param isInfo
     */
    private void implLogCacheGeometry(String logText, long durationMillis, boolean isInfo) {
        if (isInfo || ServerManager.isLogTrace()) {
            final File cacheRootDir = getHashFileMapper().getRootDir();
            final boolean isCacheServerConfigured = isCacheServerConfigured();
            final boolean isCacheServerEnabled = isCacheServerEnabled();
            final String remoteCacheURL = getRemoteCacheURL();

            final LogData[] logData = ArrayUtils.addAll(new LogData[] {
                new LogData("remote cache configured", Boolean.valueOf(isCacheServerConfigured).toString()),
                new LogData("remote cache available", Boolean.valueOf(isCacheServerEnabled).toString()),
                new LogData("remote cache URL", remoteCacheURL),
                new LogData("remote cur/max cache entries", isCacheServerConfigured ? (getRemoteEntryCount() + " / " + ((-1 == m_maxRemoteCacheCount) ? STR_UNLIMITED : Long.toString(m_maxRemoteCacheCount))) : DocumentConverterUtil.STR_NOT_AVAILABLE),
                new LogData("remote cur/max cache size", isCacheServerConfigured ? (implGetMiBSize(getRemotePersistentSize()) + STR_MIB + " / " + ((-1 == m_maxRemoteCacheSize) ? STR_UNLIMITED : implGetMiBSize(m_maxRemoteCacheSize) + STR_MIB)) : DocumentConverterUtil.STR_NOT_AVAILABLE),
                new LogData("remote oldest entry", isCacheServerConfigured ? getRemoteOldestEntrySeconds() + STR_SECONDS : DocumentConverterUtil.STR_NOT_AVAILABLE),
                new LogData("local cache dir", (null != cacheRootDir) ? cacheRootDir.toString() : DocumentConverterUtil.STR_NOT_AVAILABLE),
                new LogData("local cur/max cache entries", m_orderedContainer.size() + " / " + ((-1 == m_maxLocalCacheCount) ? STR_UNLIMITED : Long.toString(m_maxLocalCacheCount))),
                new LogData("local cur/max cache size", implGetMiBSize(m_curLocalCacheSize) + STR_MIB + " / " + ((-1 == m_maxLocalCacheSize) ? STR_UNLIMITED : implGetMiBSize(m_maxLocalCacheSize) + STR_MIB)),
                new LogData("local cur/min free size", implGetMiBSize(m_curLocalFreeSize) + STR_MIB + " / " + implGetMiBSize(m_minLocalFreeSize) + STR_MIB),
                new LogData("local oldest entry", getLocalOldestEntrySeconds() + STR_SECONDS),
                new LogData("duration", Long.toString(durationMillis) + "ms")
            }, m_serverManager.getAsyncLogData());

            if (isInfo) {
                ServerManager.logInfo(logText, logData);
            } else {
                ServerManager.logTrace(logText, logData);
            }
        }
    }

    /**
     * !!! Method needs synchronization for instance members if called!!!
     *
     * @param oldEntryCount
     * @param oldCacheSize
     * @param reason
     */
    private void implTraceCacheRemoval(long oldEntryCount, long oldCacheSize, String reason) {
        if (ServerManager.isLogTrace()) {
            final long curEntryCount = m_orderedContainer.size();

            if ((oldEntryCount != curEntryCount) || (oldCacheSize != m_curLocalCacheSize)) {
                ServerManager.logTrace("DC cache needed cleanup",
                    new LogData("local cur/old CacheEntries", Long.toString(curEntryCount) + " / " + Long.toString(oldEntryCount)),
                    new LogData("local cur/old CacheSize", implGetMiBSize(m_curLocalCacheSize) + "MiB / " + implGetMiBSize(oldCacheSize) + "MiB"),
                    new LogData("local cleanup reason", ((null != reason) && reason.length() > 0) ? reason : DocumentConverterUtil.STR_NOT_AVAILABLE));
            }
        }
    }

    /**
     * @param cacheDir
     */
    private static synchronized void implCalculateBlockSizes(File cacheDir) {
        if (null != cacheDir) {
            final File testDir = new File(cacheDir, "testdirectory-" + UUID.randomUUID().toString());

            try {
                if (ServerManager.validateOrMkdir(testDir)) {
                    DIR_BLOCK_SIZE = implCalculateFileBlockSize(testDir.getAbsolutePath());

                    final File testFile = new File(testDir, "testfile.1");

                    try (FileOutputStream oStm = new FileOutputStream(testFile)) {
                        oStm.write('x');
                        oStm.flush();
                    }

                    FILE_BLOCK_SIZE = implCalculateFileBlockSize(testFile.getAbsolutePath());
                }
            } catch (IOException e) {
                ServerManager.logExcp(e);
            } finally {
                FileUtils.deleteQuietly(testDir);
            }
        }
    }

    /**curBashPath
     * @param filePath
     * @return
     */
    private static long implCalculateFileBlockSize(@NonNull final String filePath) {
        final List<String> outputList = ServerManager.executeBashCommand("du -s -L -B 1 " + filePath);

        if (outputList.size() > 0) {
            for (final String curLine : outputList) {
                final Matcher matcher = BLOCK_SIZE_PATTERN.matcher(curLine);

                if (matcher.find()) {
                    final long fileBlockSize = NumberUtils.toLong(matcher.group(1));

                    if (fileBlockSize > 0) {
                        return fileBlockSize;
                    }
                }
            }
        }

        return BLOCK_SIZE_DEFAULT;
    }

    /**
     * @param directory
     * @return
     */
    private static long implCalculateDirectorySize(File directory, final long startSize) {
        long ret = startSize;

        if ((null != directory) && directory.isDirectory()) {
            ret += DIR_BLOCK_SIZE;

            final File[] files = directory.listFiles();

            if (null != files) {
                for (final File curFile : files) {
                    if (curFile.isDirectory()) {
                        ret += implCalculateDirectorySize(curFile, ret);
                    } else {
                        final long curFileSize = curFile.length();
                        ret += (((curFileSize / FILE_BLOCK_SIZE) + (((curFileSize % FILE_BLOCK_SIZE) > 0) ? 1 : 0)) * FILE_BLOCK_SIZE);
                    }
                }
            }
        }

        return ret;
    }

    /**
     * @param filePath
     * @return
     */
    private static long implGetFreeVolumeSize(@NonNull final File file) {
        try {
            return Files.getFileStore(Paths.get(file.getCanonicalPath())).getUsableSpace();
        } catch (Exception e) {
            ServerManager.logExcp(e);
        }

        return 0;
    }

    /**
     * @param sizeBytes
     * @return The size in Mib (1024KiB granularity)
     */
    private  static long implGetMiBSize(long sizeBytes) {
        return sizeBytes / 1048576;
    }

    /**
     * @return
     */
    private CacheServiceClient.CacheInfo implGetCacheInfo() {
        synchronized (m_cacheInfoWrapper) {
            CacheServiceClient.CacheInfo lastCacheInfo = m_cacheInfoWrapper.get();

            if ((System.currentTimeMillis() - lastCacheInfo.getTimestampMillis()) > CACHE_INFO_TIMEOUT_MILLIS) {
                // update cache info object if older than CACHE_INFO_TIMEOUT_MILLIS
                final CacheServiceClient.CacheInfo currentCacheInfo = m_cacheServiceClient.getCacheInfo();

                if (null != currentCacheInfo) {
                    m_cacheInfoWrapper.set(lastCacheInfo = currentCacheInfo);
                }
            }

            return new CacheServiceClient.CacheInfo(
                lastCacheInfo.getTimestampMillis(),
                lastCacheInfo.getKeyCount(),
                lastCacheInfo.getMaxKeyAgeMillis(),
                lastCacheInfo.getGroupLength());
        }
    }

    // - Members ---------------------------------------------------------------

    protected static long DIR_BLOCK_SIZE = BLOCK_SIZE_DEFAULT;

    protected static long FILE_BLOCK_SIZE = BLOCK_SIZE_DEFAULT;

    // -------------------------------------------------------------------------

    final private String m_cacheUID = UUID.randomUUID().toString();

    final private ICacheHashFileMapper m_cacheHashFileMapper;

    final private CacheServiceClient m_cacheServiceClient;

    final private MutableWrapper<CacheServiceClient.CacheInfo> m_cacheInfoWrapper =
        new MutableWrapper<>(new CacheServiceClient.CacheInfo());

    final protected ServerManager m_serverManager;

    final protected CacheWorker m_cacheWorker;

    final protected Deque<ICacheEntry> m_newEntryDeque = new LinkedList<>();

    final protected OrderedHashContainer m_orderedContainer = new OrderedHashContainer();

    final protected long m_localCacheEntryTimeoutMillis;

    final protected long m_remoteCacheEntryTimeoutMillis;

    final protected AtomicBoolean m_cacheRunning = new AtomicBoolean(true);

    final protected AtomicBoolean m_enabledFlag = new AtomicBoolean(false);

    final protected long m_maxLocalCacheCount;

    final protected long m_maxRemoteCacheCount;

    final protected long m_maxLocalCacheSize;

    final protected long m_maxRemoteCacheSize;

    final protected long m_minLocalFreeSize;

    final protected long m_cacheThresholdUpperPercentage;

    final protected long m_cacheThresholdLowerPercentage;

    protected long m_localVolumeSize = 0;

    protected long m_curLocalCacheSize = 0;

    protected long m_curLocalFreeSize = 0;

    protected boolean m_localConstraintLimitNotReached = true;

    protected boolean m_usingFallbackCacheDir = false;
}
