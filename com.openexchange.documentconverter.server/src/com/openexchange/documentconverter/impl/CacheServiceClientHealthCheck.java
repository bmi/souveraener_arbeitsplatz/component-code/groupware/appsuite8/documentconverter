/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl;

import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import com.openexchange.documentconverter.DocumentConverterUtil;

/**
 * {@link CacheServiceClientHealthCheck}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public class CacheServiceClientHealthCheck implements HealthCheck {

    final private static String CS_CLIENT_HEALTH_CHECK = "CacheServiceClientHealthCheck";

    final private static String CS_SERVER_REMOTEURL = "remoteCacheUrl";

    final private static String CS_SERVICE_AVAILABLE = "serviceAvailable";

    /**
     * Initializes a new {@link DCHealthCheck}.
     * @param client
     */
    public CacheServiceClientHealthCheck(ServerManager serverManager) {
        super();
        m_serverManager = serverManager;
    }

    /**
     *
     */
    @Override
    public HealthCheckResponse call() {
        final Cache cache = (null != m_serverManager) ? m_serverManager.getCache() : null;
        final boolean shouldCacheServiceBeUsed = (null != cache) && cache.isCacheServerEnabled();
        final String remoteCacheURL = (null != cache) ? cache.getRemoteCacheURL() : null;

        // return UP status in every case
        return HealthCheckResponse.builder().
            name(CS_CLIENT_HEALTH_CHECK).
            withData(CS_SERVER_REMOTEURL, (null != remoteCacheURL) ? remoteCacheURL : DocumentConverterUtil.STR_NOT_AVAILABLE).
            withData(CS_SERVICE_AVAILABLE, shouldCacheServiceBeUsed).
            up().build();
    }

    // - Members ---------------------------------------------------------------

    final private ServerManager m_serverManager;
}
