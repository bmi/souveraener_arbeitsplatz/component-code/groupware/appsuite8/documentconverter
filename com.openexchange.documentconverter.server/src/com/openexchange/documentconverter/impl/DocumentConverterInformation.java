/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl;

import javax.management.NotCompliantMBeanException;
import com.openexchange.documentconverter.DocumentType;
import com.openexchange.documentconverter.JobPriority;
import com.openexchange.documentconverter.NonNull;
import com.openexchange.documentconverter.impl.api.BackendType;
import com.openexchange.documentconverter.impl.api.IDocumentConverterLongTermInformation;
import com.openexchange.management.AnnotatedStandardMBean;

/**
 * {@link DocumentConverterInformation}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
public class DocumentConverterInformation extends AnnotatedStandardMBean implements IDocumentConverterLongTermInformation {

    /**
     * Initializes a new {@link DocumentConverterInformation}.
     *
     * @param statistics Musn't be null
     * @throws NotCompliantMBeanException
     */
    public DocumentConverterInformation(
        @NonNull final LTStatistics statistics,
        @NonNull final Class<?> mBeanInterface) throws NotCompliantMBeanException {

        super("MBean for DocumentConverter", mBeanInterface);

        m_statistics = statistics;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterLongTermInformation#getRuntimeMillis()
     */
    @Override
    public long getRuntimeSecondsInstance() {
        return m_statistics.getRuntimeMillisInstance() / 1000;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterLongTermInformation#getRuntimeSecondsLongTerm()
     */
    @Override
    public long getRuntimeSecondsLongTerm() {
        return m_statistics.getRuntimeMillisLongTerm() / 1000;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterLongTermInformation#getUserRequestCount_Total()
     */
    @Override
    public long getUserRequestCountInstance_Total() {
        return m_statistics.getUserRequestCount(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterLongTermInformation#getUserRequestCount_Text()
     */
    @Override
    public long getUserRequestCountInstance_Text() {
        return m_statistics.getUserRequestCount(DocumentType.TEXT_DOCUMENT);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterLongTermInformation#getUserRequestCount_Spreadsheet()
     */
    @Override
    public long getUserRequestCountInstance_Spreadsheet() {
        return m_statistics.getUserRequestCount(DocumentType.SPREADSHEET_DOCUMENT);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterLongTermInformation#getUserRequestCount_Presentation()
     */
    @Override
    public long getUserRequestCountInstance_Presentation() {
        return m_statistics.getUserRequestCount(DocumentType.PRESENTATION_DOCUMENT);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterLongTermInformation#getUserRequestCount_Image()
     */
    @Override
    public long getUserRequestCountInstance_Image() {
        return m_statistics.getUserRequestCount(DocumentType.IMAGE);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterLongTermInformation#getLongTermUserRequestCount_Total()
     */
    @Override
    public long getUserRequestCountLongTerm_Total() {
        return m_statistics.getUserRequestCountLongTerm(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterLongTermInformation#getLongTermUserRequestCount_Text()
     */
    @Override
    public long getUserRequestCountLongTerm_Text() {
        return m_statistics.getUserRequestCountLongTerm(DocumentType.TEXT_DOCUMENT);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterLongTermInformation#getLongTermUserRequestCount_Spreadsheet()
     */
    @Override
    public long getUserRequestCountLongTerm_Spreadsheet() {
        return m_statistics.getUserRequestCountLongTerm(DocumentType.SPREADSHEET_DOCUMENT);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterLongTermInformation#getLongTermUserRequestCount_Presentation()
     */
    @Override
    public long getUserRequestCountLongTerm_Presentation() {
        return m_statistics.getUserRequestCountLongTerm(DocumentType.PRESENTATION_DOCUMENT);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterLongTermInformation#getLongTermUserRequestCount_Image()
     */
    @Override
    public long getUserRequestCountLongTerm_Image() {
        return m_statistics.getUserRequestCountLongTerm(DocumentType.IMAGE);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterLongTermInformation#getFailedUserRequestCount_Total()
     */
    @Override
    public long getUserRequestCountInstanceFailed_Total() {
        return m_statistics.getUserRequestCountFailed(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterLongTermInformation#getFailedUserRequestCount_Text()
     */
    @Override
    public long getUserRequestCountInstanceFailed_Text() {
        return m_statistics.getUserRequestCountFailed(DocumentType.TEXT_DOCUMENT);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterLongTermInformation#getFailedUserRequestCount_Spreadsheet()
     */
    @Override
    public long getUserRequestCountInstanceFailed_Spreadsheet() {
        return m_statistics.getUserRequestCountFailed(DocumentType.SPREADSHEET_DOCUMENT);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterLongTermInformation#getFailedUserRequestCount_Presentation()
     */
    @Override
    public long getUserRequestCountInstanceFailed_Presentation() {
        return m_statistics.getUserRequestCountFailed(DocumentType.PRESENTATION_DOCUMENT);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterLongTermInformation#getFailedUserRequestCount_Image()
     */
    @Override
    public long getUserRequestCountInstanceFailed_Image() {
        return m_statistics.getUserRequestCountFailed(DocumentType.IMAGE);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterLongTermInformation#getLongTermFailedUserRequestCount_Total()
     */
    @Override
    public long getUserRequestCountLongTermFailed_Total() {
        return m_statistics.getUserRequestCountLongTermFailed(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterLongTermInformation#getLongTermFailedUserRequestCount_Text()
     */
    @Override
    public long getUserRequestCountLongTermFailed_Text() {
        return m_statistics.getUserRequestCountLongTermFailed(DocumentType.TEXT_DOCUMENT);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterLongTermInformation#getLongTermFailedUserRequestCount_Spreadsheet()
     */
    @Override
    public long getUserRequestCountLongTermFailed_Spreadsheet() {
        return m_statistics.getUserRequestCountLongTermFailed(DocumentType.SPREADSHEET_DOCUMENT);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterLongTermInformation#getLongTermFailedUserRequestCount_Presentation()
     */
    @Override
    public long getUserRequestCountLongTermFailed_Presentation() {
        return m_statistics.getUserRequestCountLongTermFailed(DocumentType.PRESENTATION_DOCUMENT);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterLongTermInformation#getLongTermFailedUserRequestCount_Image()
     */
    @Override
    public long getUserRequestCountLongTermFailed_Image() {
        return m_statistics.getUserRequestCountLongTermFailed(DocumentType.IMAGE);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterLongTermInformation#getUserRequestSuccessRatio_Total()
     */
    @Override
    public double getUserRequestSuccessRatioInstance_Total() {
        return m_statistics.getUserRequestSuccessRatio(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterLongTermInformation#getUserRequestSuccessRatio_Text()
     */
    @Override
    public double getUserRequestSuccessRatioInstance_Text() {
        return m_statistics.getUserRequestSuccessRatio(DocumentType.TEXT_DOCUMENT);
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.IDocumentConverterLongTermInformation#getUserRequestSuccessRatio_Spreadsheet()
     */
    @Override
    public double getUserRequestSuccessRatioInstance_Spreadsheet() {
        return m_statistics.getUserRequestSuccessRatio(DocumentType.SPREADSHEET_DOCUMENT);
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.IDocumentConverterLongTermInformation#getUserRequestSuccessRatio_Presentation()
     */
    @Override
    public double getUserRequestSuccessRatioInstance_Presentation() {
        return m_statistics.getUserRequestSuccessRatio(DocumentType.PRESENTATION_DOCUMENT);
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.IDocumentConverterLongTermInformation#getUserRequestSuccessRatio_Image()
     */
    @Override
    public double getUserRequestSuccessRatioInstance_Image() {
        return m_statistics.getUserRequestSuccessRatio(DocumentType.IMAGE);
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.IDocumentConverterLongTermInformation#getUserRequestSuccessRatioLongTerm_Total()
     */
    @Override
    public double getUserRequestSuccessRatioLongTerm_Total() {
        return m_statistics.getUserRequestSuccessRatioLongTerm(null);
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.IDocumentConverterLongTermInformation#getUserRequestSuccessRatioLongTerm_Text()
     */
    @Override
    public double getUserRequestSuccessRatioLongTerm_Text() {
        return m_statistics.getUserRequestSuccessRatioLongTerm(DocumentType.TEXT_DOCUMENT);
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.IDocumentConverterLongTermInformation#getUserRequestSuccessRatioLongTerm_Spreadsheet()
     */
    @Override
    public double getUserRequestSuccessRatioLongTerm_Spreadsheet() {
        return m_statistics.getUserRequestSuccessRatioLongTerm(DocumentType.SPREADSHEET_DOCUMENT);
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.IDocumentConverterLongTermInformation#getUserRequestSuccessRatioLongTerm_Presentation()
     */
    @Override
    public double getUserRequestSuccessRatioLongTerm_Presentation() {
        return m_statistics.getUserRequestSuccessRatioLongTerm(DocumentType.PRESENTATION_DOCUMENT);
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.IDocumentConverterLongTermInformation#getUserRequestSuccessRatioLongTerm_Image()
     */
    @Override
    public double getUserRequestSuccessRatioLongTerm_Image() {
        return m_statistics.getUserRequestSuccessRatioLongTerm(DocumentType.IMAGE);
    }

    // - Public API ------------------------------------------------------------

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterInformationMBean#getJobsDone()
     */
    @Override
    public long getJobsProcessed() {
        return m_statistics.getJobsProcessed();
    }

    @Override
    public long getCacheEntryCount() {
        return m_statistics.getRemoteCacheEntryCount();
    }

    @Override
    public long getLocalCacheEntryCount() {
        return m_statistics.getLocalCacheEntryCount();
    }

    @Override
    public long getCachePersistentSize() {
        return m_statistics.getRemoteCachePersistentSize();
    }

    @Override
    public long getLocalCachePersistentSize() {
        return m_statistics.getLocalCachePersistentSize();
    }

    @Override
    public long getCacheFreeVolumeSize() {
        return m_statistics.getLocalCacheFreeVolumeSize();
    }

    @Override
    public long getCacheOldestEntrySeconds() {
        return m_statistics.getRemoteCacheOldestEntrySeconds();
    }

    @Override
    public long getLocalCacheOldestEntrySeconds() {
        return m_statistics.getLocalCacheOldestEntrySeconds();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterInformationMBean#getCacheHitRatio()
     */
    @Override
    public double getCacheHitRatio() {
        return m_statistics.getCacheHitRatio();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterInformationMBean#getErrorCacheHitRatio()
     */
    @Override
    public double getErrorCacheHitRatio() {
        return m_statistics.getErrorCacheHitRatio();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterInformationMBean#getJobErrorsTotal()
     */
    @Override
    public long getJobErrorsTotal() {
        return m_statistics.getJobErrorsTotal();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterInformationMBean#getJobErrorsTimeout()
     */
    @Override
    public long getJobErrorsTimeout() {
        return m_statistics.getJobErrorsTimeout();
    }

    @Override
    public int getScheduledJobCountInQueue() {
        return m_statistics.getScheduledJobCountInQueue();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterInformationMBean#getPeakJobCountInQueue_Background()
     */
    @Override
    public int getPeakJobCountInQueue_Background() {
        return m_statistics.getPeakJobCountInQueue(JobPriority.BACKGROUND);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterInformationMBean#getPeakJobCountInQueue_Low()
     */
    @Override
    public int getPeakJobCountInQueue_Low() {
        return m_statistics.getPeakJobCountInQueue(JobPriority.LOW);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterInformationMBean#getPeakJobCountInQueue_Medium()
     */
    @Override
    public int getPeakJobCountInQueue_Medium() {
        return m_statistics.getPeakJobCountInQueue(JobPriority.MEDIUM);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterInformationMBean#getPeakJobCountInQueue_High()
     */
    @Override
    public int getPeakJobCountInQueue_High() {
        return m_statistics.getPeakJobCountInQueue(JobPriority.HIGH);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterInformationMBean#getPeakJobCountInQueue_Instant()
     */
    @Override
    public int getPeakJobCountInQueue_Instant() {
        return m_statistics.getPeakJobCountInQueue(JobPriority.INSTANT);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterInformationMBean#getPeakJobCountInQueue_Total()
     */
    @Override
    public int getPeakJobCountInQueue_Total() {
        return m_statistics.getPeakJobCountInQueue(null);
    }

    /**
     *
     */
    @Override
    public int getAsyncJobCountScheduled_Total() {
        return m_statistics.getAsyncJobCountScheduled();
    }

    /**
     *
     */
    @Override
    public long getAsyncJobCountProcessed_Total() {
        return m_statistics.getAsyncJobCountProcessed();
    }

    /**
     *
     */
    @Override
    public long getAsyncJobCountDropped_Total() {
        return m_statistics.getAsyncJobCountDropped();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterInformationMBean#getPeakJobCountInQueue_Total()
     */
    @Override
    public int getPeakAsyncJobCount_Total() {
        return m_statistics.getPeakAsyncJobCount();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterInformationMBean#getMedianQueueTimeMillis_Background()
     */
    @Override
    public long getMedianQueueTimeMillis_Background() {
        return m_statistics.getMedianQueueTimeMillis(JobPriority.BACKGROUND);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterInformationMBean#getMedianQueueTime_Low()
     */
    @Override
    public long getMedianQueueTimeMillis_Low() {
        return m_statistics.getMedianQueueTimeMillis(JobPriority.LOW);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterInformationMBean#getMedianQueueTime_Medium()
     */
    @Override
    public long getMedianQueueTimeMillis_Medium() {
        return m_statistics.getMedianQueueTimeMillis(JobPriority.MEDIUM);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterInformationMBean#getMedianQueueTime_High()
     */
    @Override
    public long getMedianQueueTimeMillis_High() {
        return m_statistics.getMedianQueueTimeMillis(JobPriority.HIGH);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterInformationMBean#getMedianQueueTime_Instant()
     */
    @Override
    public long getMedianQueueTimeMillis_Instant() {
        return m_statistics.getMedianQueueTimeMillis(JobPriority.INSTANT);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterInformationMBean#getMedianQueueTime_Total()
     */
    @Override
    public long getMedianQueueTimeMillis_Total() {
        return m_statistics.getMedianQueueTimeMillis(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterInformationMBean#getMedianConversionTimeMillis_Background()
     */
    @Override
    public long getMedianConversionTimeMillis_Background() {
        return m_statistics.getMedianConversionTimeMillis(JobPriority.BACKGROUND);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterInformationMBean#getConversionTime_Low()
     */
    @Override
    public long getMedianConversionTimeMillis_Low() {
        return m_statistics.getMedianConversionTimeMillis(JobPriority.LOW);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterInformationMBean#getConversionTime_Medium()
     */
    @Override
    public long getMedianConversionTimeMillis_Medium() {
        return m_statistics.getMedianConversionTimeMillis(JobPriority.MEDIUM);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterInformationMBean#getConversionTime_High()
     */
    @Override
    public long getMedianConversionTimeMillis_High() {
        return m_statistics.getMedianConversionTimeMillis(JobPriority.HIGH);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterInformationMBean#getConversionTime_Instant()
     */
    @Override
    public long getMedianConversionTimeMillis_Instant() {
        return m_statistics.getMedianConversionTimeMillis(JobPriority.INSTANT);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterInformationMBean#getConversionTime_Total()
     */
    @Override
    public long getMedianConversionTimeMillis_Total() {
        return m_statistics.getMedianConversionTimeMillis(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterInformationMBean#getMedianJobTimeMillis_Background()
     */
    @Override
    public long getMedianJobTimeMillis_Background() {
        return m_statistics.getMedianJobTimeMillis(JobPriority.BACKGROUND);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterInformationMBean#getConversionTime_Low()
     */
    @Override
    public long getMedianJobTimeMillis_Low() {
        return m_statistics.getMedianJobTimeMillis(JobPriority.LOW);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterInformationMBean#getConversionTime_Medium()
     */
    @Override
    public long getMedianJobTimeMillis_Medium() {
        return m_statistics.getMedianJobTimeMillis(JobPriority.MEDIUM);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterInformationMBean#getConversionTime_High()
     */
    @Override
    public long getMedianJobTimeMillis_High() {
        return m_statistics.getMedianJobTimeMillis(JobPriority.HIGH);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterInformationMBean#getConversionTime_Instant()
     */
    @Override
    public long getMedianJobTimeMillis_Instant() {
        return m_statistics.getMedianJobTimeMillis(JobPriority.INSTANT);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterInformationMBean#getConversionTime_Total()
     */
    @Override
    public long getMedianJobTimeMillis_Total() {
        return m_statistics.getMedianJobTimeMillis(null);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterInformationMBean#getConversionCount()
     */
    @Override
    public long getConversionCount() {
        return m_statistics.getConversionCount(null);
    }

    @Override
    public long getConversionCountReaderEngine() {
        return m_statistics.getConversionCount(BackendType.READERENGINE);
    }

    /**
     *
     */
    @Override
    public long getConversionCountPDFTool() {
        return m_statistics.getConversionCount(BackendType.PDFTOOL);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterInformationMBean#getConversionCountWithMultipleRuns()
     */
    @Override
    public long getConversionCountWithMultipleRuns() {
        return m_statistics.getConversionCountWithMultipleRuns();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterInformationMBean#getConversionErrorsGeneral()
     */
    @Override
    public long getConversionErrorsGeneral() {
        return m_statistics.getConversionErrorsGeneral();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterInformationMBean#getConversionErrorsTimeout()
     */
    @Override
    public long getConversionErrorsTimeout() {
        return m_statistics.getConversionErrorsTimeout();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterInformationMBean#getConversionErrorsDisposed()
     */
    @Override
    public long getConversionErrorsDisposed() {
        return m_statistics.getConversionErrorsDisposed();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterInformationMBean#getConversionErrorsPassword()
     */
    @Override
    public long getConversionErrorsPassword() {
        return m_statistics.getConversionErrorsPassword();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverterInformationMBean#getConversionErrorsNoContent()
     */
    @Override
    public long getConversionErrorsNoContent() {
        return m_statistics.getConversionErrorsNoContent();
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.api.IDocumentConverterInformationMBean#getConversionErrorsOutOfMemory()
     */
    @Override
    public long getConversionErrorsOutOfMemory() {
        return m_statistics.getConversionErrorsOutOfMemory();
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.api.IDocumentConverterInformationMBean#getConversionErrorsPDFTool()
     */
    @Override
    public long getConversionErrorsPDFTool() {
        return m_statistics.getConversionErrorsPDFTool();
    }

    @Override
    public long getPendingStatefulJobCount() {
        return m_statistics.getPendingStatefulJobCount();
    }

    /**
     *
     */
    @Override
    public long getQueueLimitHighReachedCountInstance() {
        return m_statistics.getQueueLimitHighReachedCount();
    }

    /**
    *
    */
   @Override
   public long getQueueLimitHighReachedCountLongTerm() {
       return m_statistics.getQueueLimitHighReachedCountLongTerm();
   }

    /**
     *
     */
    @Override
    public long getQueueLimitLowReachedCountInstance() {
        return m_statistics.getQueueLimitLowReachedCount();
    }

    /**
     *
     */
    @Override
    public long getQueueLimitLowReachedCountLongTerm() {
        return m_statistics.getQueueLimitLowReachedCountLongTerm();
    }

    /**
     *
     */
    @Override
    public long getQueueTimeoutCountInstance() {
        return m_statistics.getQueueTimeoutCount();
    }

    /**
     *
     */
    @Override
    public long getQueueTimeoutCountLongTerm() {
        return m_statistics.getQueueTimeoutCountLongTerm();
    }

    // - Members ---------------------------------------------------------------

    final private LTStatistics m_statistics;
}
