/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl;

import com.openexchange.documentconverter.impl.api.IDocumentConverterLongTermInformation;
import com.openexchange.metrics.micrometer.Micrometer;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Tags;

/**
 * {@link DocumentConverterMetrics}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.6
 */
public class DocumentConverterMetrics {
    private static final String GROUP = "documentconverter";
    private static final String NO_UNIT = null;
    private static final String UNIT_MS = "ms";
    private static final String UNIT_SEC = "seconds";
    private static final Tags TAGS_ASYNC = Tags.of("happening", "async");
    private static final Tags TAGS_JOBS = Tags.of("type", "jobs");
    private static final Tags TAGS_JOBS_ASYNC = TAGS_JOBS.and(TAGS_ASYNC);
    private static final Tags TAGS_CACHE = Tags.of("type", "cache");
    private static final Tags TAGS_CONVERSIONS = Tags.of("type", "conversions");
    private static final Tags TAGS_QUEUE = Tags.of("type", "queue");
    private static final Tags TAGS_QUEUE_ASYNC = TAGS_QUEUE.and(TAGS_ASYNC);
    private static final Tags TAGS_PRIO_BACKGROUND = Tags.of("priority", "background");
    private static final Tags TAGS_PRIO_LOW = Tags.of("priority", "low");
    private static final Tags TAGS_PRIO_MEDIUM = Tags.of("priority", "medium");
    private static final Tags TAGS_PRIO_HIGH = Tags.of("priority", "high");
    private static final Tags TAGS_PRIO_INSTANT = Tags.of("priority", "instant");
    private static final Tags TAGS_PRIO_ALL = Tags.of("priority", "all");
    private static final Tags TAGS_TYPE_ALL = Tags.of("type", "all");
    private static final Tags TAGS_TYPE_TEXT = Tags.of("type", "text");
    private static final Tags TAGS_TYPE_CALC = Tags.of("type", "spreadsheet");
    private static final Tags TAGS_TYPE_PRES = Tags.of("type", "presentation");
    private static final Tags TAGS_TYPE_IMAGE = Tags.of("type", "image");

    private final IDocumentConverterLongTermInformation metricsDataProvider;

    public DocumentConverterMetrics(IDocumentConverterLongTermInformation metricsDataProvider) {
        this.metricsDataProvider = metricsDataProvider;
    }

    public void registerMetrics() {

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".jobs.processed.total", TAGS_JOBS,
            "The total number of jobs that have been processed.", NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getJobsProcessed() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".jobs.processed.error.timeout.total", TAGS_JOBS,
            "The total number of jobs, that returned with a timeout error.", NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getJobErrorsTimeout() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".jobs.processed.error.total", TAGS_JOBS,
            "The total number of jobs, that returned with an error.", NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getJobErrorsTotal() : 0);

        // -------------------------------------------------------------------------

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".jobs.median.time", TAGS_PRIO_BACKGROUND,
            "The median time in milliseconds, a job needed to complete, measured from the beginning of the last reset/initialization on (default period: 5 minutes).", UNIT_MS, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getMedianJobTimeMillis_Background() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".jobs.median.time", TAGS_PRIO_LOW, null, UNIT_MS, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getMedianJobTimeMillis_Low() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".jobs.median.time", TAGS_PRIO_MEDIUM, null, UNIT_MS, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getMedianJobTimeMillis_Medium() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".jobs.median.time", TAGS_PRIO_HIGH, null, UNIT_MS, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getMedianJobTimeMillis_High() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".jobs.median.time", TAGS_PRIO_INSTANT, null, UNIT_MS, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getMedianJobTimeMillis_Instant() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".jobs.median.time", TAGS_PRIO_ALL, null, UNIT_MS, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getMedianJobTimeMillis_Total() : 0);

        // -------------------------------------------------------------------------

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".jobs.stateful.pending.count", TAGS_JOBS,
            "The number of stateful job conversions for that no endConvert has been called up to now.", NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getPendingStatefulJobCount() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".jobs.scheduled.total", TAGS_JOBS,
            "The current number of all scheduled, synchronous jobs within the queue, waiting to be processed.", NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getScheduledJobCountInQueue() : 0);

        // -------------------------------------------------------------------------

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".jobs.peak.count", TAGS_PRIO_BACKGROUND,
            "The peak number of jobs in queue, measured from the beginning of the last reset/initialization on (default period: 5 minutes).", NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getPeakJobCountInQueue_Background() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".jobs.peak.count", TAGS_PRIO_LOW, null, NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getPeakJobCountInQueue_Low() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".jobs.peak.count", TAGS_PRIO_MEDIUM, null, NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getPeakJobCountInQueue_Medium() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".jobs.peak.count", TAGS_PRIO_HIGH, null, NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getPeakJobCountInQueue_High() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".jobs.peak.count", TAGS_PRIO_INSTANT, null, NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getPeakJobCountInQueue_Instant() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".jobs.peak.count", TAGS_PRIO_ALL, null, NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getPeakJobCountInQueue_Total() : 0);

        // -------------------------------------------------------------------------

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".jobs.asyncqueue.scheduled.total", TAGS_JOBS_ASYNC,
            "The total number of asynchronous jobs that are currently scheduled.", NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getAsyncJobCountScheduled_Total() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".jobs.asyncqueue.processed.total", TAGS_JOBS_ASYNC,
            "The total number of asynchronous jobs that have been processed.", NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getAsyncJobCountProcessed_Total() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".jobs.asyncqueue.dropped.total", TAGS_JOBS_ASYNC,
            "The total number of asynchronous jobs that have been dropped.", NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getAsyncJobCountDropped_Total() : 0);

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".jobs.asyncqueue.peak.total", TAGS_QUEUE_ASYNC,
            "The peak number of asynchronous jobs that have been scheduled, measured from the beginning of the last reset/initialization on (default period: 5 minutes).", NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getPeakAsyncJobCount_Total() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".jobs.peak.count.asyncQueue", TAGS_QUEUE_ASYNC,
            "The peak number of asynchronous jobs that have been scheduled, measured from the beginning of the last reset/initialization on (default period: 5 minutes).", NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getPeakAsyncJobCount_Total() : 0);

        // -------------------------------------------------------------------------

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".jobs.median.time.waiting", TAGS_PRIO_BACKGROUND,
            "The median time in milliseconds, a job waited in the queue for processing, measured from the beginning of the last reset/initialization on (default period: 5 minutes).", UNIT_MS, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getMedianQueueTimeMillis_Background() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".jobs.median.time.waiting", TAGS_PRIO_LOW, null, UNIT_MS, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getMedianQueueTimeMillis_Low() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".jobs.median.time.waiting", TAGS_PRIO_MEDIUM, null, UNIT_MS, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getMedianQueueTimeMillis_Medium() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".jobs.median.time.waiting", TAGS_PRIO_HIGH, null, UNIT_MS, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getMedianQueueTimeMillis_High() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".jobs.median.time.waiting", TAGS_PRIO_INSTANT, null, UNIT_MS, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getMedianQueueTimeMillis_Instant() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".jobs.median.time.waiting", TAGS_PRIO_ALL, null, UNIT_MS, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getMedianConversionTimeMillis_Total() : 0);

        // -------------------------------------------------------------------------

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".cache.entry.count", TAGS_CACHE,
            "The current count of entries, stored within the persistent cache.", NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getCacheEntryCount() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".cache.entry.size", TAGS_CACHE,
            "The current size in bytes of all persistent entries, stored within the cache directory. The size is calculated on a volume block size basis to reflect the correct summed up size.", NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getCachePersistentSize() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".cache.free.volume.size", TAGS_CACHE,
            "The current free size in bytes of the volume, the cache root directory is located on. The size is calculated on a volume block size basis to reflect the correct size of the free volume.", NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getCacheFreeVolumeSize() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".cache.oldest.entry.age", TAGS_CACHE,
            "The age of the oldest entry within the cache in seconds.", UNIT_SEC, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getCacheOldestEntrySeconds() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".cache.hit.ratio", TAGS_CACHE,
            "The ratio of successful job results that are retrieved from the cache against the total number of processed jobs as simple quotient.", NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getCacheHitRatio() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".errorcache.hit.ratio", TAGS_CACHE,
            "The ratio of job errors that are retrieved from the errror cache against the total number of job errors as simple quotient.", NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getErrorCacheHitRatio() : 0);

        // -------------------------------------------------------------------------

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".conversions.readerengine.and.pdftool.total", TAGS_CONVERSIONS,
            "The total number of all physical ReaderEngine and PDFTool conversions.", NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getConversionCount() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".conversions.readerengine.total", TAGS_CONVERSIONS,
            "The total number of all physical ReaderEngine conversions.", NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getConversionCountReaderEngine() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".conversions.pdftool.total", TAGS_CONVERSIONS,
            "The total number of all physical PDFTool conversions.", NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getConversionCountPDFTool() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".conversions.needed.new.readerengine.total", TAGS_CONVERSIONS,
            "The total number of physical ReaderEngine conversions, that needed a second, new ReaderEngine instance.", NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getConversionCountWithMultipleRuns() : 0);

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".conversions.readerengine.error.total", Tags.of("error", "general"),
            "The total number of physical ReaderEngine conversions, that resulted in an error.", NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getConversionErrorsGeneral() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".conversions.readerengine.error.total", Tags.of("error", "timeout"), null, NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getConversionErrorsTimeout() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".conversions.readerengine.error.total", Tags.of("error", "disposed"), null, NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getConversionErrorsDisposed() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".conversions.readerengine.error.total", Tags.of("error", "password"), null, NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getConversionErrorsPassword() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".conversions.readerengine.error.total", Tags.of("error", "noContent"), null, NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getConversionErrorsNoContent() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".conversions.readerengine.error.total", Tags.of("error", "outOfMemory"), null, NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getConversionErrorsOutOfMemory() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".conversions.pdftool.error.total", TAGS_CONVERSIONS,
            "The total number of physical PDFTool conversions, that resulted in an error.", NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getConversionErrorsPDFTool() : 0);

        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".conversions.readerengine.median.time", TAGS_PRIO_BACKGROUND,
            "The median time in milliseconds, a physical ReaderEngine conversion with lasted, measured from the beginning of the last reset/initialization on (default period: 5 minutes).", UNIT_MS, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getMedianConversionTimeMillis_Background() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".conversions.readerengine.median.time", TAGS_PRIO_LOW, null, UNIT_MS, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getMedianConversionTimeMillis_Low() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".conversions.readerengine.median.time", TAGS_PRIO_MEDIUM, null, UNIT_MS, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getMedianConversionTimeMillis_Medium() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".conversions.readerengine.median.time", TAGS_PRIO_HIGH, null, UNIT_MS, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getMedianConversionTimeMillis_High() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".conversions.readerengine.median.time", TAGS_PRIO_INSTANT, null, UNIT_MS, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getMedianConversionTimeMillis_Instant() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".conversions.readerengine.median.time", TAGS_PRIO_ALL, null, UNIT_MS, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getMedianConversionTimeMillis_Total() : 0);

        // -------------------------------------------------------------------------
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".runtime", Tags.of("region", "instance"),
            "The runtime in seconds for the current DocumentConverter server instance", UNIT_SEC, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getRuntimeSecondsInstance() : 0);

        // -------------------------------------------------------------------------
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".user.requests.count", TAGS_TYPE_ALL,
            "The number of all WebService requests, that have the user flag set to true, for the current DocumentConverter server instance", NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getUserRequestCountInstance_Total() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".user.requests.count", TAGS_TYPE_TEXT, null, NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getUserRequestCountInstance_Text() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".user.requests.count", TAGS_TYPE_CALC, null, NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getUserRequestCountInstance_Spreadsheet() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".user.requests.count", TAGS_TYPE_PRES, null, NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getUserRequestCountInstance_Presentation() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".user.requests.count", TAGS_TYPE_IMAGE, null, NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getUserRequestCountInstance_Image() : 0);

        // -------------------------------------------------------------------------
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".user.requests.failed.count", TAGS_TYPE_ALL,
            "The number of all WebService requests, that failed with an error and that have the user flag set to true, for the current DocumentConverter server instance", NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getUserRequestCountInstanceFailed_Total() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".user.requests.failed.count", TAGS_TYPE_TEXT, null, NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getUserRequestCountInstanceFailed_Text() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".user.requests.failed.count", TAGS_TYPE_CALC, null, NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getUserRequestCountInstanceFailed_Spreadsheet() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".user.requests.failed.count", TAGS_TYPE_PRES, null, NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getUserRequestCountInstanceFailed_Presentation() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".user.requests.failed.count", TAGS_TYPE_IMAGE, null, NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getUserRequestCountInstanceFailed_Image() : 0);

        // -------------------------------------------------------------------------
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".user.requests.success.ratio", TAGS_TYPE_ALL,
            "The ratio of successful WebService requests against failed WebService requests, that have the user flag set to true, for the current DocumentConverter server instance", NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getUserRequestSuccessRatioInstance_Total() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".user.requests.success.ratio", TAGS_TYPE_TEXT, null, NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getUserRequestSuccessRatioInstance_Text() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".user.requests.success.ratio", TAGS_TYPE_CALC, null, NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getUserRequestSuccessRatioInstance_Spreadsheet() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".user.requests.success.ratio", TAGS_TYPE_PRES, null, NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getUserRequestSuccessRatioInstance_Spreadsheet() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".user.requests.success.ratio", TAGS_TYPE_IMAGE, null, NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getUserRequestSuccessRatioInstance_Image() : 0);

        // -------------------------------------------------------------------------
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".jobs.queue.limit.total", Tags.of("limit", "high"),
            "The accumulated number of times the job queue limits have been reached for the current DocumentConverter server instance", NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getQueueLimitHighReachedCountInstance() : 0);
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".jobs.queue.limit.total", Tags.of("limit", "low"), null, NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getQueueLimitLowReachedCountInstance() : 0);

        // -------------------------------------------------------------------------
        Micrometer.registerOrUpdateGauge(Metrics.globalRegistry, GROUP + ".jobs.queue.timeout.total", Tags.of("region", "queue"),
            "The accumulated number of times the job queue timeout has been reached for the current DocumentConverter server instance", NO_UNIT, this,
            (m) -> (null != metricsDataProvider) ? metricsDataProvider.getQueueTimeoutCountInstance() : 0);
    }

}
