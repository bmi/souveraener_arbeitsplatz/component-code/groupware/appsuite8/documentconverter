/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl;

import java.util.HashMap;
import com.openexchange.documentconverter.JobErrorEx;
import com.openexchange.documentconverter.LogData;
import com.openexchange.documentconverter.Properties;
import com.openexchange.documentconverter.impl.api.BackendType;
import com.openexchange.documentconverter.impl.api.ConverterStatus;
import com.sun.star.beans.PropertyValue;
import com.sun.star.graphic.XGraphic;
import com.sun.star.graphic.XGraphicProvider;
import com.sun.star.lang.XMultiComponentFactory;
import com.sun.star.uno.Exception;
import com.sun.star.uno.UnoRuntime;

//-----------------------------
//- class GraphicConverterJob -
//-----------------------------

/**
 * {@link GraphicConverterJob}
 *
 * @author <a href="mailto:sven.jacobi@open-xchange.com">Sven Jacobi</a>
 */
class GraphicConverterJob extends ConverterJob {

    /**
     * Initializes a new {@link GraphicConverterJob}.
     * @param jobProperties
     */
    GraphicConverterJob(HashMap<String, Object> jobProperties, HashMap<String, Object> resultProperties) {
        super(jobProperties, resultProperties);
        applyJobProperties();
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.ConverterJob#getHash()
     */
    @Override
    public String getHash() {
        if (null == m_cacheHash) {
            final StringBuilder hashBuilder = super.getInputFileHashBuilder();

            if (null != hashBuilder) {
                setHash(hashBuilder.append(m_resultExtension));
            }
        }

        return m_cacheHash;
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.IJob#needsInstance()
     */
    @Override
    public BackendType backendTypeNeeded() {
        return BackendType.READERENGINE;
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.ConverterJob#doExecute(com.openexchange.documentconverter.IInstance)
     */
    @Override
    protected ConverterStatus doExecute(Object jobExecutionData) throws com.sun.star.lang.DisposedException, Exception {
        final REInstance instance = (jobExecutionData instanceof REInstance) ? (REInstance) jobExecutionData : null;
        XMultiComponentFactory componentFactory = null;
        final String inputFileUrlString = ServerManager.getAdjustedFileURL(m_inputFile);

        if (ServerManager.isLogTrace()) {
            ServerManager.logTrace("DC converting graphic", new LogData("graphic_url", inputFileUrlString), new LogData("target_mimetype", m_resultMimeType));
        }

        if ((null != instance) && (null != (componentFactory = instance.getComponentFactory()))) {
            final XGraphicProvider graphicProvider = UnoRuntime.queryInterface(XGraphicProvider.class, componentFactory.createInstanceWithContext(
                    "com.sun.star.graphic.GraphicProvider",
                    instance.getDefaultContext()));

            final PropertyValue inputArgs[] = new PropertyValue[1];
            inputArgs[0] = new PropertyValue();
            inputArgs[0].Name = "URL";
            inputArgs[0].Value = inputFileUrlString;

            final XGraphic graphic = graphicProvider.queryGraphic(inputArgs);

            if (null != graphic) {
                final boolean widthOrHeightSet = (m_pixelWidth > 0) || (m_pixelHeight > 0);
                int filterDataCount = widthOrHeightSet ? 2 : 1;

                final PropertyValue[] filterData = new PropertyValue[filterDataCount];

                if (widthOrHeightSet) {
                    filterData[--filterDataCount] = new PropertyValue();
                    filterData[filterDataCount].Name = "PixelHeight";
                    filterData[filterDataCount].Value = Integer.valueOf((m_pixelHeight > 0) ? m_pixelHeight : -1);

                    filterData[--filterDataCount] = new PropertyValue();
                    filterData[filterDataCount].Name = "PixelWidth";
                    filterData[filterDataCount].Value = Integer.valueOf((m_pixelWidth > 0) ? m_pixelWidth : -1);
                } else {
                    // set FilterData (ImageResolution of 300 DPI);
                    // a dynamic setting of the logical size to be
                    // displayed would be preferred here (KA: #45144 - 2017-03-29)
                    // ---
                    // Update 2017-11-1: set fixed resolution only, if
                    // width/height properties are not set or set to -1x-1
                    filterData[--filterDataCount] = new PropertyValue();
                    filterData[filterDataCount].Name = "ImageResolution";
                    filterData[filterDataCount].Value = Integer.valueOf(300);
                }

                // set store properties
                final PropertyValue outputArgs[] = new PropertyValue[4];

                outputArgs[0] = new PropertyValue();
                outputArgs[0].Name = "MimeType";
                outputArgs[0].Value = m_resultMimeType;

                outputArgs[1] = new PropertyValue();
                outputArgs[1].Name = "URL";
                outputArgs[1].Value = ServerManager.getAdjustedFileURL(m_outputFile);

                outputArgs[2] = new PropertyValue();
                outputArgs[2].Name = "Overwrite";
                outputArgs[2].Value = Boolean.TRUE;

                outputArgs[3] = new PropertyValue();
                outputArgs[3].Name = "FilterData";
                outputArgs[3].Value = filterData;


                graphicProvider.storeGraphic(graphic, outputArgs);

                m_resultJobErrorEx = new JobErrorEx();
            }
        }

        return m_resultJobErrorEx.hasNoError() ? ConverterStatus.OK : ConverterStatus.ERROR;
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.ConverterJob#applyJobProperties(java.util.HashMap)
     */
    @Override
    protected void applyJobProperties() {
        super.applyJobProperties();

        m_resultMimeType = (String) m_jobProperties.get(Properties.PROP_MIME_TYPE);
        m_resultMimeType = ((null == m_resultMimeType) || (m_resultMimeType.length() <= 0)) ? "image/png" : m_resultMimeType.toLowerCase();
        m_resultExtension = "image/jpeg".equals(m_resultMimeType) ? "jpg" : ("image/png".equals(m_resultMimeType) ? "png" : "img");
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.ConverterJob#getMeasurementType()
     */
    @Override
    public ConverterMeasurement getMeasurementType() {
        return ConverterMeasurement.CONVERT_DOC_TO_GRAPHIC;
    }
}
