/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import org.apache.commons.io.FileUtils;
import com.openexchange.documentconverter.JobErrorEx;
import com.openexchange.documentconverter.Properties;
import com.openexchange.documentconverter.impl.api.BackendType;
import com.openexchange.documentconverter.impl.api.ConverterStatus;
import com.openexchange.exception.ExceptionUtils;
import com.sun.star.beans.PropertyValue;
import com.sun.star.document.XExporter;
import com.sun.star.document.XFilter;
import com.sun.star.io.XInputStream;
import com.sun.star.io.XOutputStream;
import com.sun.star.io.XStream;
import com.sun.star.lang.XComponent;
import com.sun.star.lang.XMultiServiceFactory;
import com.sun.star.uno.UnoRuntime;

//---------------------------
//- class ImageConverterJob -
//---------------------------

/**
 * {@link ImageConverterJob}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
class ImageConverterJob extends ConverterJob {

    /**
     * Initializes a new {@link ImageConverterJob}.
     *
     * @param inputStream
     * @param outputStream
     */
    ImageConverterJob(HashMap<String, Object> jobProperties, HashMap<String, Object> resultProperties) {
        super(jobProperties, resultProperties);
        applyJobProperties();
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.ConverterJob#getHash()
     */
    @Override
    public String getHash() {
        if (null == m_cacheHash) {
            final StringBuilder hashBuilder = super.getInputFileHashBuilder();

            if (null != hashBuilder) {
                if (m_resultZipArchive) {
                    hashBuilder.append('z');
                }

                hashBuilder.
                    append(m_resultExtension).
                    append('#').append(m_pageRange).
                    append('@').append(m_imageResolution).
                    append(',').append(m_pixelWidth).append('x').append(m_pixelHeight);

                setHash(hashBuilder);
            }
        }

        return m_cacheHash;
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.IJob#needsInstance()
     */
    @Override
    public BackendType backendTypeNeeded() {
        return BackendType.READERENGINE;
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.ConverterJob#doExecute(com.openexchange.documentconverter.IInstance)
     */
    @Override
    protected ConverterStatus doExecute(Object jobExecutionData) throws com.sun.star.lang.DisposedException, InterruptedException, IOException, MalformedURLException, com.sun.star.uno.Exception {
        final REInstance instance = (jobExecutionData instanceof REInstance) ? (REInstance) jobExecutionData : null;
        ConverterStatus converterStatus = ConverterStatus.ERROR;

        if (null != instance) {
            final XComponent component = super.loadComponent(instance, "1");
            final XMultiServiceFactory filterFactory = instance.getFilterFactory();

            if ((null != component) && (null != filterFactory)) {
                XFilter filter = null;
                final XExporter exporter = UnoRuntime.queryInterface(
                    XExporter.class,
                    filterFactory.createInstanceWithArguments("writer_ox_multi_exporter_filter", new PropertyValue[0]));

                if ((null != exporter) && (null != (filter = UnoRuntime.queryInterface(XFilter.class, exporter)))) {

                    final XStream xMemoryStream = UnoRuntime.queryInterface(XStream.class,
                        instance.getComponentFactory().createInstanceWithContext("com.sun.star.comp.MemoryStream",
                            instance.getDefaultContext()));
                    final XOutputStream xOutputStream = xMemoryStream.getOutputStream();
                    final PropertyValue outputArgs[] = new PropertyValue[3];

                    outputArgs[0] = new PropertyValue();
                    outputArgs[0].Name = "OutputStream";
                    outputArgs[0].Value = xOutputStream;

                    outputArgs[1] = new PropertyValue();
                    outputArgs[1].Name = "Overwrite";
                    outputArgs[1].Value = Boolean.TRUE;

                    final PropertyValue filterData[] = new PropertyValue[7];

                    filterData[0] = new PropertyValue();
                    filterData[0].Name = "ContainerType";
                    filterData[0].Value = (m_singlePage && !m_resultZipArchive) ? "SinglePage" : "ZipArchive";

                    filterData[1] = new PropertyValue();
                    filterData[1].Name = "MimeType";
                    filterData[1].Value = m_resultMimeType;

                    filterData[2] = new PropertyValue();
                    filterData[2].Name = "PageRange";
                    filterData[2].Value = m_pageRange;

                    filterData[3] = new PropertyValue();
                    filterData[3].Name = "ImageResolution";
                    filterData[3].Value = m_imageResolution;

                    filterData[4] = new PropertyValue();
                    filterData[4].Name = "PixelWidth";
                    filterData[4].Value = Integer.valueOf(m_pixelWidth);

                    filterData[5] = new PropertyValue();
                    filterData[5].Name = "PixelHeight";
                    filterData[5].Value = Integer.valueOf(m_pixelHeight);

                    filterData[6] = new PropertyValue();
                    filterData[6].Name = "ImageScaling";
                    filterData[6].Value = "PreserveAspectRatio";

                    if ((null != m_imageScaleType) && (ScaleType.COVER == m_imageScaleType)) {
                        filterData[6].Value = filterData[6].Value + "NoExpand";
                    }

                    outputArgs[2] = new PropertyValue();
                    outputArgs[2].Name = "FilterData";
                    outputArgs[2].Value = filterData;

                    exporter.setSourceDocument(component);
                    filter.filter(outputArgs);

                    xOutputStream.flush();
                    xOutputStream.closeOutput();

                    final XInputStream xInputStream = xMemoryStream.getInputStream();

                    if (null != xInputStream) {
                        try (final BufferedOutputStream bufferedOutputStm = new BufferedOutputStream(FileUtils.openOutputStream(m_outputFile))) {
                            final int bufferSize = 8192;
                            final byte buffer[][] = new byte[1][];

                            for (int readBytes = 0; (readBytes = xInputStream.readBytes(buffer, bufferSize)) > 0;) {
                                bufferedOutputStm.write(buffer[0], 0, readBytes);
                            }

                            bufferedOutputStm.flush();
                        } catch (final Exception e) {
                            ServerManager.logExcp(e);
                        } finally {
                            try {
                                xInputStream.closeInput();
                            } catch (final Exception e) {
                                ServerManager.logExcp(e);
                            }
                        }
                    }

                    m_resultJobErrorEx = new JobErrorEx();
                }
            }

            if (null != component) {
                try {
                    closeComponent(component);
                    converterStatus = ConverterStatus.OK;
                } catch (Throwable e) {
                    ExceptionUtils.handleThrowable(e);

                    if (m_resultJobErrorEx.hasNoError()) {
                        converterStatus = ConverterStatus.OK_WITH_ENGINE_ERROR;
                    } else {
                        throw e;
                    }
                }
            }
        }

        return converterStatus;
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.ConverterJob#applyJobProperties(java.util.HashMap)
     */
    @Override
    protected void applyJobProperties() {
        super.applyJobProperties();

        Object obj = null;

        m_resultMimeType = (String) m_jobProperties.get(Properties.PROP_MIME_TYPE);
        m_resultMimeType = ((null == m_resultMimeType) || (m_resultMimeType.length() <= 0)) ? "image/jpeg" : m_resultMimeType.toLowerCase();
        m_resultExtension = "image/jpeg".equals(m_resultMimeType) ? "jpg" : ("image/png".equals(m_resultMimeType) ? "png" : "img");
        m_singlePage = false;

        if (null != (obj = m_jobProperties.get(Properties.PROP_IMAGE_RESOLUTION))) {
            m_imageResolution = (Integer) obj;
        }

        if ((obj = m_jobProperties.get(Properties.PROP_IMAGE_SCALE_TYPE)) instanceof String) {
            m_imageScaleType = ScaleType.fromString((String) obj);
        }

        if (null != (obj = m_jobProperties.get(Properties.PROP_PIXEL_WIDTH))) {
            m_pixelWidth = ((Integer) obj).intValue();
        }

        if (null != (obj = m_jobProperties.get(Properties.PROP_PIXEL_HEIGHT))) {
            m_pixelHeight = ((Integer) obj).intValue();
        }

        try {
            // if page range is reverse parseable to an int, we have a single page to export
            if (Integer.toString(Integer.parseInt(m_pageRange)).equals(m_pageRange)) {
                m_resultPageCount = 1;
                m_singlePage = true;
            }
        } catch (@SuppressWarnings("unused") final NumberFormatException e) {
            // page Count not known currently
            m_resultPageCount = -1;
        }
    }

    // - Implementation-------------------------------------------------

    /*
     * (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.ConverterJob#getMeasurementType()
     */
    @Override
    public ConverterMeasurement getMeasurementType() {
        return ConverterMeasurement.CONVERT_DOC_TO_GRAPHIC;
    }

    // -------------------------------------------------------------------------

    private Integer m_imageResolution = ServerManager.IMAGE_RESOLUTION_ORIGINAL;

    private ScaleType m_imageScaleType = ScaleType.CONTAIN;

    private boolean m_singlePage = false;
}
