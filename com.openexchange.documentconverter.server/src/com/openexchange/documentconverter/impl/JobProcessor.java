/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import com.openexchange.documentconverter.JobError;
import com.openexchange.documentconverter.JobErrorEx;
import com.openexchange.documentconverter.JobPriority;
import com.openexchange.documentconverter.LogData;
import com.openexchange.documentconverter.NonNull;
import com.openexchange.documentconverter.Properties;
import com.openexchange.documentconverter.impl.Watchdog.WatchdogMode;
import com.openexchange.documentconverter.impl.api.BackendType;
import com.openexchange.documentconverter.impl.api.IJob;
import com.openexchange.documentconverter.impl.api.IJobProcessor;
import com.openexchange.documentconverter.impl.api.IJobStatusListener;
import com.openexchange.documentconverter.impl.api.IQueueMonitor;
import com.openexchange.documentconverter.impl.api.JobEntry;
import com.openexchange.documentconverter.impl.api.JobStatus;
import com.openexchange.documentconverter.impl.api.REDescriptor;
import com.openexchange.exception.ExceptionUtils;

//----------------------------
//- class ServerJobProcessor -
//----------------------------

/**
 * {@link ServerJobProcessor}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
public class JobProcessor implements IJobProcessor, WatchdogHandler {

    final protected static long NO_RESPONSE_TIMEOUT_PERCENTAGE = 125L;

    final protected static String GENERIC_JOB = "generic job";

    /**
     * {@link EngineGroup}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v7.8.0
     */
    class EngineGroup {

        /**
         * Initializes a new {@link EngineGroup}.
         *
         * @param engineVector
         */
        EngineGroup(@NonNull final Vector<JobThread> engineVector, @NonNull final JobEntryQueue entryQueue) {
            super();

            m_engineVector = engineVector;
            m_entryQueue = entryQueue;
        }

        /**
         * @return The name of the supported backend engine type
         */
        String getGroupName() {
            return ((m_engineVector.size() > 0) ? m_engineVector.get(0).getBackendType().toString().toUpperCase() : "Unknown");
        }

        /**
         * getJobEntryQueue
         *
         * @return
         */
        JobEntryQueue getJobEntryQueue() {
            return m_entryQueue;
        }

        /**
         * @return
         */
        Vector<JobThread> getJobThreads() {
            return m_engineVector;
        }

        /**
         *
         */
        void start() {
            for (final JobThread curThread : m_engineVector) {
                curThread.start();
            }
        }

        /**
         *
         */
        void terminate() {
            final String groupName = getGroupName();
            final boolean trace = ServerManager.isLogTrace();
            long traceStartTimeMillis = 0;

            if (trace) {
                traceStartTimeMillis = System.currentTimeMillis();
                ServerManager.logTrace(new StringBuilder(256).
                    append("DC starting shutdown of EngineGroup ").append(groupName).append(": ").
                    append("...").toString());
            }

            m_entryQueue.terminate();

            for (final JobThread curThread : m_engineVector) {
                try {
                    curThread.terminate();
                } catch (Throwable e) {
                    ExceptionUtils.handleThrowable(e);
                    ServerManager.logExcp(new Exception(e));
                }

                try {
                    curThread.join();
                } catch (Throwable e) {
                    ExceptionUtils.handleThrowable(e);
                }
            }

            if (trace) {
                ServerManager.logTrace(new StringBuilder(256).
                    append("DC finished shutdown of EngineGroup ").append(groupName).append(": ").
                    append(System.currentTimeMillis() - traceStartTimeMillis).append("ms").
                    append('!').toString());
            }

        }

        // - Members -----------------------------------------------------------

        Vector<JobThread> m_engineVector = null;

        JobEntryQueue m_entryQueue = null;
    }

    /**
     * Initializes a new {@link JobProcessor}.
     */
    public JobProcessor(
        @NonNull ServerManager serverManager,
        @NonNull final REDescriptor engineDescriptor,
        @NonNull final Cache cache) {

        final ServerConfig serverConfig = serverManager.getServerConfig();

        m_serverManager = serverManager;
        m_cache = cache;
        m_engineInstallPath = engineDescriptor.installPath;

        final long jobTimeoutMilliSeconds = serverConfig.JOB_EXECUTION_TIMEOUT_MILLISECONDS;
        final int engineCount = serverConfig.JOB_PROCESSOR_COUNT;

        m_instanceProvider = new REInstanceProvider(serverManager, this, engineDescriptor, serverConfig.JOB_PROCESSOR_POOLSIZE);

        // Timeout Watchdog
        m_jobWatchdog = new Watchdog("DC JobProcessor watchdog", jobTimeoutMilliSeconds, WatchdogMode.TIMEOUT_AND_DISPOSED);

        // NoResponse timeout
        m_noResponseTimeoutMillis = jobTimeoutMilliSeconds * NO_RESPONSE_TIMEOUT_PERCENTAGE;

        // ErrorCache
        m_errorCache = new ErrorCache(serverConfig.ERROR_CACHE_TIMEOUT_SECONDS * 1000L,
            serverConfig.ERROR_CACHE_MAX_CYCLE_COUNT);

        // initialize and start all engines for Documentconverter
        m_readerEngineExecutorService = Executors.newFixedThreadPool(engineCount);

        // create engine groups
        implInitEngineGroups(engineCount);

        // start all created engine groups
        for (final EngineGroup curGroup : m_engineGroups.values()) {
            curGroup.start();
        }

        // Create monitor for ReaderEngine queue monitoring
        // and time based job removal from scheduled queue
        m_jobMonitor = new JobMonitor(
            m_serverManager,
            this,
            serverConfig.JOB_QUEUE_COUNT_LIMIT_HIGH,
            serverConfig.JOB_QUEUE_COUNT_LIMIT_LOW,
            serverConfig.JOB_QUEUE_TIMEOUT_SECONDS);

        // Start job watchdog
        m_jobWatchdog.start();
}

    /*
     * (non-Javadoc)
     *
     * @see
     * com.openexchange.documentconverter.IJobProcessor#addJob(com.openexchange
     * .documentconverter.IJob)
     */
    @Override
    final public String scheduleJob(IJob job, JobPriority jobPriority, IJobStatusListener jobStatusListener, String jobDescription) {
        String jobId = null;

        if ((null != job) && isRunning()) {
            final JobPriority usedJobPriority = (null != jobPriority) ? jobPriority : JobPriority.MEDIUM;
            final JobEntry newJobEntry = new JobEntry(jobId = UUID.randomUUID().toString(), job, usedJobPriority, jobStatusListener);
            final String finalJobDescription = ((null != jobDescription) && (jobDescription.length() > 0)) ? jobDescription : GENERIC_JOB;

            if (ServerManager.isLogTrace()) {
                final String infoFilename = (String) job.getJobProperties().get(Properties.PROP_INFO_FILENAME);
                ServerManager.logTrace("DC scheduling " + finalJobDescription, new LogData("jobid", jobId), new LogData("doctype", (String) job.getJobProperties().get(Properties.PROP_INPUT_TYPE)), new LogData("filename", (null != infoFilename) ? infoFilename : "unkown"));
            }

            final EngineGroup engineGroup = m_engineGroups.get(job.backendTypeNeeded());

            if (null != engineGroup) {
                // schedule the new JobEntry at the appropriate queue
                engineGroup.m_entryQueue.schedule(newJobEntry);

                if (ServerManager.isLogDebug()) {
                    ServerManager.logDebug("DC job added to " + engineGroup.getGroupName() + " queue", new LogData("jobid", jobId), new LogData("jobs_remaining", Integer.toString(engineGroup.m_entryQueue.size())));
                }
            }
        }

        return jobId;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IJobProcessor#removeJob(long)
     */
    @Override
    final public boolean removeJob(final String jobId) {
        return (StringUtils.isNotBlank(jobId)) ?
            implRemoveJob(jobId, false) :
                false;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IJobProcessor#removeJob(long)
     */
    @Override
    final public boolean removeScheduledJob(final String jobId) {
        return (StringUtils.isNotBlank(jobId)) ?
            implRemoveJob(jobId, true) :
                false;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.openexchange.documentconverter.IJobProcessor#addJobStatusChangeListener
     * (com.openexchange.documentconverter.IJobStatusListener)
     */
    @Override
    final public void addJobStatusChangeListener(final IJobStatusListener listener) {
        m_jobStatusListeners.add(listener);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IJobProcessor#
     * removeJobStatusChangeListener
     * (com.openexchange.documentconverter.IJobStatusListener)
     */
    @Override
    final public void removeJobStatusChangeListener(final IJobStatusListener listener) {
        if (null != listener) {
            m_jobStatusListeners.remove(listener);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.server.impl.WatchdogHandler#notify(long, com.openexchange.documentconverter.JobError)
     */
    @Override
    public void handleWatchdogNotification(final long curTimeMillis, final long startTimeMillis, final JobErrorEx notificationJobErrorEx) {
        if (JobError.TIMEOUT == notificationJobErrorEx.getJobError()) {
            // this handler is called, whenever a scheduled (Ready) entry
            // has reached its 'no response' timeout, which means, that
            // no instance thread has requested the next job within this timeout;
            if (ServerManager.isLogTrace()) {
                ServerManager.logTrace("DC received 'NO RESPONSE' timeout => replacing job threads");
            }

            // check all readerEngine instances against the last time,
            // they took a job from the queue; kill and replace thread
            // with a new copy, if this timeout has been reached
            final EngineGroup readerEngineGroup = m_engineGroups.get(BackendType.READERENGINE);

            for (int i = 0; i < readerEngineGroup.m_engineVector.size(); ++i) {
                final JobThread curJobThread = readerEngineGroup.m_engineVector.get(i);
                final JobEntry curJobEntry = curJobThread.getCurrentJobEntry();

                if ((null != curJobEntry) && (curTimeMillis - startTimeMillis) >= m_noResponseTimeoutMillis) {
                    final JobThread newJobThread = curJobThread.createCopy();

                    if (null != newJobThread) {
                        curJobThread.terminate();
                        readerEngineGroup.m_engineVector.set(i, newJobThread);
                        newJobThread.start();
                    }
                }
            }
        }
    }

    // - Public API ------------------------------------------------------------

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IJobProcessor#terminate()
     */
    final public synchronized void shutdown() {
        if (m_isRunning.compareAndSet(true, false)) {
            final long jobTimeoutMilliSeconds = m_jobWatchdog.getTimeoutMilliSeconds();
            final boolean trace = ServerManager.isLogTrace();
            long traceStartTimeMillis = 0;

            if (trace) {
                traceStartTimeMillis = System.currentTimeMillis();
                ServerManager.logTrace("DC JobProcessor starting shutdown...");
            }

            logStatistics();

            // terminate JobMonitor in order to not accept new jobs anymore
            m_jobMonitor.terminate();

            // terminate RE instance provider
            m_instanceProvider.terminate();

            // shutdown all engine groups
            for (final EngineGroup curGroup : m_engineGroups.values()) {
                curGroup.terminate();
            }

            // interrupt all pending jobs with the help of the Watchdog
            m_jobWatchdog.terminate();

            try {
                m_jobWatchdog.join();
            } catch (@SuppressWarnings("unused") final InterruptedException e) {
                // ok
            }

            // shutdown thread pool
            try {
                m_readerEngineExecutorService.shutdownNow();
                m_readerEngineExecutorService.awaitTermination(jobTimeoutMilliSeconds, TimeUnit.MILLISECONDS);
            } catch (final Exception e) {
                ServerManager.logExcp(e);
            }

            // shudown errorcache
            m_errorCache.terminate();

            // cleanup of all still living OSL_PIPE_* files located
            // in LO/AOO hard coded /tmp or /var/tmp directories
            try {
                File tmpDir = new File("/tmp");

                if (!tmpDir.canWrite()) {
                    tmpDir = new File("/var/tmp");
                }

                if (tmpDir.canWrite()) {
                    final String tmpDirEntries[] = tmpDir.list();

                    if (null != tmpDirEntries) {
                        for (final String curEntry : tmpDirEntries) {
                            if (curEntry.startsWith("OSL_PIPE_")) {
                                FileUtils.deleteQuietly(new File(tmpDir, curEntry));
                            }
                        }
                    }
                }
            } catch (final Exception e) {
                ServerManager.logExcp(e);
            }

            if (trace) {
                ServerManager.logTrace(new StringBuilder(256).
                    append("DC JobProcessor finished shutdown: ").
                    append(System.currentTimeMillis() - traceStartTimeMillis).append("ms").
                    append('!').toString());
            }
        }
    }

    /**
     *
     */
    public IQueueMonitor getQueueMonitor() {
        return m_jobMonitor;
    }

    /**
     * @param _jobType
     * @param jobProperties
     * @param resultProperties
     * @return
     */
    public IJob createServerJob(String _jobType, HashMap<String, Object> jobProperties, HashMap<String, Object> resultProperties) {
        IJob job = null;

        if (null != _jobType) {
            final String jobType = _jobType.toLowerCase();

            jobProperties.put(Properties.PROP_READERENGINE_ROOT, m_engineInstallPath);

            if ("pdf".equals(jobType) || "pdfa".equals(jobType)) {
                jobProperties.put(Properties.PROP_FILTER_SHORT_NAME, "pdf");
                job = new DocumentConverterJob(jobProperties, resultProperties, jobType);
            } else if (jobType.startsWith("pdf2svg")) {
                job = new PDF2SVGConverterJob(jobProperties, resultProperties);
            } else if (jobType.startsWith("pdf2grf")) {
                job = new PDF2GraphicConverterJob(jobProperties, resultProperties);
            } else if (jobType.startsWith("pdf2html")) {
                job = new PDF2HTMLConverterJob(jobProperties, resultProperties);
            } else if ("ooxml".equals(jobType)) {
                jobProperties.put(Properties.PROP_FILTER_SHORT_NAME, "ooxml");
                job = new DocumentConverterJob(jobProperties, resultProperties, jobType);
            } else if ("odf".equals(jobType)) {
                jobProperties.put(Properties.PROP_FILTER_SHORT_NAME, "odf");
                job = new DocumentConverterJob(jobProperties, resultProperties, jobType);
            } else if ("image".equals(jobType)) {
                job = new ImageConverterJob(jobProperties, resultProperties);
            } else if ("graphic".equals(jobType)) {
                job = new GraphicConverterJob(jobProperties, resultProperties);
            } else if ("shape2png".equals(jobType)) {
                job = new Shape2PNGConverterJob(jobProperties, resultProperties);
            } else if ("imagetransformation".equals(jobType)) {
                job = new ImageConverterJob(jobProperties, resultProperties);
            }
        }

        return job;
    }

    /**
     * @return
     */
    public boolean isRunning() {
        return m_isRunning.get();
    }

    /**
     * @param jobId
     * @param jobStartExecTimeMillis
     */
    public void killJob(@NonNull String jobId, @NonNull final JobErrorEx jobErrorEx) {
        final long curTimeMillis = System.currentTimeMillis();

        if (StringUtils.isNotBlank(jobId)) {
            for (final EngineGroup curGroup : m_engineGroups.values()) {
                for (final JobThread curThread : curGroup.m_engineVector) {
                    final JobEntry curEntry = curThread.getCurrentJobEntry();

                    if ((null != curEntry) && jobId.equals(curEntry.getJobId())) {
                        curThread.handleWatchdogNotification(curTimeMillis, curThread.getCurrentStartExecutionTimeMillis(), jobErrorEx);
                        break;
                    }
                }
            }
        }
    }

    /**
     * @return
     */
    final Cache getCache() {
        return m_cache;
    }

    /**
     * @return
     */
    final ErrorCache getErrorCache() {
        return m_errorCache;
    }

    /**
     * @param backendType
     * @return
     */
    final EngineStatus getEngineStatus(@NonNull final BackendType backendType) {
        final EngineGroup engineGroup = m_engineGroups.get(backendType);

        return new EngineStatus() {

            @Override
            public int getTotalCount() {
                return (null != engineGroup) ?
                    engineGroup.getJobThreads().size() :
                        0;
            }

            @Override
            public int getRunningCount() {
                int runningCount = 0;

                if (null != engineGroup) {
                    final Vector<JobThread> jobThreads = engineGroup.getJobThreads();

                    for (final JobThread curJobThread : jobThreads) {
                        if (curJobThread.isAlive() && curJobThread.isRunning()) {
                            ++runningCount;
                        }
                    }
                }

                return runningCount;
            }
        };
    }

    /**
     * @return The internally available executor service
     */
    final ExecutorService getReaderEngineExecutorService() {
        return m_readerEngineExecutorService;
    }

    /**
     * @param jobEntry
     * @param newJobStatus
     */
    final void updateServerJobStatus(final JobEntry jobEntry, final JobStatus newJobStatus) {
        if (null != jobEntry) {
            final JobStatus oldJobStatus = jobEntry.getJobStatus();

            if (newJobStatus != oldJobStatus) {
                final String jobId = jobEntry.getJobId();

                // immediately set new job status, so that all
                // following calls will work with the correct, new
                // JobStatus set at the currently updated JobEntry
                jobEntry.setJobStatus(newJobStatus);

                if (JobStatus.SCHEDULED == newJobStatus) {
                    m_jobMonitor.jobScheduled(jobEntry);
                } else if (JobStatus.SCHEDULED == oldJobStatus) {
                    m_jobMonitor.jobSchedulingFinished(jobEntry);
                }

                // notify all registered listeners
                synchronized (m_jobStatusListeners) {
                    for (final IJobStatusListener listener : m_jobStatusListeners) {
                        if (null != listener) {
                            listener.statusChanged(jobId, jobEntry);
                        }
                    }
                }

                // notify listener, set at JobEntry itself
                final IJobStatusListener jobStatusListener = jobEntry.getJobStatusListener();

                if (null != jobStatusListener) {
                    jobStatusListener.statusChanged(jobId, jobEntry);
                }
            }
        }
    }

    /**
     * Blocking (!) call to retrieve the next available job with
     * JobStatus.SCHEDULED
     *
     * @return the next available jobEntry or null in case of termination
     */
    final JobEntry getNextJobEntryToProcess(@NonNull final BackendType backendType) {
        final EngineGroup engineGroup = m_engineGroups.get(backendType);

        return ((null != engineGroup) ? engineGroup.m_entryQueue.getNextJobEntryToProcess() : null);
    }

    /**
     * @return the next available jobEntry or null in case of termination
     */
    final void finishedJobEntry(@NonNull final BackendType backendType,
        @NonNull final JobEntry jobEntry,
        @NonNull final JobStatus jobStatus) {

        final EngineGroup engineGroup = m_engineGroups.get(backendType);

        if (null != engineGroup) {
            engineGroup.m_entryQueue.finishedJobEntry(jobEntry, jobStatus);
        }
    }

    /**
     * write out some fundamental statistics when processor is going to be shut down
     */
    private void logStatistics() {
        if (ServerManager.isLogInfo()) {
            final LTStatistics statistics = m_serverManager.getStatistics();
            final double cacheHitRatio = (Math.floor(statistics.getCacheHitRatio() * 10000.0) / 100.0);
            final double errorCacheHitRatio = (Math.floor(statistics.getErrorCacheHitRatio() * 10000.0) / 100.0);
            final StringBuilder statisticsBuilder = new StringBuilder(1024).append("DC statistics");

            statisticsBuilder.
                append("; jobsProcessed: ").append(statistics.getJobsProcessed()).
                append("; jobErrors: ").append(statistics.getJobErrorsTotal()).
                append("; conversions: ").append(statistics.getConversionCount(null)).
                append("; conversionsReaderEngine: ").append(statistics.getConversionCount(BackendType.READERENGINE)).
                append("; conversionsPDFTool: ").append(statistics.getConversionCount(BackendType.PDFTOOL)).
                append("; conversionsMultiRun: ").append(statistics.getConversionCountWithMultipleRuns()).
                append("; conversionErrorsGeneral: ").append(statistics.getConversionErrorsGeneral()).
                append("; conversionErrorsTimeout: ").append(statistics.getConversionErrorsTimeout()).
                append("; conversionErrorsDisposed: ").append(statistics.getConversionErrorsDisposed()).
                append("; conversionErrorsPassword: ").append(statistics.getConversionErrorsPassword()).
                append("; conversionErrorsNoContent: ").append(statistics.getConversionErrorsNoContent()).
                append("; cacheHits: ").append(statistics.getCacheHits()).
                append("; cacheRatio: ").append(Double.toString(cacheHitRatio)).append("%").
                append("; errorCacheHits: ").append(statistics.getErrorCacheHits()).
                append("; errorCacheRatio: ").append(Double.toString(errorCacheHitRatio)).append("%");

            ServerManager.logInfo(statisticsBuilder.toString());
        }
    }

    // - Implementation --------------------------------------------------------

    /**
     * @param descriptor
     * @param engineCount
     */
    private void implInitEngineGroups(final int engineCount) {
        // ReaderEngine
        final Vector<JobThread> engineVector = new Vector<>(engineCount);

        for (int i = 0; i < engineVector.capacity(); ++i) {
            engineVector.add(new REThread(m_serverManager, this, m_instanceProvider, m_cache, m_errorCache, m_jobWatchdog));
        }

        m_engineGroups.put(BackendType.READERENGINE, new EngineGroup(engineVector, new JobEntryQueue(this)));

        // PDFTool
        final Vector<JobThread> pdfToolVector = new Vector<>(engineCount);

        for (int i = 0; i < pdfToolVector.capacity(); ++i) {
            pdfToolVector.add(new PDFToolThread(m_serverManager, this, m_cache, m_errorCache, m_jobWatchdog));
        }

        m_engineGroups.put(BackendType.PDFTOOL, new EngineGroup(pdfToolVector, new JobEntryQueue(this)));
    }

    /**
     * @param jobId
     * @param scheduledOnly
     */
    private boolean implRemoveJob(@NonNull final String jobId, boolean scheduledOnly) {
        final boolean trace = ServerManager.isLogTrace();

        for (final EngineGroup curGroup : m_engineGroups.values()) {
            if ((null != curGroup.m_entryQueue.remove(jobId, scheduledOnly))) {
                if (trace) {
                    ServerManager.logTrace(new StringBuilder(256).
                        append("DC removed ").append(scheduledOnly ? "scheduled " : "").append("job from ").
                        append(curGroup.getGroupName()).append(" queue").toString(),
                        new LogData("jobid", jobId),
                        new LogData("jobs_remaining", Integer.toString(curGroup.m_entryQueue.size())));
                }

                return true;
            }
        }

        return false;
    }

    // -------------------------------------------------------------------------

    final protected ServerManager m_serverManager;

    final private Cache m_cache;

    final private String m_engineInstallPath;

    final protected REInstanceProvider m_instanceProvider;

    final private Watchdog m_jobWatchdog;

    final private JobMonitor m_jobMonitor;

    final private ErrorCache m_errorCache;

    final private ExecutorService m_readerEngineExecutorService;

    final private HashMap<BackendType, EngineGroup> m_engineGroups = new HashMap<>();

    final private List<IJobStatusListener> m_jobStatusListeners = Collections.synchronizedList(new LinkedList<>());

    final private AtomicBoolean m_isRunning = new AtomicBoolean(true);

    final private long m_noResponseTimeoutMillis;
}

