/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import com.openexchange.documentconverter.DocumentConverterUtil;
import com.openexchange.documentconverter.JobError;
import com.openexchange.documentconverter.JobErrorEx;
import com.openexchange.documentconverter.LogData;
import com.openexchange.documentconverter.MutableWrapper;
import com.openexchange.documentconverter.NonNull;
import com.openexchange.documentconverter.Properties;
import com.openexchange.documentconverter.impl.api.BackendType;
import com.openexchange.documentconverter.impl.api.ConverterStatus;
import com.openexchange.documentconverter.impl.api.ICacheEntry;
import com.openexchange.documentconverter.impl.api.IJob;
import com.openexchange.documentconverter.impl.api.IdLocker;
import com.openexchange.documentconverter.impl.api.JobEntry;
import com.openexchange.documentconverter.impl.api.JobStatus;
import com.openexchange.exception.ExceptionUtils;
import com.sun.star.lang.DisposedException;

/**
 * {@link JobThread}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.6.0
 */
abstract class JobThread extends Thread implements WatchdogHandler {

    /**
     * Initializes a new {@link JobThread}.
     *
     * @param jobProcessor
     * @param cache
     * @param timeoutWatchdog
     * @param threadName
     */
    JobThread(
        @NonNull final ServerManager serverManager,
        @NonNull final JobProcessor jobProcessor,
        @NonNull BackendType backendType,
        @NonNull final Cache cache,
        @NonNull final ErrorCache errorCache,
        @NonNull final Watchdog watchdog) {

        super();

        m_serverManager = serverManager;
        m_jobProcessor = jobProcessor;
        m_backendType = backendType;
        m_cache = cache;
        m_errorCache = errorCache;
        m_watchdog = watchdog;
    }

    /**
     * The number of backend runs, the
     * actual thread is supporting. If this number is
     * greater than 1 (default), the backend processing
     * loop will run for this number of times
     * in total when a {@link JobError#GENERAL} or
     * {@link JobError#DISPOSED} happened during
     * the last run. In this case, the
     * {@link JobThread#endProcessingJob(JobEntry)}
     * method will not be called before the total
     * run count has been reached.
     *
     * @return The number of supported runs
     */
    Map<JobError, Integer> getSupportedBackendRuns() {
        return ServerManager.getDefaultSupportedBackendRuns();
    }

    /**
     * @return
     */
    abstract JobThread createCopy();

    /**
     * @return The type of currently supported backend
     */
    BackendType getBackendType() {
        return m_backendType;
    }

    /**
     * set the termination flag and interrupt the thread to terminate ASAP
     */
    void terminate() {
        if (m_isRunning.compareAndSet(true, false)) {
            final boolean trace = ServerManager.isLogTrace();
            final String threadName = getName();
            long traceStartTimeMillis = 0;

            if (trace) {
                traceStartTimeMillis = System.currentTimeMillis();
                ServerManager.logTrace(getLogBuilder().
                    append("starting shutdown ").append(threadName).
                    append("...").toString());
            }

            shutdownCurrentInstance();
            interrupt();

            if (trace) {
                ServerManager.logTrace(getLogBuilder().
                    append("finished shutdown ").append(threadName).append(": ").
                    append(System.currentTimeMillis() - traceStartTimeMillis).append("ms").
                    append('!').toString());
            }
        }
    }

    /**
     * set the termination flag and interrupt the thread to terminate ASAP
     */
    boolean isRunning() {
        return m_isRunning.get();
    }

    /**
     * shutdown internal backend
     */
    void shutdownCurrentInstance() {
        m_instanceJobsProcessed = 0;
        setJobExecutionData(null);
    }

    /**
     * @return The current JobEhtry, that is processed or null
     */
    JobEntry getCurrentJobEntry() {
        return m_curJobEntry;
    }

    /**
     * @return
     */
    long getCurrentStartExecutionTimeMillis() {
        return m_currentStartExecutionTimeMillis.get();
    }

    // - Thread ----------------------------------------------------------------

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Thread#run()
     */
    @Override
    public void run() {
        final MutableWrapper<JobErrorEx> jobErrorExWrapper = new MutableWrapper<>(new JobErrorEx());
        JobEntry curJobEntry = null;

        if (ServerManager.isLogInfo()) {
            ServerManager.logInfo(getLogBuilder().append("running").toString());
        }

        while (isRunning()) {
            try {
                // prepare processing job
                if (!prepareProcessingJob()) {
                    handlePrepareProcessingJobFailed();
                    continue;
                }

                // start processing job
                if (startProcessingJob() && (null != (curJobEntry = m_curJobEntry))) {
                    final String jobLocale = curJobEntry.getJobLocale();
                    final boolean debug = ServerManager.isLogDebug();
                    final boolean trace = ServerManager.isLogTrace();

                    m_currentStartExecutionTimeMillis.set(m_watchdog.addWatchdogHandler(this));
                    jobErrorExWrapper.set(new JobErrorEx());

                    if (trace) {
                        ServerManager.logTrace(
                            getLogBuilder().append("processing job").toString(),
                            curJobEntry.getJobProperties(),
                            new LogData("jobid", curJobEntry.getJobId()),
                            new LogData("hash", curJobEntry.getHash()),
                            !StringUtils.isEmpty(jobLocale) ? new LogData("logtext", jobLocale) : null);
                    }

                    try {
                        // set locale for current job
                        setLocale(jobLocale);

                        if (trace) {
                            ServerManager.logTrace(
                                getLogBuilder().append("started processing job").toString());
                        }

                        if (!executeJob(curJobEntry, m_jobExecutionData)) {
                            jobErrorExWrapper.set(curJobEntry.getJobErrorEx());

                            if (jobErrorExWrapper.get().hasNoError()) {
                                jobErrorExWrapper.set(new JobErrorEx(JobError.GENERAL));
                                curJobEntry.setJobErrorEx(jobErrorExWrapper.get());
                            }

                            if (trace) {
                                ServerManager.logTrace(getLogBuilder().
                                    append("error processing job (Execute path): ").
                                    append(System.currentTimeMillis() - m_currentStartExecutionTimeMillis.get()).append("ms").toString());
                            }
                        } else if (trace) {
                            ServerManager.logTrace(getLogBuilder().
                                append("finished processing job: ").
                                append(System.currentTimeMillis() - m_currentStartExecutionTimeMillis.get()).append("ms").toString());
                        }
                    } catch (Throwable e) {
                        ExceptionUtils.handleThrowable(e);

                        jobErrorExWrapper.set(curJobEntry.getJobErrorEx());

                        if (jobErrorExWrapper.get().hasNoError()) {
                            final JobErrorEx exceptionJobErrorEx = new JobErrorEx((e instanceof DisposedException) ? JobError.DISPOSED : JobError.GENERAL);

                            jobErrorExWrapper.set(exceptionJobErrorEx);
                            curJobEntry.setJobErrorEx(exceptionJobErrorEx);
                        }

                        if (trace) {
                            ServerManager.logTrace(getLogBuilder().
                                append("error processing job (Exception path): ").
                                append(System.currentTimeMillis() - m_currentStartExecutionTimeMillis.get()).append("ms").toString(),
                                new LogData("exception", e.getMessage()));
                        }
                    }

                    // immediately remove watchdog handler
                    m_watchdog.removeWatchdogHandler(this);

                    // increase number of processed jobs for the currently used ReaderEngine instance
                    ++m_instanceJobsProcessed;

                    // check all locations for a possibly set error code
                    JobErrorEx resultingJobErrorEx = jobErrorExWrapper.get();

                    if (resultingJobErrorEx.hasNoError() && curJobEntry.getJobErrorEx().hasError()) {
                        resultingJobErrorEx = curJobEntry.getJobErrorEx();
                    }

                    // check, if the used thread supports more than one run in case of an
                    // error => if yes, reset job error and backend instance and perform the
                    // next run until the maximum count of runs (error dependent) is reached
                    if (resultingJobErrorEx.hasError()) {
                        final int maxRuns = getSupportedBackendRuns().get(resultingJobErrorEx.getJobError()).intValue();

                        if ((m_curBackendRun < maxRuns) && isRunning()) {
                            if (trace) {
                                ServerManager.logTrace(getLogBuilder().
                                    append("will reuse current job for followup run #").append(m_curBackendRun + 1).toString(),
                                    curJobEntry.getJobProperties(),
                                    new LogData("joberror", resultingJobErrorEx.getJobError().getErrorText()),
                                    new LogData("jobid", curJobEntry.getJobId()),
                                    new LogData("hash", curJobEntry.getHash()));
                            }

                            shutdownCurrentInstance();
                            curJobEntry.setJobErrorEx(new JobErrorEx());
                        } else {
                            curJobEntry.setJobErrorEx(resultingJobErrorEx);
                            m_curBackendRun = 0;

                            // job error, but no more runs to complete  => cleanup, notifying, cache writing etc.
                            endProcessingJob(curJobEntry);
                        }
                    } else {
                        // job ok with no error => cleanup, notifying, cache writing etc.
                        endProcessingJob(curJobEntry);
                    }

                    if (debug) {
                        ServerManager.logDebug(getLogBuilder().
                            append("released job").toString(),
                            new LogData("jobid", curJobEntry.getJobId()),
                            new LogData("hash", curJobEntry.getHash()));
                    }

                    if (trace) {
                        ServerManager.logTrace(getLogBuilder().
                            append("finished job run: ").
                            append(System.currentTimeMillis() - m_currentStartExecutionTimeMillis.get()).append("ms").toString());
                    }

                    m_currentStartExecutionTimeMillis.set(0);
                }
            } catch (Exception e) {
                ServerManager.logExcp(e);
            }
        }

        curJobEntry = null;
        shutdownCurrentInstance();

        if (ServerManager.isLogInfo()) {
            ServerManager.logInfo(getLogBuilder().append("finished").toString());
        }
    }

    // - WatchdogHandler --------------------------------------------------------

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.tools.WatchdogHandler#handleWatchdogNotification(long, long, com.openexchange.documentconverter.JobError)
     */
    @Override
    public final void handleWatchdogNotification(final long curTimeMillis, final long startTimeMillis, final JobErrorEx notificationJobErrorEx) {
        final JobEntry curJobEntry = m_curJobEntry;

        if (null != curJobEntry) {
            curJobEntry.setJobErrorEx(notificationJobErrorEx);

            try {
                // notify processor that an error has happened
                shutdownCurrentInstance();

                if (ServerManager.isLogTrace()) {
                    ServerManager.logTrace(getLogBuilder().
                        append((JobError.TIMEOUT == notificationJobErrorEx.getJobError()) ? "TMEOUT reached" :"disposed").
                        append(" => job has been killed").toString(),
                        new LogData("jobid", curJobEntry.getJobId()),
                        new LogData("hash", curJobEntry.getHash()),
                        new LogData("exectime", Long.toString(curTimeMillis - startTimeMillis) + "ms"));
                }
            } catch (Throwable e) {
                ExceptionUtils.handleThrowable(e);

                ServerManager.logExcp(new Exception(getLogBuilder().
                    append("exception caught during Watchdog abort notification: ").
                    append(notificationJobErrorEx.getJobError().getErrorText()).toString()));
            }
        }
    }

    // - Implementation --------------------------------------------------------

    /**
     * @return
     */
    protected long getInstanceJobsProcessed() {
        return m_instanceJobsProcessed;
    }

    /**
     * @return
     */
    protected int getRestartCount() {
        return 1;
    }

    /**
     * @return
     */
    protected boolean prepareProcessingJob() {
        return true;
    }

    /**
     *
     */
    protected void handlePrepareProcessingJobFailed() {
        // nothing to be done here
    }

    /**
     * @return
     */
    protected boolean startProcessingJob() {
        boolean ret = false;

        // just use the currently set job entry in case of a retry
        // or wait until we get a next job from the queue via calling
        // the appropriate, blocking JobProcessor method
        if (null == m_curJobEntry) {
            m_curBackendRun = 1;
            m_curJobEntry = m_jobProcessor.getNextJobEntryToProcess(getBackendType());

            if (null != m_curJobEntry) {
                if (ServerManager.isLogDebug()) {
                    ServerManager.logDebug(getLogBuilder().
                        append("acquired job for run #1").toString(),
                        new LogData("jobid", m_curJobEntry.getJobId()),
                        new LogData("hash", m_curJobEntry.getHash()));
                }

                // increment number of totally processed jobs for appropriate BackendType before first run only;
                // subseqent backend runs for the same job will not increment this number
                m_serverManager.getStatistics().incrementConversionCount(m_backendType);

                // always try to process a result from cache or
                // errorcache first, if this is the first backend run
                if (JobStatus.INVALID != processCachedJobEntry(m_jobProcessor, m_curJobEntry)) {
                    endProcessingJob(m_curJobEntry);
                    m_curJobEntry = null;
                } else {
                    ret = true;
                }
            }
        } else {
            ret = true;

            // increment run count and increment statistics counter, if run count reached '2'
            if (2 == ++m_curBackendRun) {
                m_serverManager.getStatistics().incrementConversionWithMultipleRunsCount();
            }

            if (ServerManager.isLogDebug()) {
                ServerManager.logDebug(getLogBuilder().
                    append("reusing job for run #").append(m_curBackendRun).toString(),
                    new LogData("jobid", m_curJobEntry.getJobId()),
                    new LogData("hash", m_curJobEntry.getHash()));
            }
        }

        return ret;
    }

    /**
     * @param jobLocale The new job locale to be set as instance locale for the upcoming processing of jobs. The instance locale is set to a
     *            valid locale in any case after this call.
     */
    protected String setLocale(String jobLocale) {
        return ((null != jobLocale) && (jobLocale.length() > 1)) ? jobLocale : "en_US";
    }

    /**
     * @param executionData User data to be set before the job is executed.
     *            The data is given to the various execute calls as parameter
     */
    protected void setJobExecutionData(Object executionData) {
        m_jobExecutionData = executionData;
    }

    /**
     *
     */
    protected synchronized void endProcessingJob(JobEntry curJobEntry) {
        if (null != curJobEntry) {
            final ErrorCache errorCache = m_jobProcessor.getErrorCache();
            final JobErrorEx jobErrorEx = curJobEntry.getJobErrorEx();
            final boolean hasError = jobErrorEx.hasError();
            final JobStatus jobStatus = hasError ? JobStatus.ERROR : JobStatus.FINISHED;

            // a possible cache entry has been written by the JobEntryExecutor,
            // just add an error to the errorcache here if not already contained
            if (1 == errorCache.notifyJobFinish(curJobEntry) || (hasError && !errorCache.isEnabled())) {
                saveErrorFile(curJobEntry);
            }

            // update Statistics in case of error
            if (hasError) {
                // Increment conversion error statistics (<= increment conversion count for specific JobError)
                m_serverManager.getStatistics().incrementConversionErrorCount(jobErrorEx.getJobError());
            }

            m_jobProcessor.finishedJobEntry(getBackendType(), curJobEntry, jobStatus);
        }

        m_curJobEntry = null;
    }

    /**
     * @param jobEntry
     * @param jobExecutionData
     * @return
     * @throws com.sun.star.lang.DisposedException
     * @throws InterruptedException
     * @throws IOException
     * @throws MalformedURLException
     * @throws Exception
     */
    protected boolean executeJob(@NonNull JobEntry jobEntry, Object jobExecutionData) throws com.sun.star.lang.DisposedException, InterruptedException, IOException, MalformedURLException, Exception {
        IJob job = jobEntry.getJob();
        boolean ret = false;

        if (null != job) {
            String hash = job.getHash();
            final boolean serverJobCachable = Cache.isValidHash(hash) && m_cache.isEnabled();

            if (!serverJobCachable) {
                // if the cache can't handle new cache entry with given hash
                // (e.g. invalid hash or cache is still filled up in the
                // background and thus currently disabled) =>
                // use unique hash id and don't cache at end of job execution
                hash = jobEntry.getJobId();
            }

            if (!Cache.isValidHash(hash)) {
                ServerManager.logWarn(getLogBuilder().
                    append("executeJob call with invalid hash not possible").toString());

                return false;
            }

            ICacheEntry newCacheEntry = null;

            IdLocker.lock(hash);

            try {
                boolean addedToCache = false;

                if (null != (newCacheEntry = m_cache.startNewEntry(hash))) {
                    final HashMap<String, Object> additionalJobProperties = new HashMap<>(4);
                    final boolean trace = ServerManager.isLogTrace();
                    final long traceStartTimeMillis = trace ? System.currentTimeMillis() : 0;

                    // set property to not close document, since this
                    // is the last conversion for the current instance
                    if ((m_instanceJobsProcessed + 1) >= getRestartCount()) {
                        additionalJobProperties.put(Properties.PROP_CLOSE_DOCUMENT, Boolean.FALSE);
                    }

                    // finally, set output file property at job
                    additionalJobProperties.put(Properties.PROP_OUTPUT_FILE, newCacheEntry.getResultFile());

                    job.addJobProperties(additionalJobProperties);

                    final File jobInputFile = (File) job.getJobProperties().get(Properties.PROP_INPUT_FILE);

                    if (trace) {
                        ServerManager.logTrace(getLogBuilder().append("started executing job").toString());
                    }

                    ConverterStatus converterStatus = ConverterStatus.ERROR;

                    try {
                        // execute the job, but don't try a job execution at all,
                        // if an input file is set, but has a file length of 0 bytes
                        if ((null == jobInputFile) || (jobInputFile.length() > 0)) {
                            converterStatus = job.execute(jobExecutionData);

                            ret = (ConverterStatus.ERROR != converterStatus);

                            if (trace) {
                                ServerManager.logTrace(getLogBuilder().
                                    append("finished executing job: ").
                                    append(System.currentTimeMillis() - traceStartTimeMillis).append("ms").toString());
                            }
                        }
                    } catch (Exception e) {
                        if (trace) {
                            ServerManager.logTrace(getLogBuilder().
                                append("error executing job: ").
                                append(System.currentTimeMillis() - traceStartTimeMillis).append("ms").toString());
                        }

                        // rethrow Exception in case something bad happened
                        throw e;
                    } finally {
                        final HashMap<String, Object> jobResultProperties = job.getResultProperties();

                        // if succeeded job has a valid cache set, persistently write result to cache;
                        // if not, the result property already contains all information
                        if (ret && serverJobCachable) {
                            // make result properties persistent at existing location and add to cache
                            if (newCacheEntry.updateAndMakePersistent(jobResultProperties)) {
                                final String filename = (String) job.getJobProperties().get(Properties.PROP_INFO_FILENAME);

                                addedToCache = m_cache.addEntry(newCacheEntry, true, filename);
                            }
                        } else if (jobResultProperties.containsKey(Properties.PROP_RESULT_ERROR_CODE)) {
                            jobEntry.setJobErrorEx(JobErrorEx.fromResultProperties(jobResultProperties));
                        }

                        if (!addedToCache) {
                            if (null != jobInputFile) {
                                // remove lock file from just killed conversion process
                                FileUtils.deleteQuietly(new File(jobInputFile.getParentFile(), ".~lock." + jobInputFile.getName() + "#"));
                            }

                            // clear just created cache file
                            newCacheEntry.clear();
                        }
                    }
                }
            } finally {
                if (null != newCacheEntry) {
                    m_cache.endNewEntry(newCacheEntry);
                }

                IdLocker.unlock(hash);
            }
        }

        return ret;
    }

    // - Statics interface -----------------------------------------------------

    /**
     * @param jobEntry
     * @return The <code>JobStatus</code> from a cached entry or {@link JobStatus.INVALID}
     *         if result could not be retrieved from cache
     */
    JobStatus processCachedJobEntry(JobProcessor jobProcessor, JobEntry jobEntry) {
        JobStatus ret = JobStatus.INVALID;

        // always try to retrieve a result from result or errorcache first
        if (null != jobEntry) {
            final IJob job = jobEntry.getJob();
            final Cache cache = jobProcessor.getCache();
            final String hash = job.getHash();
            final String filename = (String) jobEntry.getJobProperties().get(Properties.PROP_INFO_FILENAME);
            final JobErrorEx cachedJobErrorEx = jobProcessor.getErrorCache().getJobErrorExForHash(hash);

            if (null == cachedJobErrorEx) {
                try (final InputStream cachedResultStm = ServerManager.getCachedResult(cache, hash, filename, job.getResultProperties(), false)) {
                    if (null != cachedResultStm) {
                        jobEntry.setCacheHit();
                        ret = JobStatus.FINISHED;

                        if (ServerManager.isLogTrace()) {
                            ServerManager.logTrace(getLogBuilder().
                                append("cache hit in job thread").toString(),
                                new LogData("jobid", jobEntry.getJobId()),
                                new LogData("hash", hash),
                                new LogData("filename", (null != filename) ? filename : DocumentConverterUtil.STR_UNKNOWN));
                        }
                    }
                } catch (final IOException e) {
                    ServerManager.logExcp(e);
                }
            } else {
                jobEntry.setErrorCacheHit(cachedJobErrorEx);
                ret = JobStatus.ERROR;

                if (ServerManager.isLogTrace()) {
                    ServerManager.logTrace(getLogBuilder().
                        append("errorcache hit in job thread").toString(),
                        new LogData("jobid", jobEntry.getJobId()),
                        new LogData("hash", hash),
                        new LogData("filename", (null != filename) ? filename : DocumentConverterUtil.STR_UNKNOWN));
                }
            }
        }

        return ret;
    }

    /**
     * @param entry
     */
    protected void saveErrorFile(@NonNull JobEntry entry) {
        final File errorDir = new File(m_serverManager.getServerConfig().ERRORDIR, "error");
        final JobErrorEx jobErrorEx = entry.getJobErrorEx();

        if (errorDir.canWrite() && !SAVE_JOBERROR_EXCLUDE_LIST.contains(jobErrorEx)) {
            final IJob iJob = entry.getJob();
            final HashMap<String, Object> jobProperties = (null != iJob) ? iJob.getJobProperties() : null;

            if ((null != jobProperties) && (null != entry.getJobId()) && jobProperties.containsKey(Properties.PROP_INPUT_FILE)) {
                final String jobHash = entry.getHash();
                final StringBuilder fileNameBuilder = new StringBuilder(Cache.isValidHash(jobHash) ? jobHash : entry.getJobId());

                // add suffix to jobId, if known at all
                if (jobProperties.containsKey(Properties.PROP_INFO_FILENAME)) {
                    final String suffix = FilenameUtils.getExtension((String) jobProperties.get(Properties.PROP_INFO_FILENAME));

                    if (null != suffix) {
                        fileNameBuilder.append('.').append(suffix);
                    }
                }

                final File timeoutDir = new File(m_serverManager.getServerConfig().ERRORDIR, "timeout");
                final File destFile = new File((JobError.TIMEOUT == jobErrorEx.getJobError()) ? timeoutDir : errorDir, fileNameBuilder.toString());

                try {
                    Files.copy(((File) jobProperties.get(Properties.PROP_INPUT_FILE)).toPath(), FileUtils.openOutputStream(destFile));
                } catch (final IOException e) {
                    ServerManager.logExcp(e);
                }
            }
        }
    }

    /**
     * @return
     */
    protected StringBuilder getLogBuilder() {
        return new StringBuilder(256).append("DC ").append(m_backendType.toString()).append(" JobThread ");
    }

    // - Members ---------------------------------------------------------------

    protected final ServerManager m_serverManager;

    protected final JobProcessor m_jobProcessor;

    protected final BackendType m_backendType;

    protected final Cache m_cache;

    protected final ErrorCache m_errorCache;

    protected final Watchdog m_watchdog;

    protected final  AtomicLong m_currentStartExecutionTimeMillis = new AtomicLong(0);

    protected final AtomicBoolean m_isRunning = new AtomicBoolean(true);

    protected volatile JobEntry m_curJobEntry = null;

    private Object m_jobExecutionData = null;

    private long m_instanceJobsProcessed = 0;

    private int m_curBackendRun = 0;

    // - Static members --------------------------------------------------------

    final private static Set<JobErrorEx> SAVE_JOBERROR_EXCLUDE_LIST = new HashSet<>(Arrays.asList(
        new JobErrorEx(JobError.PASSWORD),
        new JobErrorEx(JobError.NO_CONTENT)));
}
