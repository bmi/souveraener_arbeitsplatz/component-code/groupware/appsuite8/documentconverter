/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import com.google.common.base.Throwables;
import com.google.gson.Gson;
import com.openexchange.documentconverter.DocumentInformation;
import com.openexchange.documentconverter.DocumentType;
import com.openexchange.documentconverter.JobError;
import com.openexchange.documentconverter.JobErrorEx;
import com.openexchange.documentconverter.LogData;
import com.openexchange.documentconverter.NonNull;
import com.openexchange.documentconverter.Nullable;
import com.openexchange.documentconverter.Properties;

/**
 * {@link LTStatistics}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.0
 */
public class LTStatistics extends Statistics {

    /**
     * LTS_FILENAME
     */
    final private static String LTS_FILENAME = "oxdclts.data";

    /**
     * JOB_ERRORS_FAILURE
     */
    final private static JobError[] JOB_ERRORS_FAILURE = {
        JobError.GENERAL,
        JobError.TIMEOUT,
        JobError.DISPOSED,
        JobError.UNSUPPORTED,
        JobError.OUT_OF_MEMORY,
        JobError.PDFTOOL,
        JobError.MAX_QUEUE_COUNT,
        JobError.QUEUE_TIMEOUT
    };

    /**
     * {@link LTData}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v7.10.0
     */
    protected final static class LTData {

        final public static int VERSION = 3;

        final public static int VERSION_STATUS_COUNTS_ADDED = 2;

        final public static int VERSION_QUEUE_LIMITS_ADDED = 3;

        /**
         * Initializes a new {@link LTData}.
         */
        protected LTData() {
            super();
        }

        // - Members -----------------------------------------------------------

        protected int version = VERSION;

        protected AtomicLong instanceRuntimeMillis = new AtomicLong(0);

        protected AtomicLong longTermRuntimeMillis = new AtomicLong(0);

        transient protected AtomicLong persistentRuntimeMillis = new AtomicLong(0);

        protected Map<DocumentType, Map<JobError, AtomicLong>> instanceStatusCount = new HashMap<>();

        protected Map<DocumentType, Map<JobError, AtomicLong>> longTermStatusCount = new HashMap<>();

        transient protected Map<DocumentType, Map<JobError, AtomicLong>> persistentStatusCount = new HashMap<>();

        protected AtomicLong instanceQueueCountLimitHighReached = new AtomicLong(0);

        protected AtomicLong longTermQueueCountLimitHighReached = new AtomicLong(0);

        transient protected AtomicLong persistentQueueCountLimitHighReached = new AtomicLong(0);

        protected AtomicLong instanceQueueCountLimitLowReached = new AtomicLong(0);

        protected AtomicLong longTermQueueCountLimitLowReached = new AtomicLong(0);

        transient protected AtomicLong persistentQueueCountLimitLowReached = new AtomicLong(0);

        protected AtomicLong instanceQueueTimeoutCount = new AtomicLong(0);

        protected AtomicLong longTermQueueTimeoutCount = new AtomicLong(0);

        transient protected AtomicLong persistentQueueTimeoutCount = new AtomicLong(0);
    }

    /**
     * Initializes a new {@link LTStatistics}.
     *
     * @param manager
     */
    public LTStatistics(@NonNull ServerManager serverManager, @Nullable final JobProcessor processor, @Nullable final Cache cache) {
        super(serverManager, processor, cache);

        final String ltsDir = m_serverManager.getServerConfig().LTSDIR;

        m_ltsFile = new File(StringUtils.isNotEmpty(ltsDir) ? ltsDir : "/tmp", LTS_FILENAME);

        DocumentType.forEach(curDocumentType -> {
            Map<JobError, AtomicLong> instanceMap = new HashMap<>();
            Map<JobError, AtomicLong> longTermMap = new HashMap<>();
            Map<JobError, AtomicLong> persistentMap = new HashMap<>();

            JobError.forEach(curJobError -> {
                instanceMap.put(curJobError, new AtomicLong(0));
                longTermMap.put(curJobError, new AtomicLong(0));
                persistentMap.put(curJobError, new AtomicLong(0));
            });

            m_ltData.instanceStatusCount.put(curDocumentType, instanceMap);
            m_ltData.longTermStatusCount.put(curDocumentType, longTermMap);
            m_ltData.persistentStatusCount.put(curDocumentType, persistentMap);

        });

        final File ltsParentDir = m_ltsFile.getParentFile();

        // create scratch directory in every case in order to created temp. files within
        try {
            FileUtils.forceMkdir(ltsParentDir);
        } catch (@SuppressWarnings("unused") final IOException e) {
            //
        }

        if (ltsParentDir.canWrite()) {
            try {
                implReadPersistentData();
                implResetInstanceData();
            } catch (Exception e) {
                ServerManager.logError("DC long term statistics error when reading statistics file: " + m_ltsFile.getPath(),
                    new LogData("rootCause", Throwables.getRootCause(e).getMessage()));
            }
        } else {
            m_ltsFile = null;

            if (ServerManager.isLogError()) {
                ServerManager.logWarn("DC long term statistics directory could not be created or is write protected: " + ltsParentDir);
            }
        }
    }

    /**
     *
     */
    @Override
    public void terminate() {
        if (m_isRunnning.compareAndSet(true, false)) {
            try {
                implWritePersistentData();
            } catch (Exception e) {
                ServerManager.logExcp(e);
            }
        }

        super.terminate();
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.documentconverter.IJobStatusListener#statusChanged(java .lang.String)
     */
    public synchronized void incrememtUserRequestCount(@Nullable HashMap<String, Object> jobProperties, @Nullable HashMap<String, Object> resultProperties) {
        if ((null != jobProperties) && (null != resultProperties)) {
            final String inputType = (String) jobProperties.get(Properties.PROP_INPUT_TYPE);
            final DocumentInformation documentInformation = DocumentInformation.createFromExtension(inputType);
            final DocumentType documentType = (null != documentInformation) ?
                documentInformation.getDocumentType() :
                    null;

            if (null != documentType) {
                final Boolean isUserRequest = (Boolean) jobProperties.get(Properties.PROP_USER_REQUEST);

                if ((null != isUserRequest) &&
                    isUserRequest.booleanValue() &&
                    resultProperties.containsKey(Properties.PROP_RESULT_ERROR_CODE)) {

                    m_ltData.instanceStatusCount.get(documentType).
                        get(JobErrorEx.fromResultProperties(resultProperties).getJobError()).
                            incrementAndGet();
                }
            } else if (ServerManager.isLogDebug()) {
                ServerManager.logDebug("DC detected user request without proper source input type set", jobProperties);
            }
        }
    }

    /**
     * incrementQueueCountLimitHighReached
     *
     */
    public void incrementQueueCountLimitHighReached() {
        m_ltData.instanceQueueCountLimitHighReached.incrementAndGet();
    }

    /**
     * incrementQueueCountLimitLowReached
     *
     */
    public void incrementQueueCountLimitLowReached() {
        m_ltData.instanceQueueCountLimitLowReached.incrementAndGet();
    }

    /**
     * incrementQueueTimeoutCount
     *
     */
    public void incrementQueueTimeoutCount() {
        m_ltData.instanceQueueTimeoutCount.incrementAndGet();
    }

    /**
     * @return
     */
    public long getRuntimeMillisInstance() {
        return (System.currentTimeMillis() - m_startTimeMillis);
    }

    /**
     * @return
     */
    public long getRuntimeMillisLongTerm() {
        return (m_ltData.persistentRuntimeMillis.get() + getRuntimeMillisInstance());
    }

    /**
     * @param documentType
     * @return
     */
    public long getUserRequestCount(@Nullable final DocumentType documentType) {
        return implGetUserRequestCount(documentType, JobError.values());
    }

    /**
     * @param documentType
     * @return
     */
    public long getUserRequestCountFailed(@Nullable final DocumentType documentType) {
        return implGetUserRequestCount(documentType, JOB_ERRORS_FAILURE);
    }

    /**
     * @param documentType
     * @return
     */
    public long getUserRequestCountLongTerm(@Nullable final DocumentType documentType) {
        return implGetUserRequestCountLongTerm(documentType, JobError.values());
    }

    /**
     * @param documentType
     * @return
     */
    public long getUserRequestCountLongTermFailed(@Nullable final DocumentType documentType) {
        return implGetUserRequestCountLongTerm(documentType, JOB_ERRORS_FAILURE);
    }

    /**
     * @param documentType
     * @return
     */
    public double getUserRequestSuccessRatio(@Nullable final DocumentType documentType) {
        final long requestCount = getUserRequestCount(documentType);
        final long failureCount = getUserRequestCountFailed(documentType);

        return (requestCount > 0) ?
            (1.0 - ((double) failureCount / requestCount)) :
                1.0;
    }

    /**
     * @param documentType
     * @return
     */
    public double getUserRequestSuccessRatioLongTerm(@Nullable final DocumentType documentType) {
        final long longTermRequestCount = getUserRequestCountLongTerm(documentType);
        final long longTermFailureCount = getUserRequestCountLongTermFailed(documentType);

        return (longTermRequestCount > 0) ?
            (1.0 - ((double) longTermFailureCount / longTermRequestCount)) :
                1.0;
    }

    /**
     * getQueueLimitHighReachedCount
     *
     * @return
     */
    public long getQueueLimitHighReachedCount() {
        return m_ltData.instanceQueueCountLimitHighReached.get();
    }

    /**
     * getQueueLimitHighReachedCountLongTerm
     *
     * @return
     */
    public long getQueueLimitHighReachedCountLongTerm() {
        return m_ltData.persistentQueueCountLimitHighReached.get() + getQueueLimitHighReachedCount();
    }

    /**
     * getQueueLimitLowReachedCount
     *
     * @return
     */
    public long getQueueLimitLowReachedCount() {
        return m_ltData.instanceQueueCountLimitLowReached.get();
    }

    /**
     * getQueueLimitLowReachedCountLongTerm
     *
     * @return
     */
    public long getQueueLimitLowReachedCountLongTerm() {
        return m_ltData.persistentQueueCountLimitLowReached.get() + getQueueLimitLowReachedCount();
    }

    public long getQueueTimeoutCount() {
        return m_ltData.instanceQueueTimeoutCount.get();
    }

    public long getQueueTimeoutCountLongTerm() {
        return m_ltData.persistentQueueTimeoutCount.get() + getQueueTimeoutCount();
    }

    // - Implementation --------------------------------------------------------

    /**
     * @param documentType
     * @param jobErrorsToUse
     * @return
     */
    private long implGetUserRequestCount(@Nullable final DocumentType documentType, @NonNull final JobError[] jobErrorsToUse) {
        long ret = 0;

        for (final DocumentType curDocumentType : ((null == documentType) ? DocumentType.values() : (new DocumentType[] { documentType }))) {
            final Map<JobError, AtomicLong> instanceDocumentTypeMap = m_ltData.instanceStatusCount.get(curDocumentType);

            for (final JobError curJobError : jobErrorsToUse) {
                ret += instanceDocumentTypeMap.get(curJobError).get();
            }
        }

        return ret;
    }

    private long implGetUserRequestCountLongTerm(@Nullable final DocumentType documentType, @NonNull final JobError[] jobErrorsToUse) {
        long ret = 0;

        for (final DocumentType curDocumentType : ((null == documentType) ? DocumentType.values() : (new DocumentType[] { documentType }))) {
            final Map<JobError, AtomicLong> persistentDocumentTypeMap = m_ltData.persistentStatusCount.get(curDocumentType);
            final Map<JobError, AtomicLong> instanceDocumentTypeMap = m_ltData.instanceStatusCount.get(curDocumentType);

            for (final JobError curJobError : jobErrorsToUse) {
                ret += (persistentDocumentTypeMap.get(curJobError).get() + instanceDocumentTypeMap.get(curJobError).get());
            }
        }

        return ret;

    }

    /**
     * @throws Exception
     */
    private void implReadPersistentData() throws Exception {
        if ((null != m_ltsFile) && m_ltsFile.isFile() && m_ltsFile.canRead()) {
            try (final BufferedReader reader = new BufferedReader(new FileReader(m_ltsFile))) {
                final LTData readData = (new Gson()).fromJson(reader, LTData.class);

                // status counts from version >= 2 on
                if (readData.version >= LTData.VERSION_STATUS_COUNTS_ADDED) {
                    m_ltData = readData;
                    m_ltData.persistentRuntimeMillis.set(m_ltData.longTermRuntimeMillis.get());

                    DocumentType.forEach(curDocumentType -> {
                        // some data might not be set, in case the DocumentType got
                        // enlarged after the last write of persistent data =>
                        // ensure that all data is set to valid values
                        if (!m_ltData.instanceStatusCount.containsKey(curDocumentType)) {
                            m_ltData.instanceStatusCount.put(curDocumentType, new HashMap<>());
                        }

                        if (!m_ltData.longTermStatusCount.containsKey(curDocumentType)) {
                            m_ltData.longTermStatusCount.put(curDocumentType, new HashMap<>());
                        }

                        if (!m_ltData.persistentStatusCount.containsKey(curDocumentType)) {
                            m_ltData.persistentStatusCount.put(curDocumentType, new HashMap<>());
                        }

                        final Map<JobError, AtomicLong> instanceMap = m_ltData.instanceStatusCount.get(curDocumentType);
                        final Map<JobError, AtomicLong> longTermMap = m_ltData.longTermStatusCount.get(curDocumentType);
                        final Map<JobError, AtomicLong> persistentMap = new HashMap<>();

                        JobError.forEach(curJobError -> {
                            // some data might not be set, in case the JobError got
                            // enlarged after the last write of persistent data =>
                            // ensure that all data is set to valid values
                            if (!instanceMap.containsKey(curJobError)) {
                                instanceMap.put(curJobError, new AtomicLong(0));
                            }

                            if (!longTermMap.containsKey(curJobError)) {
                                longTermMap.put(curJobError, new AtomicLong(0));
                            }

                            // put data from longTerm map into persistentMap
                            persistentMap.put(curJobError, longTermMap.get(curJobError));
                        });

                        // set correctly initialized persistent map
                        m_ltData.persistentStatusCount.put(curDocumentType, persistentMap);
                    });

                    // queue limits from version >= 3 on
                    if (readData.version >= LTData.VERSION_QUEUE_LIMITS_ADDED) {
                        m_ltData.persistentQueueCountLimitHighReached.set(m_ltData.longTermQueueCountLimitHighReached.get());
                        m_ltData.persistentQueueCountLimitLowReached.set(m_ltData.longTermQueueCountLimitLowReached.get());
                        m_ltData.persistentQueueTimeoutCount.set(m_ltData.longTermQueueTimeoutCount.get());
                    }
                }
            }
        }
    }

    /**
     * @throws Exception
     */
    private void implWritePersistentData() throws Exception {
        if ((null != m_ltsFile) && m_ltsFile.getParentFile().canWrite()) {
            // run times
            m_ltData.instanceRuntimeMillis.set(getRuntimeMillisInstance());
            m_ltData.longTermRuntimeMillis.set(getRuntimeMillisLongTerm());

            // status counts (from version >= 2 on)
            DocumentType.forEach(curDocumentType ->
                JobError.forEach(curJobError ->
                    m_ltData.longTermStatusCount.get(curDocumentType).get(curJobError).set(
                        m_ltData.persistentStatusCount.get(curDocumentType).get(curJobError).get() +
                        m_ltData.instanceStatusCount.get(curDocumentType).get(curJobError).get())));

            // queue limits/timeout (from version >= 3 on)
            m_ltData.longTermQueueCountLimitHighReached.set(
                m_ltData.persistentQueueCountLimitHighReached.get() + m_ltData.instanceQueueCountLimitHighReached.get());

            m_ltData.longTermQueueCountLimitLowReached.set(
                m_ltData.persistentQueueCountLimitLowReached.get() + m_ltData.instanceQueueCountLimitLowReached.get());

            m_ltData.longTermQueueTimeoutCount.set(
                m_ltData.persistentQueueTimeoutCount.get() + m_ltData.instanceQueueTimeoutCount.get());

            // write current instance data and just updated long term data via Gson
            try (final PrintWriter writer = new PrintWriter(new FileWriter(m_ltsFile))) {
                (new Gson()).toJson(m_ltData, writer);
            }
        }
    }

    /**
     * (re)initialize instance data
     *
     * @throws Exception
     */
    private void implResetInstanceData() throws Exception {
        // set VERSION to current version, it might have still the old, persistent value
        m_ltData.version = LTData.VERSION;

        // update current instance run time
        m_ltData.instanceRuntimeMillis.set(getRuntimeMillisInstance());

        // update instance counts
        DocumentType.forEach(curDocumentType -> {
            final Map<JobError, AtomicLong> instanceMap = m_ltData.instanceStatusCount.get(curDocumentType);

            if (null != instanceMap) {
                JobError.forEach(curJobError -> {
                    final AtomicLong instanceCount = instanceMap.get(curJobError);

                    if (null != instanceCount) {
                        instanceCount.set(0);
                    }
                });
            }
        });

        // reset instance queue limits/timeouts
        m_ltData.instanceQueueCountLimitHighReached.set(0);
        m_ltData.instanceQueueCountLimitLowReached.set(0);
        m_ltData.instanceQueueTimeoutCount.set(0);
    }

    // - Members ----------------------------------------------------------------

    private long m_startTimeMillis = System.currentTimeMillis();

    private AtomicBoolean m_isRunnning = new AtomicBoolean(true);

    private File m_ltsFile = null;

    private LTData m_ltData = new LTData();
}
