/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import com.openexchange.documentconverter.NonNull;
import com.openexchange.documentconverter.Properties;

/**
 * {@link PDF2GraphicConverterJob}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
public class PDF2GraphicConverterJob extends PDF2SVGConverterJob {

    /**
     * Initializes a new {@link PDF2GraphicConverterJob}.
     *
     * @param jobProperties
     * @param resultProperties
     */
    public PDF2GraphicConverterJob(HashMap<String, Object> jobProperties, HashMap<String, Object> resultProperties) {
        super(jobProperties, resultProperties);
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.ConverterJob#getHash()
     */
    @Override
    public String getHash() {
        if (null == m_cacheHash) {
            final StringBuilder hashBuilder = super.getInputFileHashBuilder();

            if (null != hashBuilder) {
                if (m_resultZipArchive) {
                    hashBuilder.append('z');
                }

                hashBuilder.append("p2g").append(m_resultExtension);

                if (m_autoscale) {
                    hashBuilder.append('a');
                }

                if (null != m_graphicScaleType) {
                    hashBuilder.append(m_graphicScaleType.toString().substring(0, 3));
                }

                if (null != m_pageRange) {
                    hashBuilder.append('#').append(m_pageRange);
                }

                hashBuilder.append('@').append(m_pixelWidth).append('x').append(m_pixelHeight);

                setHash(hashBuilder);
            }
        }

        return m_cacheHash;
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.ConverterJob#applyJobProperties(java.util.HashMap)
     */
    @Override
    protected void applyJobProperties() {
        super.applyJobProperties();

        if (-1 == m_singlePageNumber) {
            m_singlePageNumber = 1;
        }

        // determine the destination graphic format
        m_resultMimeType = (String) m_jobProperties.get(Properties.PROP_MIME_TYPE);
        m_resultMimeType = ((null == m_resultMimeType) || (m_resultMimeType.length() <= 0)) ? "image/jpeg" : m_resultMimeType.toLowerCase();
        m_resultExtension = "image/jpeg".equals(m_resultMimeType) ? "jpg" : "png";

        // Graphic scaleType
        final Object scaleType = m_jobProperties.get(Properties.PROP_IMAGE_SCALE_TYPE);

        if (scaleType instanceof String) {
            m_graphicScaleType = ScaleType.fromString((String) scaleType);
        }

        m_resultZipArchive = false;
    }

    /**
     *
     */
    @Override
    protected String[] getPDFToolCommandLine(@NonNull File tempDir) {
        final String pageNumberStr = Integer.toString(m_singlePageNumber);

        return new String[] {
            m_pdfToolProgramPath,
            "-" + m_resultExtension,
            Integer.toString(m_pixelWidth),
            Integer.toString(m_pixelHeight),
            m_graphicScaleType.toString(),
            m_inputFile.getAbsolutePath(),
            new File(tempDir, "%d#%d_%dx%d").getAbsolutePath(),
            (0 < m_singlePageNumber) ? pageNumberStr : "all",
            pageNumberStr
        };
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.PDF2SVGConverterJob#getPageExtension()
     */
    @Override
    protected String getPageExtension() {
        return m_resultExtension;
    }

    // - Implementation

    /*
     * (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.ConverterJob#getMeasurementType()
     */
    @Override
    public ConverterMeasurement getMeasurementType() {
        return ConverterMeasurement.CONVERT_PDF_TO_GRAPHIC;
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.PDF2SVGConverterJob#writePageFileToOutputStream(java.io.File, int, java.io.OutputStream)
     */
    @Override
    protected void writePageFileToOutputStream(File resultFile, int pageNumber, OutputStream outputStm) {
        try (final InputStream resultFileInputStream = FileUtils.openInputStream(resultFile)) {
            IOUtils.copy(resultFileInputStream, outputStm);
            outputStm.flush();
        } catch (IOException e) {
            ServerManager.logExcp(e);
        }
    }


    // - Members ---------------------------------------------------------------

    private ScaleType m_graphicScaleType = ScaleType.CONTAIN;
}
