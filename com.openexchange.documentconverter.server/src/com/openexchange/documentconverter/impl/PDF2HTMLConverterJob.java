/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import com.openexchange.documentconverter.JobError;
import com.openexchange.documentconverter.JobErrorEx;
import com.openexchange.documentconverter.NonNull;
import com.openexchange.documentconverter.Properties;

/**
 * {@link PDF2HTMLConverterJob}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.6.0
 */
public class PDF2HTMLConverterJob extends PDF2SVGConverterJob {

    /**
     * Initializes a new {@link PDF2HTMLConverterJob}.
     *
     * @param jobProperties
     * @param resultProperties
     */
    public PDF2HTMLConverterJob(HashMap<String, Object> jobProperties, HashMap<String, Object> resultProperties) {
        super(jobProperties, resultProperties);
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.ConverterJob#getHash()
     */
    @Override
    public String getHash() {
        if (null == m_cacheHash) {
            if (m_resultZipArchive) {
                m_cacheHash = super.getHash();
            } else {
                final StringBuilder hashBuilder = super.getInputFileHashBuilder();

                if (null != hashBuilder) {
                    hashBuilder.append("p2h");

                    if (m_autoscale) {
                        hashBuilder.append('a');
                    }

                    if (null != m_pageRange) {
                        hashBuilder.append('#').append(m_pageRange);
                    }

                    setHash(hashBuilder);
                }
            }
        }

        return m_cacheHash;
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.ConverterJob#applyJobProperties(java.util.HashMap)
     */
    @Override
    protected void applyJobProperties() {
        super.applyJobProperties();

        // determine, if we need a SVG or full HTML conversion
        final Boolean zipArchive = (Boolean) m_jobProperties.get(Properties.PROP_ZIP_ARCHIVE);
        m_resultZipArchive = (null != zipArchive) ? zipArchive.booleanValue() : false;
    }

    /**
     * @param pageDirectory
     */
    @Override
    protected JobErrorEx postProcessPages(@NonNull final File pageDirectory) {
        JobErrorEx jobErrorEx = new JobErrorEx(JobError.GENERAL);

        if (m_resultZipArchive) {
            jobErrorEx = super.postProcessPages(pageDirectory);
        } else {
            final String[] fileNames = pageDirectory.list();

            if (!ArrayUtils.isEmpty(fileNames)) {
                try (final OutputStream outputStm = FileUtils.openOutputStream(m_outputFile);
                     final PrintWriter writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(outputStm)))) {
                    writer.println("<html>");
                    writer.println("<head><title>Open-Xchange DC</title><meta charset=\"UTF-8\"></head>");
                    writer.println("<body>");

                    Arrays.sort(fileNames, 0, fileNames.length, new Comparator<String>() {
                        @Override
                        public int compare(String file1, String file2) {
                            final String pageNumber1 = file1.substring(file1.lastIndexOf('#') + 1, file1.lastIndexOf('_'));
                            final String pageNumber2 = file2.substring(file2.lastIndexOf('#') + 1, file2.lastIndexOf('_'));

                            return Integer.valueOf(pageNumber1).compareTo(Integer.valueOf(pageNumber2));
                        }
                    });

                    int curPageNumber = 0;

                    for (String curFilename : fileNames) {
                        if (curPageNumber++ > 0) {
                            writer.println("<hr>");
                        }

                        writer.flush();
                        writePageFileToOutputStream(new File(pageDirectory, curFilename), curPageNumber, outputStm);
                    }

                    writer.flush();
                    writer.println("</body></html>");

                    jobErrorEx = new JobErrorEx();
                } catch (Exception e) {
                    ServerManager.logExcp(e);
                }
            }
        }

        return jobErrorEx;
    }
}
