/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import com.openexchange.documentconverter.DocumentConverterUtil;
import com.openexchange.documentconverter.JobError;
import com.openexchange.documentconverter.JobErrorEx;
import com.openexchange.documentconverter.NonNull;
import com.openexchange.documentconverter.Properties;
import com.openexchange.documentconverter.impl.api.BackendType;
import com.openexchange.documentconverter.impl.api.ConverterStatus;
import com.openexchange.exception.ExceptionUtils;
import com.sun.star.lang.DisposedException;

//-------------------------
//- class PDFConverterJob -
//-------------------------

/**
 * {@link PDF2SVGConverterJob}
 *
 * @author <a href="mailto:malte.timmermann@open-xchange.com">Malte Timmermann</a>
 */
public class PDF2SVGConverterJob extends ConverterJob {

    final private static int PDFTOOL_ERRORCODE_TIMEOUT = -1;

    final private static int PDFTOOL_ERRORCODE_NONE           = 0x00000000;
    final private static int PDFTOOL_ERRORCODE_ARGUMENT_COUNT = 0x00000002;
    final private static int PDFTOOL_ERRORCODE_NO_BINARY      = 0x00000040;
    final private static int PDFTOOL_ERRORCODE_PASSWORD       = 0x00000010;

    final private static File PDFTOOL_EXECUTABLE_FILE = new File("/opt/open-xchange/sbin/pdftool");

    /**
     * Initializes a new {@link PDF2SVGConverterJob}.
     *
     * @param jobProperties
     * @param resultProperties
     */
    public PDF2SVGConverterJob(HashMap<String, Object> jobProperties, HashMap<String, Object> resultProperties) {
        super(jobProperties, resultProperties);
        applyJobProperties();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.impl.ConverterJob#getHash()
     */
    @Override
    public String getHash() {
        if (null == m_cacheHash) {
            final StringBuilder hashBuilder = super.getInputFileHashBuilder();

            if (null != hashBuilder) {
                if (m_resultZipArchive) {
                    hashBuilder.append('z');
                }

                hashBuilder.append("p2s");

                if (m_autoscale) {
                    hashBuilder.append('a');
                }

                if (null != m_pageRange) {
                    hashBuilder.append('#').append(m_pageRange);
                }

                setHash(hashBuilder);
            }
        }

        return m_cacheHash;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IJob#needsInstance()
     */
    @Override
    public BackendType backendTypeNeeded() {
        return BackendType.PDFTOOL;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.impl.ConverterJob#applyJobProperties(java.util.HashMap)
     */
    @Override
    protected void applyJobProperties() {
        super.applyJobProperties();

        Object obj = null;

        m_resultMimeType = "text/html;charset=" + ENCODING;
        m_resultExtension = "html";

        // check page range
        m_singlePageNumber = -1;

        try {
            final int singlePageNumber = Integer.parseInt(m_pageRange);

            // if page range is reverse parseable to an int, we have a single page to export
            if (Integer.toString(singlePageNumber).equals(m_pageRange)) {
                m_singlePageNumber = singlePageNumber;
            }
        } catch (@SuppressWarnings("unused") NumberFormatException e) {
            // single page number not known => export all pages
            m_singlePageNumber = -1;
        }

        if (null != (obj = m_jobProperties.get(Properties.PROP_AUTOSCALE))) {
            m_autoscale = ((Boolean) obj).booleanValue();
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.impl.ConverterJob#beginExecute(com.openexchange.documentconverter.IInstance)
     */
    @Override
    protected boolean beginExecute(Object jobExecutionData) throws com.sun.star.lang.DisposedException, com.sun.star.uno.Exception, IOException {
        return (super.beginExecute(jobExecutionData) && implInitPDFTool());
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.impl.ConverterJob#execute(com.openexchange.documentconverter.IInstance)
     */
    @Override
    protected ConverterStatus doExecute(Object jobExecutionData) throws InterruptedException, IOException {
        final String pageExtension = getPageExtension();
        File tempDir = ServerManager.get().createTempDir("oxdcpdf2svg", pageExtension);

        if (null != tempDir) {
            try {
                final ServerConfig config = ServerManager.get().getServerConfig();
                final String[] bashCommandLine = ServerManager.getMaxVMemBashCommandLine(config.PDFTOOL_MAX_VMEM_MB);

                m_pdfProcess = Runtime.getRuntime().exec(ServerManager.getCommandLine(bashCommandLine, getPDFToolCommandLine(tempDir)));

                final int exitValue = ServerManager.waitForProcess(m_pdfProcess, config.JOB_EXECUTION_TIMEOUT_MILLISECONDS);

                if (PDFTOOL_ERRORCODE_NONE == exitValue) {
                    m_resultJobErrorEx = postProcessPages(tempDir);
                } else {
                    if (PDFTOOL_ERRORCODE_TIMEOUT == exitValue) {
                        // kill process ultimately in case of a timeout error to get rid of looping processes
                        final Process usedProcess = m_pdfProcess;

                        if (null != usedProcess) {
                            usedProcess.destroyForcibly();
                        }

                        setJobErrorEx(new JobErrorEx(JobError.TIMEOUT));
                    } else if (PDFTOOL_ERRORCODE_PASSWORD == exitValue) {
                        setJobErrorEx(new JobErrorEx(JobError.PASSWORD));
                    } else {
                        setJobErrorEx(new JobErrorEx(JobError.PDFTOOL));
                    }
                }
            } catch (Throwable e) {
                ExceptionUtils.handleThrowable(e);

                m_resultJobErrorEx = new JobErrorEx(JobError.GENERAL);

                if (ServerManager.isLogDebug()) {
                    ServerManager.logDebug(e.getMessage());
                }
            } finally {
                FileUtils.deleteQuietly(tempDir.getParentFile());
            }
        }

        return m_resultJobErrorEx.hasNoError() ? ConverterStatus.OK : ConverterStatus.ERROR;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.impl.ConverterJob#endExecute(com.openexchange.documentconverter.IInstance)
     */
    @Override
    protected ConverterStatus endExecute() throws IOException, MalformedURLException, DisposedException, com.sun.star.uno.Exception {
        final ConverterStatus ret = super.endExecute();

        m_pdfProcess = null;

        return ret;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.impl.ConverterJob#getMeasurementType()
     */
    @Override
    public ConverterMeasurement getMeasurementType() {
        return ConverterMeasurement.CONVERT_PDF_TO_SVG;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.server.impl.ConverterJob#kill()
     */
    @Override
    public void kill() {
        final Process usedProcess = m_pdfProcess;

        if (null != usedProcess) {
            usedProcess.destroyForcibly();

            if (ServerManager.isLogTrace()) {
                ServerManager.logTrace("DC PDFTool killed");
            }
        }

    }
    // - Implementatation ------------------------------------------------------

    /**
     * @return
     */
    protected String[] getPDFToolCommandLine(@NonNull File tempDir) {
        final String pageNumberStr = Integer.toString(m_singlePageNumber);

        return new String[] {
            m_pdfToolProgramPath,
            m_inputFile.getAbsolutePath(),
            new File(tempDir, "%d#%d_%dx%d").getAbsolutePath(),
            (m_singlePageNumber > 0) ? pageNumberStr : "all",
            pageNumberStr
        };
    }

    /**
     * @param pageFilename The PageCount#PageNumber_PixelWidthxPixelHeight mangled filename
     * @return The page number of the given page string or -1 if not valid
     */
    final protected int parsePageFilename(String pageFilename) {
        final int hashPos = pageFilename.indexOf('#');
        final int underscorePos = pageFilename.lastIndexOf('_');
        final int crossPos = pageFilename.lastIndexOf('x');
        int ret = -1;

        if ((hashPos > 0) && (underscorePos > (hashPos + 1)) && (crossPos > (underscorePos + 1)) && (crossPos < (pageFilename.length() - 1))) {
            m_originalPageCount = Integer.valueOf(pageFilename.substring(0, hashPos)).intValue();
            m_pixelWidth = Integer.valueOf(pageFilename.substring(underscorePos + 1, crossPos)).intValue();
            m_pixelHeight = Integer.valueOf(pageFilename.substring(crossPos + 1)).intValue();

            ret = Integer.valueOf(pageFilename.substring(hashPos + 1, underscorePos)).intValue();
        }

        return ret;
    }

    /**
     * @return The extension to be used for single pages
     */
    @SuppressWarnings("static-method")
    protected String getPageExtension() {
        return "svg";
    }

    /**
     * @param pageDirectory
     */
    protected JobErrorEx postProcessPages(@NonNull final File pageDirectory) {
        final File[] files = pageDirectory.listFiles();
        JobErrorEx jobErrorEx = new JobErrorEx(JobError.GENERAL);

        if (!ArrayUtils.isEmpty(files)) {
            if ((files.length > 1) || m_resultZipArchive) {
                try (final ZipOutputStream zipOutputStm = new ZipOutputStream(new BufferedOutputStream(FileUtils.openOutputStream(m_outputFile)))) {
                    for (int i = 0, count = files.length; i < count; ++i) {
                        final String curFilename = files[i].getName();
                        final int realPageNumber = parsePageFilename(curFilename);
                        final File pageFile = new File(pageDirectory, curFilename);

                        zipOutputStm.putNextEntry(new ZipEntry(Integer.toString(realPageNumber - 1)));
                        writePageFileToOutputStream(pageFile, realPageNumber, zipOutputStm);
                        zipOutputStm.closeEntry();
                    }

                    zipOutputStm.finish();
                    zipOutputStm.flush();

                    m_resultPageCount = files.length;
                    jobErrorEx = new JobErrorEx();
                } catch (IOException e) {
                    ServerManager.logExcp(e);
                }
            } else {
                final File resultPageFile = new File(pageDirectory, files[0].getName());

                if (resultPageFile.canRead()) {
                    if (resultPageFile.renameTo(m_outputFile)) {
                        m_resultPageCount = 1;
                        jobErrorEx = new JobErrorEx();
                    } else {
                        try {
                            Files.copy(resultPageFile.toPath(), FileUtils.openOutputStream(m_outputFile));

                            m_resultPageCount = 1;
                            jobErrorEx = new JobErrorEx();
                        } catch (IOException e) {
                            ServerManager.logExcp(e);
                        }
                    }
                }
            }
        }

        return jobErrorEx;
    }

    /**
     * @param resultFile
     * @param pageNumber
     * @param outputStm
     * @throws Exception
     */
    protected void writePageFileToOutputStream(File resultFile, int pageNumber, OutputStream outputStm) {
        String svgContent = null;

        if (null != resultFile) {
            try {
                svgContent = FileUtils.readFileToString(resultFile, ENCODING);
            } catch (Exception e) {
                ServerManager.logExcp(e);
            }
        }

        if (null != svgContent) {
            if (m_originalPageCount != 1) {
                final String replacement = (new StringBuilder("$1$2-")).append(PAGEID_APPENDIXBASE).append(Integer.toString(pageNumber)).append("$3").toString();
                svgContent = svgContent.replaceAll(REGEX_4_IDS, replacement).replaceAll(REGEX_4_IDREFS_HREF, replacement).replaceAll(REGEX_4_IDREFS_URL, replacement);
            }

            if (m_autoscale) {
                try {
                    final double scaleFactor = 1.333;
                    StringBuilder extentStringBuilder = new StringBuilder();
                    int widthPos = svgContent.indexOf("width=");
                    int heightPos = svgContent.indexOf("height=");
                    int replaceStartPos = -1;
                    int replaceEndPos = -1;

                    if (0 < widthPos) {
                        replaceStartPos = widthPos;
                        replaceEndPos = svgContent.indexOf("pt", widthPos + 7);
                        extentStringBuilder.append("width=\"");
                        extentStringBuilder.append((int) Math.floor(Double.parseDouble(svgContent.substring(widthPos + 7, replaceEndPos)) * scaleFactor));
                        extentStringBuilder.append("pt\" ");
                        replaceEndPos += 4;
                    }

                    if (widthPos < heightPos) {
                        replaceEndPos = svgContent.indexOf("pt", heightPos + 8);
                        extentStringBuilder.append("height=\"");
                        extentStringBuilder.append((int) Math.floor(Double.parseDouble(svgContent.substring(heightPos + 8, replaceEndPos)) * scaleFactor));
                        extentStringBuilder.append("pt\" ");
                        replaceEndPos += 4;
                    }

                    if (extentStringBuilder.length() > 0) {
                        svgContent = svgContent.substring(0, replaceStartPos) + extentStringBuilder.toString() + svgContent.substring(replaceEndPos);
                    }
                } catch (@SuppressWarnings("unused") Exception e) {
                    ServerManager.logWarn("DC not able to autoscale SVG content. Ignoring autoscaling.");
                }
            }

            try {
                IOUtils.write(svgContent, outputStm, ENCODING);
                outputStm.flush();
            } catch (IOException e) {
                ServerManager.logExcp(e);
            }
        }
    }

    /**
     * @return
     * @throws IOException
     */
    private synchronized boolean implInitPDFTool() throws IOException {
        if (null == m_pdfToolProgramPath) {
            String readerEngineRoot = null;
            File pdfExtractorExecutable = null;

            // check all possible binary names and use executable one
            if (implCanExecute(pdfExtractorExecutable = PDFTOOL_EXECUTABLE_FILE) ||
                ((null != (readerEngineRoot = (String) m_jobProperties.get(Properties.PROP_READERENGINE_ROOT))) &&
                 implCanExecute(pdfExtractorExecutable = new File(readerEngineRoot, "/program/pdf2svg")))) {

                m_pdfToolProgramPath = pdfExtractorExecutable.getCanonicalPath();
            }

            if (null == m_pdfToolProgramPath) {
                final String errorText = new StringBuilder(512).
                    append("DC is not able to find a proper PDFTool executable. Please check setup! [Search pathes: ").
                    append(PDFTOOL_EXECUTABLE_FILE.getAbsolutePath()).
                    append(", ").
                    append((null != pdfExtractorExecutable) ? pdfExtractorExecutable.getAbsolutePath() : DocumentConverterUtil.STR_NOT_AVAILABLE).
                    append(']').toString();

                ServerManager.logError(errorText);
                throw new IOException(errorText);
            }
        }

        return true;
    }

    /**
     * @param file
     * @return
     */
    private boolean implCanExecute(@NonNull final File file) {
        final Path path = file.toPath();
        final String pdf2svgPathStr = file.getAbsolutePath();
        final boolean ret = Files.isExecutable(path);
        String errorMsg = null;

        if (ret) {
            Process pdfToolProcess = null;
            int testReturnValue = PDFTOOL_ERRORCODE_NO_BINARY;

            if (ServerManager.isLogTrace()) {
                ServerManager.logTrace("DC found PDF2SVG: " + pdf2svgPathStr);
            }

            try {
                pdfToolProcess = Runtime.getRuntime().exec(pdf2svgPathStr);

                if (null != pdfToolProcess) {
                    testReturnValue = ServerManager.waitForProcess(pdfToolProcess, 5000);
                }
            } catch (Exception e) {
                ServerManager.logExcp(e);
            }

            if ((PDFTOOL_ERRORCODE_NONE != testReturnValue) && (PDFTOOL_ERRORCODE_ARGUMENT_COUNT != testReturnValue)) {
                final StringBuilder errorMsgBuilder = new StringBuilder(256).append("DC PDFTool ");

                if (PDFTOOL_ERRORCODE_TIMEOUT == testReturnValue) {
                   // kill process ultimately in case of a timeout error to get rid of looping processes
                   if (null != pdfToolProcess) {
                       pdfToolProcess.destroyForcibly();
                   }

                   errorMsgBuilder.append("received timeout");
                } else if (PDFTOOL_ERRORCODE_NO_BINARY == testReturnValue) {
                    errorMsgBuilder.append("is not executable by wrapper script");
                }else {
                    errorMsgBuilder.append("is not executable");
                }

                errorMsg = errorMsgBuilder.append(" (errorCode: ").append(testReturnValue).append("): ").append(pdf2svgPathStr).toString();
            }
        } else {
            errorMsg = "DC PDF2SVG cannot be found or executed: " + pdf2svgPathStr;
        }

        if (null != errorMsg) {
            ServerManager.logError(errorMsg);
        }

        return ret;
    }

    // - Static members --------------------------------------------------------

    protected static String m_pdfToolProgramPath = null;

    final private static String PAGEID_APPENDIXBASE = "page-";

    final private static String ENCODING = "UTF-8";

    final private static String REGEX_4_IDS = "(id=\")(.*?)(\")";

    final private static String REGEX_4_IDREFS_HREF = "(xlink:href=\"#)(.*?)(\")";

    final private static String REGEX_4_IDREFS_URL = "(url\\(#)(.*?)(\\))";

    // - Members ---------------------------------------------------------------

    protected int m_singlePageNumber = -1;

    protected boolean m_autoscale = false;

    protected volatile Process m_pdfProcess = null;

}
