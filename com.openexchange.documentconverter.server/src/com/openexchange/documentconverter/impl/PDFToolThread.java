/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 *
 */
package com.openexchange.documentconverter.impl;

import java.util.concurrent.atomic.AtomicInteger;
import com.openexchange.documentconverter.NonNull;
import com.openexchange.documentconverter.impl.api.BackendType;
import com.openexchange.documentconverter.impl.api.IJob;
import com.openexchange.documentconverter.impl.api.JobEntry;

/**
 * @author ka
 *
 */
public class PDFToolThread extends JobThread {

    /**
     * @param jobProcessor
     * @param cache
     * @param watchdog
     */
    public PDFToolThread(
        @NonNull final ServerManager serverManager,
        @NonNull final JobProcessor jobProcessor,
        @NonNull final Cache cache,
        @NonNull ErrorCache errorCache,
        @NonNull final Watchdog watchdog) {

        super(serverManager, jobProcessor, BackendType.PDFTOOL, cache, errorCache, watchdog);

        setName(getLogBuilder().append("#").append(m_pdfToolThreadNumber).toString());
    }

    // - Overrides -------------------------------------------------------------

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.JobThread#createCopy()
     */
    @Override
    protected JobThread createCopy() {
        return new PDFToolThread(m_serverManager, m_jobProcessor, m_cache, m_errorCache, m_watchdog);
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.JobThread#shutdownCurrentInstance()
     */
    @Override
    protected void shutdownCurrentInstance() {
        final JobEntry curJobEntry = getCurrentJobEntry();

        if (null != curJobEntry) {
            final IJob job = curJobEntry.getJob();

            if (null != job) {
                try {
                    job.kill();
                } catch (Exception e) {
                    ServerManager.logExcp(e);
                }
            }
        }

        super.shutdownCurrentInstance();
    }

    // - Static Members ---------------------------------------------------------------

    private final static AtomicInteger PDF_THREAD_NUMBER = new AtomicInteger(0);

    // - Members ----------------------------------------------------------------------

    private final int m_pdfToolThreadNumber = PDF_THREAD_NUMBER.incrementAndGet();
}
