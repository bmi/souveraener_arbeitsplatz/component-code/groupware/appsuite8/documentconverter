/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.commons.io.FileUtils;
import com.openexchange.documentconverter.JobError;
import com.openexchange.documentconverter.JobPriority;
import com.openexchange.documentconverter.LogData;
import com.openexchange.documentconverter.NonNull;
import com.openexchange.documentconverter.Nullable;
import com.openexchange.documentconverter.PDFExtractor;
import com.openexchange.documentconverter.Properties;
import com.openexchange.documentconverter.impl.api.IQueueMonitor;

/**
 * {@link PageConverter}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.1
 */
public class PageConverter {

    final private static String DC_PAGECONVERTER_TIMER_NAME = "DC PageConverter timer";

    /**
     * {@link JobData}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v7.10.2
     */
    private class JobData {

        /**
         * Initializes a new {@link JobData}.
         * @param pageJobProperties
         */
        JobData(
            @NonNull final File pdfInputFile,
            @NonNull final String pageJobType,
            @NonNull final Map<String, Object> pageJobProperties) {

            m_pageJobType = pageJobType;
            m_pageJobProperties = pageJobProperties;
            m_pdfInputFile = pdfInputFile;
        }

        /**
         *
         */
        void updateTimestamp() {
            m_timeStampMillis.set(System.currentTimeMillis());
        }

        /**
         *
         */
        void clear() {
            if (null != m_pdfInputFile) {
                FileUtils.deleteQuietly(m_pdfInputFile);
                m_pdfInputFile = null;
            }
        }

        /**
         * @return
         */
        String getPageJobType() {
            return m_pageJobType;
        }

        /**
         * @return
         */
        Map<String, Object> getPageJobProperties() {
            return m_pageJobProperties;
        }

        /**
         * @return
         */
        long getTimeStampMillis() {
            return m_timeStampMillis.get();
        }

        // - Members ----------------------------------------------------------------

        final private String m_pageJobType;

        final private Map<String, Object> m_pageJobProperties;

        private File m_pdfInputFile;

        private AtomicLong m_timeStampMillis = new AtomicLong(System.currentTimeMillis());
    }


    /**
     * Initializes a new {@link PageConverter}.
     */
    public PageConverter(
        @NonNull final ServerManager serverManager,
        @NonNull final JobProcessor jobProcessor,
        @Nullable final PDFExtractor pdfExtractor,
        @Nullable final IQueueMonitor queueMonitor,
        long autoCleanupTimeoutMillis) {

        super();

        m_serverManager = serverManager;
        m_jobProcessor = jobProcessor;
        m_pdfExtractor = pdfExtractor;
        m_queueMonitor = queueMonitor;

        // create auto cleanup timer, if indicated by config
        m_autoCleanupTimeoutMillis = Math.max(autoCleanupTimeoutMillis, 0);

        if (m_autoCleanupTimeoutMillis > 0) {
            final long autoCleanupCheckPeriodMillis = Math.min(60000, m_autoCleanupTimeoutMillis);

            // start periodic timer (max. 60s) for auto clean up of stateful resources
            (m_autoCleanupTimer = new Timer(DC_PAGECONVERTER_TIMER_NAME, true)).scheduleAtFixedRate(
                new TimerTask() {

                    @Override
                    public void run() {
                        // perform possible cleanup of left over, stateful resources
                        implAutoCleanup(false);
                    }

                },
                autoCleanupCheckPeriodMillis,
                autoCleanupCheckPeriodMillis);
        }
    }

    /**
     *
     */
    public void shutdown() {
        if (m_isRunning.compareAndSet(true, false)) {
            long shutdownStartTime = 0;
            final boolean trace = ServerManager.isLogTrace();

            if (trace) {
                shutdownStartTime = System.currentTimeMillis();
                ServerManager.logTrace("DC PageConverter starting shutdown...");
            }

            if (null != m_autoCleanupTimer) {
                m_autoCleanupTimer.cancel();
                m_autoCleanupTimer = null;
            }

            implAutoCleanup(true);

            if (trace) {
                ServerManager.logTrace(new StringBuilder(256).
                    append("DC PageConverter finished shutdown: ").
                    append(System.currentTimeMillis() - shutdownStartTime).append("ms").
                    append('!').toString());
            }
        }
    }

    /**
     * @param jobId
     * @param jobProperties
     * @param resultProperties
     */
    String beginConversion(String jobType, @NonNull HashMap<String, Object> jobProperties, @NonNull HashMap<String, Object> resultProperties) {
        final ManagedJob managedJob = new ManagedJob(m_serverManager, m_jobProcessor, m_queueMonitor);
        final HashMap<String, Object> pdfJobProperties = new HashMap<>(12);
        final HashMap<String, Object> pdfResultProperties = new HashMap<>(8);
        final boolean logTrace = ServerManager.isLogTrace();
        final long startTime = logTrace ? System.currentTimeMillis() : 0;
        File pdfResultFile = null;
        JobData jobData = null;
        String jobId = null;

        pdfJobProperties.putAll(jobProperties);

        try (final InputStream pdfResultStm = managedJob.process("pdf", pdfJobProperties, pdfResultProperties)) {
            if (null != pdfResultStm) {
                pdfResultFile = m_serverManager.createTempFile("oxcs");

                if (null != pdfResultFile) {
                    FileUtils.copyInputStreamToFile(pdfResultStm, pdfResultFile);
                }
            }
        } catch (IOException e) {
            FileUtils.deleteQuietly(pdfResultFile);
            pdfResultFile = null;

            ServerManager.logExcp(e);
        }

        try {
            final int pdfPageCount = ((null != m_pdfExtractor) && (null != pdfResultFile)) ?
                m_pdfExtractor.getPageCount(pdfResultFile, (String) jobProperties.get(Properties.PROP_INFO_FILENAME)) :
                    0;

            if (pdfPageCount > 0) {
                // set properties to be used for each page conversion
                final String pageJobType = (jobType.startsWith("image") || jobType.startsWith("preview")) ? "pdf2grf" : "pdf2svg";
                final HashMap<String, Object> pageJobProperties = implCreatePageJobProperties(pageJobType, pdfResultFile, jobProperties);

                // create and add new job data with unique job id
                jobId = UUID.randomUUID().toString();
                jobData = new JobData(pdfResultFile, pageJobType, pageJobProperties);

                m_jobDatas.put(jobId, jobData);

                // update statistics
                m_serverManager.getStatistics().incrementStatefulJobCount();

                // fill result properties
                resultProperties.clear();

                resultProperties.put(Properties.PROP_RESULT_JOBID, jobId);
                resultProperties.put(Properties.PROP_RESULT_SINGLE_PAGE_JOBTYPE, pageJobType);
                resultProperties.put(Properties.PROP_RESULT_PAGE_COUNT, Integer.valueOf(pdfPageCount));
                resultProperties.put(Properties.PROP_RESULT_ORIGINAL_PAGE_COUNT, Integer.valueOf(pdfPageCount));
                resultProperties.put(Properties.PROP_RESULT_INPUTFILE_HASH, jobId);

                String resultMimeType = null;
                String resultExtension = null;

                if ("pdf2svg".equals(pageJobType)) {
                    resultMimeType = "image/svg+xml";
                    resultExtension = "svg";
                } else {
                    resultMimeType = (String) pageJobProperties.getOrDefault(Properties.PROP_MIME_TYPE, "image/jpeg");
                    resultExtension = "image/png".equals(resultMimeType) ? "png" : "jpg";
                }

                resultProperties.put(Properties.PROP_RESULT_MIME_TYPE, resultMimeType);
                resultProperties.put(Properties.PROP_RESULT_EXTENSION, resultExtension);
                resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, pdfResultProperties.getOrDefault(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(1)));

                if (pdfResultProperties.containsKey(Properties.PROP_RESULT_ERROR_DATA)) {
                    resultProperties.put(Properties.PROP_RESULT_ERROR_DATA, pdfResultProperties.get(Properties.PROP_RESULT_ERROR_DATA));
                }

                if (logTrace) {
                    ServerManager.logTrace("DC PageConverter initiated page conversion",
                        jobProperties,
                        new LogData("jobid", jobId),
                        new LogData("exectime", Long.toString(System.currentTimeMillis() - startTime) + "ms"));
                }
            } else if (logTrace) {
                ServerManager.logTrace("DC PageConverter could not get total page count when initiating page conversion", jobProperties);
            }
        } catch (Exception e) {
            ServerManager.logExcp(e);
        } finally {
            if (null == jobData) {
                FileUtils.deleteQuietly(pdfResultFile);
                pdfResultFile = null;

                resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(1));

                if (logTrace) {
                    ServerManager.logTrace("DC PageConverter could not not create job data when initiating page conversion", jobProperties);
                }
            }
        }

        return jobId;
    }

    /**
     * @param jobId
     * @param jobProperties
     * @param resultProperties
     */
    InputStream getPage(@NonNull String jobId, int pageNumber, @NonNull HashMap<String, Object> jobProperties, @NonNull HashMap<String, Object> resultProperties) {
        final JobData curJobData = m_jobDatas.get(jobId);
        final boolean logTrace = ServerManager.isLogTrace();
        final long startTime = logTrace ? System.currentTimeMillis() : 0;
        final String pageNumberStr = Integer.toString(pageNumber);
        InputStream ret = null;

        if (null != curJobData) {
            curJobData.updateTimestamp();

            final HashMap<String, Object> pageJobProperties = new HashMap<>(12);

            pageJobProperties.putAll(jobProperties);
            pageJobProperties.putAll(curJobData.getPageJobProperties());
            pageJobProperties.put(Properties.PROP_PAGE_NUMBER, Integer.valueOf(pageNumber));
            pageJobProperties.put(Properties.PROP_PAGE_RANGE, pageNumberStr);

            // getPage requests might occur as a result of an rconvert call,
            // passing the final target format and parameters in getPage Requests =>
            // overwrite existing JobData from beginConvert with appropriate parameters
            String pageJobType = curJobData.getPageJobType();
            final String pageMimeType = (String) pageJobProperties.get(Properties.PROP_MIME_TYPE);

            if ("image/png".equals(pageMimeType) || "image/jpeg".equals(pageMimeType)) {
                pageJobType = "pdf2grf";
            } else if ("image/svg+xml".equals(pageMimeType) || "text/html".equals(pageMimeType)) {
                pageJobType = "pdf2svg";
            }

            ret = (new ManagedJob(m_serverManager, m_jobProcessor)).process(pageJobType, pageJobProperties, resultProperties);

            if (null != ret) {
                resultProperties.put(Properties.PROP_RESULT_PAGE_NUMBER, Integer.valueOf(pageNumber));
                resultProperties.put(Properties.PROP_RESULT_PAGE_COUNT, Integer.valueOf(1));
            }
        } else if (ServerManager.isLogDebug()) {
            ServerManager.logDebug("DC PageConverter has no information about given jobId in #getPage call", new LogData("jobid", jobId));
        }

        if (logTrace) {
            if (null != ret) {
                ServerManager.logTrace("DC PageConverter delivered page",
                    new LogData("jobid", jobId),
                    new LogData("pagenumber", pageNumberStr),
                    new LogData("exectime", Long.toString(System.currentTimeMillis() - startTime) + "ms"));
            } else {
                ServerManager.logTrace( "DC PageConverter not able to deliver page",
                    jobProperties,
                    new LogData("jobid", jobId),
                    new LogData("pagenumber", pageNumberStr));
            }
        }


        return ret;
    }

    /**
     * @param jobId
     * @param jobProperties
     * @param resultProperties
     */
    void endConversion(@NonNull String jobId, @NonNull HashMap<String, Object> resultProperties) {
        final JobData curJobData = m_jobDatas.remove(jobId);
        int resultErrorCode = 0;

        if (null != curJobData) {
            // update statistics
            m_serverManager.getStatistics().decrementStatefulJobCount();

            curJobData.clear();

            if (ServerManager.isLogTrace()) {
                ServerManager.logTrace("DC PageConverter finished page conversion", new LogData("jobid", jobId));
            }
        } else {
            resultErrorCode = JobError.GENERAL.getErrorCode();

            if (ServerManager.isLogDebug()) {
                ServerManager.logDebug("DC PageConverter has no information about given jobId in #endConversion call", new LogData("jobid", jobId));
            }
        }

        resultProperties.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(resultErrorCode));
    }


    // - Implementation --------------------------------------------------------

    /**
    *
    */
    private void implAutoCleanup(boolean force) {
        final int sizeBefore = m_jobDatas.size();

        if (sizeBefore > 0) {
            final boolean trace  = ServerManager.isLogTrace();
            final long curTimeMillis = System.currentTimeMillis();
            Set<String> jobIdsToRemove = null;

            synchronized (m_jobDatas) {
                for (final String curJobId : m_jobDatas.keySet()) {
                    final JobData curJobData = m_jobDatas.get(curJobId);

                    if (null != curJobData) {
                        final long timestampMillis = curJobData.getTimeStampMillis();

                        if (force || ((curTimeMillis - timestampMillis) >= m_autoCleanupTimeoutMillis)) {

                            if (null == jobIdsToRemove) {
                                jobIdsToRemove = new HashSet<>();
                            }

                            jobIdsToRemove.add(curJobId);
                        }
                    }
                }
            }

            if ((null != jobIdsToRemove) && (jobIdsToRemove.size() > 0)) {
                // dummy job and result properties
                final HashMap<String, Object> resultProperties = new HashMap<>(4);
                final boolean warn = ServerManager.isLogWarn();

                for (final String curJobId : jobIdsToRemove) {
                    endConversion(curJobId, resultProperties);

                    if (warn) {
                        ServerManager.logWarn("DC PageConverter automatically finishes stateful conversion by calling #endConvert", new LogData("jobid", curJobId));
                    }
                }
            }

            if (trace) {
                ServerManager.logTrace(new StringBuilder(512).
                    append("DC PageConverter auto cleanup handler finished ").
                    append((null != jobIdsToRemove) ? jobIdsToRemove.size() : 0).append(" / ").append(sizeBefore).
                    append(" open page conversion(s) after a configured timeout of ").
                    append(m_autoCleanupTimeoutMillis / 1000).append("s. ").
                    append(m_jobDatas.size()).append(" page conversion(s) still open.").toString());
            }
        }
    }

    /**
     * @param pageJobType
     * @param pdfInputFile
     * @param jobProperties
     * @return
     */
    private HashMap<String, Object> implCreatePageJobProperties(
        @NonNull final String pageJobType,
        @NonNull final File pdfInputFile,
        @NonNull final HashMap<String, Object> jobProperties) {

        final HashMap<String, Object> pageJobProperties = new HashMap<>(12);
        final String pageInfoFilename = ((String) jobProperties.getOrDefault(Properties.PROP_INFO_FILENAME, "input")) + ".pdf";
        final String pageInputURL = (String) jobProperties.getOrDefault(Properties.PROP_INPUT_URL, "file:///" + pageInfoFilename);
        final JobPriority pageJobPriority = (JobPriority) jobProperties.getOrDefault(Properties.PROP_PRIORITY, JobPriority.MEDIUM);
        final String pageLocale = (String) jobProperties.get(Properties.PROP_LOCALE);
        final Boolean pageAutoScale = (Boolean) jobProperties.get(Properties.PROP_AUTOSCALE);

        pageJobProperties.put(Properties.PROP_INPUT_FILE, pdfInputFile);
        pageJobProperties.put(Properties.PROP_INPUT_TYPE, "pdf");
        pageJobProperties.put(Properties.PROP_INPUT_URL, pageInputURL);
        pageJobProperties.put(Properties.PROP_PRIORITY, pageJobPriority);
        pageJobProperties.put(Properties.PROP_INFO_FILENAME, pageInfoFilename);

        // locale
        if (null != pageLocale) {
            pageJobProperties.put(Properties.PROP_LOCALE, pageLocale);
        }

        // autoscale
        if (null != pageAutoScale) {
            pageJobProperties.put(Properties.PROP_AUTOSCALE, pageAutoScale);
        }

        // set some additional propertieas for PDF to image conversion
        if ("pdf2grf".equals(pageJobType)) {
            // mime type
            String imageMimeType = (String) jobProperties.get(Properties.PROP_MIME_TYPE);

            // only PNG org JPG mimetype allowed ATM (JPG is default)
            if ((null == imageMimeType) || !(imageMimeType.equalsIgnoreCase("image/png") || imageMimeType.equalsIgnoreCase("image/jpeg"))) {
                imageMimeType = "image/jpeg";
            }

            pageJobProperties.put(Properties.PROP_MIME_TYPE, imageMimeType);

            //pixel width
            final Integer pixelWidth = (Integer) jobProperties.get(Properties.PROP_PIXEL_WIDTH);

            if (null != pixelWidth) {
                pageJobProperties.put(Properties.PROP_PIXEL_WIDTH, pixelWidth);
            }

            //pixel height
            final Integer pixelHeight = (Integer) jobProperties.get(Properties.PROP_PIXEL_HEIGHT);

            if (null != pixelHeight) {
                pageJobProperties.put(Properties.PROP_PIXEL_HEIGHT, pixelHeight);
            }

            //pixel zoom
            final Double pixelZoom = (Double) jobProperties.get(Properties.PROP_PIXEL_ZOOM);

            if (null != pixelZoom) {
                pageJobProperties.put(Properties.PROP_PIXEL_ZOOM, pixelZoom);
            }

            // image scale type
            final String imageScaleType = (String) jobProperties.get(Properties.PROP_IMAGE_SCALE_TYPE);

            if (null != imageScaleType) {
                pageJobProperties.put(Properties.PROP_IMAGE_SCALE_TYPE, imageScaleType);
            }

            // image resolution
            final Integer imageResolution = (Integer) jobProperties.get(Properties.PROP_IMAGE_RESOLUTION);

            if (null != imageResolution) {
                pageJobProperties.put(Properties.PROP_IMAGE_RESOLUTION, imageResolution);
            }
        }

        return pageJobProperties;
    }


    // - Members ---------------------------------------------------------------

    final private ServerManager m_serverManager;

    final private JobProcessor m_jobProcessor;

    final private PDFExtractor m_pdfExtractor;

    final private IQueueMonitor m_queueMonitor;

    final private Map<String, JobData> m_jobDatas = new ConcurrentHashMap<>();

    final private AtomicBoolean m_isRunning = new AtomicBoolean(true);

    private Timer m_autoCleanupTimer = null;

    private long m_autoCleanupTimeoutMillis = 0;
}
