/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import com.openexchange.documentconverter.LogData;
import com.openexchange.documentconverter.NonNull;
import com.openexchange.documentconverter.Nullable;
import com.openexchange.documentconverter.impl.api.REDescriptor;

/**
 * {@link REInstanceProvider}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.1
 */
public class REInstanceProvider extends Thread {

    /**
     * Initializes a new {@link REInstanceProvider}.
     * UNUSED
     */
    @SuppressWarnings("unused")
    private REInstanceProvider() {
        // UNUSED
        super();

        m_serverManager = null;
        m_jobProcessor = null;
        m_descriptor = null;
        m_instanceQueue = null;
        m_instancePoolSize = 0;
        m_instanceCreateThreadPool = null;
    }

    /**
     * Initializes a new {@link REInstanceProvider}.
     */
    public REInstanceProvider(
        @NonNull final ServerManager serverManager,
        @NonNull final JobProcessor jobProcessor,
        @NonNull final REDescriptor descriptor,
        final int instancePoolSize) {

        super("DC ReaderEngine Instance Provider");

        m_serverManager = serverManager;
        m_jobProcessor = jobProcessor;
        m_descriptor = descriptor;
        m_instancePoolSize = instancePoolSize;

        if (m_instancePoolSize > 0) {
            m_instanceQueue = new ArrayBlockingQueue<>(m_instancePoolSize);
            m_instanceCreateThreadPool = Executors.newFixedThreadPool(m_instancePoolSize);

            super.start();

            if (ServerManager.isLogTrace()) {
                ServerManager.logTrace("DC ReaderEngine instance provider created thread pool",
                    new LogData("capacity", Integer.toString(m_instancePoolSize)));
            }
        } else {
            m_instanceQueue = null;
            m_instanceCreateThreadPool = null;

            if (ServerManager.isLogTrace()) {
                ServerManager.logTrace("DC ReaderEngine instance provider is using no thread pool");
            }
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Thread#run()
     */
    @Override
    public void run() {
        while (isRunning()) {
            final int poolInstancesToCreate = (m_instancePoolSize - m_instanceQueue.size());
            final Collection<Callable<Boolean>> callables = new ArrayList<>(poolInstancesToCreate);

            for (int i = 0; i < poolInstancesToCreate; ++i) {
                callables.add(new Callable<Boolean>() {

                    @Override
                    public Boolean call() {
                        final REInstance newPoolInstance = implCreateInstance();
                        boolean ret = false;

                        if (null != newPoolInstance) {
                            ret = m_instanceQueue.offer(newPoolInstance);

                            if (ret) {
                                if (ServerManager.isLogTrace()) {
                                    ServerManager.logTrace(new StringBuilder(128).
                                        append("DC ReaderEngine instance provider added new instance to pool").
                                        append(" (").
                                            append(m_instanceQueue.size()).
                                            append('/').
                                            append(m_instancePoolSize).
                                        append(')').toString());
                                }
                            } else {
                                newPoolInstance.kill();

                                if (ServerManager.isLogTrace()) {
                                    ServerManager.logTrace("DC ReaderEngine instance provider was not able to add new instance to pool due to capacity constraints or termination in progress");
                                }
                            }
                        }

                        return Boolean.valueOf(ret);
                    }
                });
            }

            if (callables.size() > 0) {
                try {
                    m_instanceCreateThreadPool.invokeAll(callables);
                } catch (@SuppressWarnings("unused") InterruptedException e) {
                    // ok, we got interrupted for whatever reason
                }
            }

            // wait for 1s max. till next insert check or
            // if this thread is (preferably) notified
            // after taking a pool instance
            synchronized (this) {
                try {
                    wait(1000);
                } catch (@SuppressWarnings("unused") InterruptedException e) {
                    // ok, we got interrupted for whatever reason
                }
            }
        }

        // tracing thread has finished
        if (ServerManager.isLogInfo()) {
            ServerManager.logInfo("DC ReaderEngine instance provider thread finished");
        }
    }

    /**
     *
     */
    public void terminate() {
        if (m_isRunning.compareAndSet(true, false)) {
            final boolean trace = ServerManager.isLogTrace();
            long traceStartTimeMillis = 0;

            if (trace) {
                traceStartTimeMillis = System.currentTimeMillis();
                ServerManager.logTrace("DC ReaderEngine instance provider starting shutdown...");
            }

            if (null != m_instanceCreateThreadPool) {
                m_instanceCreateThreadPool.shutdownNow();
            }

            interrupt();

            if (trace) {
                ServerManager.logTrace(new StringBuilder(256).
                    append("DC ReaderEngine ReaderEngine instance provider finished shutdown: ").
                    append(System.currentTimeMillis() - traceStartTimeMillis).append("ms").
                    append('!').toString());
            }
        }
    }

    /**
     * @return
     */
    public boolean isRunning() {
        return m_isRunning.get();
    }

    /**
     * @return
     */
    @Nullable public REInstance getREInstance() {
        REInstance ret = null;

        if (isRunning()) {
            if ((null != m_instanceQueue) && !m_instanceQueue.isEmpty()) {
                try {
                    ret = m_instanceQueue.take();

                    if (null != ret) {
                        synchronized (this) {
                            // notify thread worker that space has become available
                            // and a new pool instance can be added to the pool
                            notify();
                        }

                        if (ServerManager.isLogTrace()) {
                            ServerManager.logTrace("DC ReaderEngine instance provider returned instance from pool");
                        }
                    }
                } catch (@SuppressWarnings("unused") InterruptedException e) {
                    // ok, we need to create a new instance
                }
            }

            if (null == ret) {
                ret = implCreateInstance();
            }
        }

        return ret;
    }

    // - Implementation --------------------------------------------------------

    /**
     * @return
     */
    protected REInstance implCreateInstance() {
        REInstance ret = null;

        if (isRunning()) {
            if (!(ret = new REInstance(m_serverManager, m_jobProcessor, m_descriptor)).launch()) {
                ret.kill();
                ret = null;
            }

            if (ServerManager.isLogTrace()) {
                ServerManager.logTrace(((null != ret) ?
                    "DC ReaderEngine instance provider created and launched new instance: " :
                        "DC ReaderEngine instance provider was not able to create and launch new instance:") +
                    m_descriptor.installPath);
            }
        }

        return ret;
    }

    // - Members ---------------------------------------------------------------

    final protected BlockingQueue<REInstance> m_instanceQueue;

    final int m_instancePoolSize;

    final private AtomicBoolean m_isRunning = new AtomicBoolean(true);

    final private ServerManager m_serverManager;

    final private JobProcessor m_jobProcessor;

    final private REDescriptor m_descriptor;

    final private ExecutorService m_instanceCreateThreadPool;
}
