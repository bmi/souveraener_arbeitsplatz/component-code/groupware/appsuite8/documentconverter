/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl;


/**
 * {@link ScaleType}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v8.0.0
 */
public enum ScaleType {
    CONTAIN,
    COVER;

    final private static String CONTAIN_STR = "contain";

    final private static String COVER_STR = "cover";

    @Override
    public String toString() {
        if (ScaleType.COVER == this) {
            return COVER_STR;
        }

        return CONTAIN_STR;
    }

    /**
     * @param scaleTypeStr
     * @return
     */
    public static ScaleType fromString(final String scaleTypeStr) {
        if ((null != scaleTypeStr) && (scaleTypeStr.toLowerCase().startsWith(COVER_STR))) {
            return ScaleType.COVER;
        }

        return ScaleType.CONTAIN;
    }
}
