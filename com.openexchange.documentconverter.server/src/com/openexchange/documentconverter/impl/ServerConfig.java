/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import javax.net.ssl.SSLContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.config.ConfigurationService;
import com.openexchange.documentconverter.NonNull;
import com.openexchange.documentconverter.Nullable;
import com.openexchange.documentconverter.Properties;

/**
 * {@link Config}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
/**
 * {@link ServerConfig}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
public class ServerConfig {

    /**
     * MAX_CACHE_ENTRYCOUNT
     */
    final public static int MAX_CACHE_ENTRYCOUNT = 15000000;

    /**
     * HTTP_SCHEMA
     */
    final private static String HTTP_SCHEMA = "http://";

    /**
     * HTTP_SCHEMA
     */
    final private static String HTTPS_SCHEMA = "https://";

    /**
     * Initializes a new {@link ServerConfig}.
     * @param configService
     */
    public ServerConfig(@NonNull final ConfigurationService configService, @Nullable SSLContext sslContext) {
        SSL_CONTEXT = sslContext;
        implReadConfig(configService);
    }

    /**
     * The {@link SSLContext} to be used for client connections to Http servers
     */
    public SSLContext SSL_CONTEXT = null;

    /**
     * the directory, containing the ./program directory of the underlying readerEngine
     */
    public String RE_INSTALLDIR = "/opt/readerengine";

    /**
     * the directory, containing the cache for persistent, huge job data at runtime
     */
    public String CACHEDIR = "/var/spool/open-xchange/documentconverter/readerengine.cache";

    /**
     * the mapping object from hash strings to target hash directories
     */
    public int CACHEDIR_DEPTH = 0;

    /**
     * a temp. directory, containing the runtime data for each started engine process
     */
    public String SCRATCHDIR = "/var/spool/open-xchange/documentconverter/readerengine.scratch";

    /**
     * a directory, containing the long term statistics data
     */
    public String LTSDIR = "/var/spool/open-xchange/documentconverter/readerengine.scratch/oxdc.lts";

    /**
     * a temp. directory, containing files that could not be loaded due to an error condition or due to a timeout
     */
    public String ERRORDIR = null;

    /**
     * external URL blacklist file
     */
    public String URL_BLACKLIST_FILE = "/opt/open-xchange/documentconverter/etc/readerengine.blacklist";

    /**
     * external URL whitelist file
     */
    public String URL_WHITELIST_FILE = "/opt/open-xchange/documentconverter/etc/readerengine.whitelist";

    /**
     * external URL maximum link count
     */
    public int URL_LINK_LIMIT = 200;

    /**
     * proxy server for external readerengine URLs
     */
    public String URL_LINK_PROXY = null;

    /**
     * Cache URL(s) to be used for remote conversions; the local backend will handle caching in every case, but a cache speedup can be
     * achieved by adding additional cache servers
     */
    public String[] REMOTE_CACHEURLS = null;

    /**
     * the number of engines, working in parallel to execute jobs;
     */
    public int JOB_PROCESSOR_COUNT = 3;

    /**
     * the number of engines, held within an instance pool;
     */
    public int JOB_PROCESSOR_POOLSIZE = 0;

    /**
     * the maximum number of executed jobs, after which a single engine is automatically restarted;
     */
    public int JOB_RESTART_COUNT = 50;

    /**
     * Restart a job after a TIMEOUT error occurs
     */
    public boolean JOB_RESTART_AFTER_TIMEOUT = false;

    /**     * the timeout in milliseconds, after which the execution of a single job is terminated
     */
    public long JOB_EXECUTION_TIMEOUT_MILLISECONDS = 60000;

    /**
     * the maximum size in MegaBytes of virtual memory that a readerengine instance can allocate; -1 for no upper limit
     */
    public long MAX_VMEM_MB = 2048;

    /**
     * the maximum size in MegaBytes for source files to be converted; -1 for no upper limit
     */
    public long MAX_SOURCEFILESIZE_MB = 128;

    /**
     * the minimum size in MegaBytes of the volume, the cache is located on, that will not be used by the cache;
     */
    public long MIN_FREE_VOLUMESIZE_MB = 1024;

    /**
     * the maximum size in MegaBytes of all persistently cached converter job entries at runtime; the total cache size will also take into
     * account the value for the minimum free volume size, so that at least this free volume size will not be occupied by the cache -1 for
     * no upper limit
     */
    public long MAX_CACHESIZE_MB = -1;

    /**
     * the maximum count of converter jobs cached at runtime
     */
    public long MAX_CACHEENTRIES = 4000000;

    /**
     * The percentage of MAX_CACHEENTRIES when the cache clean cleanup thread starts to cleanup cache entries down to the number
     * of CACHE_CLEANUP_THRESHOLD_LOWER_PERCENTAGE of MAX_CACHEENTRIES.
     * Internally limited to 1 <= CACHE_CLEANUP_THRESHOLD_UPPER_PERCENTAGE <= 100.
     */
    public long CACHE_CLEANUP_THRESHOLD_UPPER_PERCENTAGE = 95;

    /**
     * The percentage of MAX_CACHEENTRIES when the cache clean stops to cleanup cache entries.
     * Internally limited to 0 <= CACHE_CLEANUP_THRESHOLD_LOWER_PERCENTAGE <= CACHE_CLEANUP_THRESHOLD_UPPER_PERCENTAGE.
     */
   public long CACHE_CLEANUP_THRESHOLD_LOWER_PERCENTAGE = 95;

    /**
     * the timeout in seconds, after which a cached job execution result is automatically removed from the cache 0 for disabling the timeout
     * based removal
     */
    public long CACHEENTRY_TIMEOUT_SECONDS = 2592000;

    /**
     * the period in seconds, after which the next cache cleanup should happen
     */
    public long CACHE_CLEANUP_PERIOD_SECONDS = 300;

    /**
     * the period in seconds for the which the cache tries to reestablish a connection to remote cache server
     */
    public long READINESS_UP_RECOVER_PERIOD_SECONDS= 20;

    /**
     * the maximum size in MegaBytes of virtual memory that a PDFTool instance can allocate; -1 for no upper limit
     */
    public long PDFTOOL_MAX_VMEM_MB = 1024;

    /**
     * Flag to forbid/allow the servlet engine to handle local file:// URLS
     */
    public boolean ALLOW_LOCAL_FILE_URLS = false;

    /**
     * the timeout in milliseconds, after which an entry is removed from the errorcache
     */
    public long ERROR_CACHE_TIMEOUT_SECONDS = 600;

    /**
     * AUTO_CLEANUP_TIMEOUT_SECONDS
     */
    public long AUTO_CLEANUP_TIMEOUT_SECONDS = 900;

    /**
     * the max. number of cycles, a hash is added to the errorcache,
     * until it won't be removed from the errorcache anymore
     */
    public int ERROR_CACHE_MAX_CYCLE_COUNT = 5;

    /**
     * JOB_ASYNC_QUEUE_COUNT_LIMIT_HIGH
     */
    public int JOB_ASYNC_QUEUE_COUNT_LIMIT_HIGH = 2048;

    /**
     * JOB_QUEUE_COUNT_LIMIT_HIGH
     */
    public int JOB_QUEUE_COUNT_LIMIT_HIGH = 40;

    /**
     * JOB_QUEUE_COUNT_LIMIT_LOW
     */
    public int JOB_QUEUE_COUNT_LIMIT_LOW = 30;

    /**
     * JOB_QUEUE_TIMEOUT_SECONDS
     */
    public int JOB_QUEUE_TIMEOUT_SECONDS = 300;

    // The JPEG image quality percentage value when exporting documents to PDF
    public int PDF_JPEG_QUALITY_PERCENTAGE = 75;

    public long LIVENESS_PERIOD_SECONDS = 10;

    public long LIVENESS_DOWN_AFTER_READINESS_DOWN_SECONDS = 15;

    public long READINESS_PERIOD_SECONDS = 5;

    public long READINESS_DOWN_AFTER_USED_SERVICE_UNAVAILABILITY_SECONDS = 300;

    /**
     * @param configService
     */
    void implReadConfig(@NonNull final ConfigurationService configService) {
        final String emptyString = "";
        String curStringEntry = emptyString;
        final int emptyInt = Integer.MIN_VALUE;
        int curIntEntry = emptyInt;

        // RE_INSTALLDIR
        if ((curStringEntry = configService.getProperty("com.openexchange.documentconverter.installDir", emptyString)) != emptyString) {
            RE_INSTALLDIR = curStringEntry;
        }

        // CACHEDIR
        if ((curStringEntry = configService.getProperty("com.openexchange.documentconverter.cacheDir", emptyString)) != emptyString) {
            CACHEDIR = curStringEntry;
        }

        // CACHEDIR
        if ((curIntEntry = configService.getIntProperty("com.openexchange.documentconverter.cacheDirDepth", emptyInt)) != emptyInt) {
            CACHEDIR_DEPTH = curIntEntry;
        }

        // SCRATCHDIR and LTSDIR
        if ((curStringEntry = configService.getProperty("com.openexchange.documentconverter.scratchDir", emptyString)) != emptyString) {
            // add an own tmp. directory to the given path, we can work within and completely destroy after usage without doing any harm
            SCRATCHDIR = new File(curStringEntry, Properties.OX_DOCUMENTCONVERTER_TEMPDIR_NAME).toString();
            LTSDIR = new File(curStringEntry, Properties.OX_DOCUMENTCONVERTER_LTSDIR_NAME).toString();
        }

        // ERRORDIR
        if ((curStringEntry = configService.getProperty("com.openexchange.documentconverter.errorDir", emptyString)) != emptyString) {
            ERRORDIR = curStringEntry;
        }

        // URL_BLACKLIST_FILE
        if ((curStringEntry = configService.getProperty("com.openexchange.documentconverter.blacklistFile", emptyString)) != emptyString) {
            URL_BLACKLIST_FILE = curStringEntry;
        }

        // URL_WHITELIST_FILE
        if ((curStringEntry = configService.getProperty("com.openexchange.documentconverter.whitelistFile", emptyString)) != emptyString) {
            URL_WHITELIST_FILE = curStringEntry;
        }

        // URL_LINK_LIMIT
        if ((curIntEntry = configService.getIntProperty("com.openexchange.documentconverter.urlLinkLimit", emptyInt)) != emptyInt) {
            URL_LINK_LIMIT = curIntEntry;
        }

        // URL_LINK_PROXY
        if ((curStringEntry = configService.getProperty("com.openexchange.documentconverter.urlLinkProxy", emptyString)) != emptyString) {
            URL_LINK_PROXY = curStringEntry;
        }

        // REMOTE_CACHEURLS
        if ((curStringEntry = configService.getProperty("com.openexchange.documentconverter.RemoteCacheUrls", emptyString)) != emptyString) {
            final StringTokenizer tokenizer = new StringTokenizer(curStringEntry, " \t\n\r\f,;");
            final List<String> urlStrList = new ArrayList<>();

            while (tokenizer.hasMoreTokens()) {
                final String curToken = tokenizer.nextToken();

                if (null != curToken) {
                    final String trimmedToken = curToken.trim();

                    if (trimmedToken.startsWith(HTTP_SCHEMA) || trimmedToken.startsWith(HTTPS_SCHEMA)) {
                        urlStrList.add(trimmedToken);
                    }
                } else {
                    LOG.warn("com.openexchange.documentconverter.RemoteCacheUrls contains an invalid http(s):// URL: " + curToken);
                }
            }

            REMOTE_CACHEURLS = urlStrList.isEmpty() ? null : urlStrList.toArray(new String[urlStrList.size()]);
        }

        // JOB_PROCESSOR_COUNT
        if ((curIntEntry = configService.getIntProperty("com.openexchange.documentconverter.jobProcessorCount", emptyInt)) != emptyInt) {
            JOB_PROCESSOR_COUNT = Math.max(1, curIntEntry);
        }

        // JOB_RESTART_COUNT
        if ((curIntEntry = configService.getIntProperty("com.openexchange.documentconverter.jobRestartCount", emptyInt)) != emptyInt) {
            JOB_RESTART_COUNT = Math.max(0, curIntEntry);
        }

        // JOB_RESTART_AFTER_TIMEOUT
        JOB_RESTART_AFTER_TIMEOUT = configService.getBoolProperty("com.openexchange.documentconverter.jobRestartAfterTimeout", false);

        // JOB_PROCESSOR_POOLSIZE
        if ((curIntEntry = configService.getIntProperty("com.openexchange.documentconverter.processorPoolSize", emptyInt)) != emptyInt) {
            JOB_PROCESSOR_POOLSIZE = Math.max(0, curIntEntry);
        }

        // JOB_EXECUTION_TIMEOUT_MILLISECONDS
        if ((curIntEntry = configService.getIntProperty("com.openexchange.documentconverter.jobExecutionTimeoutMilliseconds", emptyInt)) != emptyInt) {
            JOB_EXECUTION_TIMEOUT_MILLISECONDS = Math.max(1, curIntEntry);
        }

        // MAX_VMEM_MB
        if ((curIntEntry = configService.getIntProperty("com.openexchange.documentconverter.maxVMemMB", emptyInt)) != emptyInt) {
            MAX_VMEM_MB = Math.max(1, curIntEntry);
        }

        // PDFTOOL_MAX_VMEM_MB
        if ((curIntEntry = configService.getIntProperty("com.openexchange.documentconverter.pdftoolMaxVMemMB", emptyInt)) != emptyInt) {
            PDFTOOL_MAX_VMEM_MB = Math.max(1, curIntEntry);
        }

        // MAX_SOURCEFILESIZE_MB
        if ((curIntEntry = configService.getIntProperty("com.openexchange.documentconverter.maxSourceFileSizeMB", emptyInt)) != emptyInt) {
            MAX_SOURCEFILESIZE_MB = Math.max(1, curIntEntry);
        }

        // MIN_FREE_VOLUMESIZE_MB
        if ((curIntEntry = configService.getIntProperty("com.openexchange.documentconverter.minFreeVolumeSizeMB", emptyInt)) != emptyInt) {
            MIN_FREE_VOLUMESIZE_MB = Math.max(0, curIntEntry);
        }

        // MAX_CACHESIZE_MB
        if ((curIntEntry = configService.getIntProperty("com.openexchange.documentconverter.maxCacheSizeMB", emptyInt)) != emptyInt) {
            MAX_CACHESIZE_MB = Math.max(-1, curIntEntry);
        }

        // MAX_CACHEENTRIES
        if ((curIntEntry = configService.getIntProperty("com.openexchange.documentconverter.maxCacheEntries", emptyInt)) != emptyInt) {
            MAX_CACHEENTRIES = ((curIntEntry < 0) || (curIntEntry > MAX_CACHE_ENTRYCOUNT)) ?
                MAX_CACHE_ENTRYCOUNT :
                    curIntEntry;
        }

        // CACHE_CLEANUP_THRESHOLD_UPPER_PERCENTAGE
        if ((curIntEntry = configService.getIntProperty("com.openexchange.documentconverter.cacheCleanupThresholdUpperPercentage", emptyInt)) != emptyInt) {
            CACHE_CLEANUP_THRESHOLD_UPPER_PERCENTAGE = Math.min(100, Math.max(1, curIntEntry));
        }

        // CACHE_CLEANUP_THRESHOLD_LOWER_PERCENTAGE
        if ((curIntEntry = configService.getIntProperty("com.openexchange.documentconverter.cacheCleanupThresholdLowerPercentage", emptyInt)) != emptyInt) {
            CACHE_CLEANUP_THRESHOLD_LOWER_PERCENTAGE = Math.min(CACHE_CLEANUP_THRESHOLD_UPPER_PERCENTAGE, curIntEntry);
        }

        // CACHEENTRY_TIMEOUT_SECONDS
        if ((curIntEntry = configService.getIntProperty("com.openexchange.documentconverter.cacheEntryTimeoutSeconds", emptyInt)) != emptyInt) {
            CACHEENTRY_TIMEOUT_SECONDS = Math.max(0, curIntEntry);
        }

        // CACHE_CLEANUP_PERIOD_SECONDS
        if ((curIntEntry = configService.getIntProperty("com.openexchange.documentconverter.cacheCleanupPeriodSeconds", emptyInt)) != emptyInt) {
            CACHE_CLEANUP_PERIOD_SECONDS = Math.max(0, curIntEntry);
        }

        // READINESS_UP_RECOVER_PERIOD_SECONDS
        if ((curIntEntry = configService.getIntProperty("com.openexchange.documentconverter.probe.readiness.upRecoveryPeriodSeconds", emptyInt)) != emptyInt) {
            READINESS_UP_RECOVER_PERIOD_SECONDS = Math.max(1, curIntEntry);
        }

        // ERROR_CACHE_TIMEOUT_SECONDS
        if ((curIntEntry = configService.getIntProperty("com.openexchange.documentconverter.errorCacheTimeoutSeconds", emptyInt)) != emptyInt) {
            ERROR_CACHE_TIMEOUT_SECONDS = Math.max(0, curIntEntry);
        }

        // AUTO_CLEANUP_TIMEOUT_SECONDS
        if ((curIntEntry = configService.getIntProperty("com.openexchange.documentconverter.autoCleanupTimeoutSeconds", emptyInt)) != emptyInt) {
            AUTO_CLEANUP_TIMEOUT_SECONDS = Math.max(0, curIntEntry);
        }

        // ERROR_CACHE_MAX_CYCLE_COUNT
        if ((curIntEntry = configService.getIntProperty("com.openexchange.documentconverter.errorCacheMaxCycleCount", emptyInt)) != emptyInt) {
            ERROR_CACHE_MAX_CYCLE_COUNT = Math.max(0, curIntEntry);
        }

        // JOB_QUEUE_COUNT_LIMIT_HIGH
        if ((curIntEntry = configService.getIntProperty("com.openexchange.documentconverter.jobQueueCountLimitHigh", emptyInt)) != emptyInt) {
            JOB_QUEUE_COUNT_LIMIT_HIGH = Math.max(1, curIntEntry);
        }

        // JOB_QUEUE_COUNT_LIMIT_LOW
        if ((curIntEntry = configService.getIntProperty("com.openexchange.documentconverter.jobQueueCountLimitLow", emptyInt)) != emptyInt) {
            JOB_QUEUE_COUNT_LIMIT_LOW = Math.max(0, curIntEntry);
        }

        // adjust for sensible values (JOB_QUEUE_COUNT_LIMIT_HIGH > JOB_QUEUE_COUNT_LIMIT_LOW)
        if (JOB_QUEUE_COUNT_LIMIT_LOW >= JOB_QUEUE_COUNT_LIMIT_HIGH) {
            // lower limit higher or equal than higher limit does not make any sense
            JOB_QUEUE_COUNT_LIMIT_LOW = JOB_QUEUE_COUNT_LIMIT_HIGH - 1;
        }

        // JOB_QUEUE_TIMEOUT_SECONDS
        if ((curIntEntry = configService.getIntProperty("com.openexchange.documentconverter.jobQueueTimeoutSeconds", emptyInt)) != emptyInt) {
            JOB_QUEUE_TIMEOUT_SECONDS = Math.max(1, curIntEntry);
        }

        // JOB_ASYNC_QUEUE_COUNT_LIMIT_HIGH
        if ((curIntEntry = configService.getIntProperty("com.openexchange.documentconverter.jobAsyncQueueCountLimitHigh", emptyInt)) != emptyInt) {
            JOB_ASYNC_QUEUE_COUNT_LIMIT_HIGH = Math.max(1, curIntEntry);
        }

        // ALLOW_LOCAL_FILE_URLS
        ALLOW_LOCAL_FILE_URLS = configService.getBoolProperty("com.openexchange.documentconverter.servletLocalFileUrls", false);

        // PDF_JPEG_COMPRESSION_PERCENTAGE
        if ((curIntEntry = configService.getIntProperty("com.openexchange.documentconverter.pdf.jpegQualityPercentage", emptyInt)) != emptyInt) {
            PDF_JPEG_QUALITY_PERCENTAGE = Math.min(100, Math.max(1, curIntEntry));
        }

        // LIVENESS_PERIOD_SECONDS
        if ((curIntEntry = configService.getIntProperty("com.openexchange.documentconverter.probe.liveness.periodSeconds", emptyInt)) != emptyInt) {
            LIVENESS_PERIOD_SECONDS = Math.max(1, curIntEntry);
        }

        // LIVENESS_DOWN_AFTER_READINESS_DOWN_SECONDS
        if ((curIntEntry = configService.getIntProperty("com.openexchange.documentconverter.probe.liveness.downAfterReadinessDownSeconds", emptyInt)) != emptyInt) {
            LIVENESS_DOWN_AFTER_READINESS_DOWN_SECONDS = curIntEntry;
        }

        // READINESS_PERIOD_SECONDS
        if ((curIntEntry = configService.getIntProperty("com.openexchange.documentconverter.probe.readiness.periodSeconds", emptyInt)) != emptyInt) {
            READINESS_PERIOD_SECONDS = Math.max(1, curIntEntry);
        }

        // READINESS_DOWN_AFTER_USED_SERVICE_UNAVAILABILITY_SECONDS
        if ((curIntEntry = configService.getIntProperty("com.openexchange.documentconverter.probe.readiness.downAfterUsedServiceUnavailabilitySeconds", emptyInt)) != emptyInt) {
            READINESS_DOWN_AFTER_USED_SERVICE_UNAVAILABILITY_SECONDS = curIntEntry;
        }
    }

    // - Members ---------------------------------------------------------------

    /**
     * LOG
     */
    final private static Logger LOG = LoggerFactory.getLogger(ServerConfig.class);
}
