/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import com.openexchange.conversion.DataExceptionCodes;
import com.openexchange.documentconverter.AsyncExecutor;
import com.openexchange.documentconverter.DataSourceFile;
import com.openexchange.documentconverter.DocumentConverterManager;
import com.openexchange.documentconverter.DocumentConverterUtil;
import com.openexchange.documentconverter.HttpHelper;
import com.openexchange.documentconverter.IDocumentConverter;
import com.openexchange.documentconverter.JobError;
import com.openexchange.documentconverter.JobErrorEx;
import com.openexchange.documentconverter.LogData;
import com.openexchange.documentconverter.MutableWrapper;
import com.openexchange.documentconverter.NonNull;
import com.openexchange.documentconverter.Nullable;
import com.openexchange.documentconverter.PDFExtractor;
import com.openexchange.documentconverter.Properties;
import com.openexchange.documentconverter.ServerType;
import com.openexchange.documentconverter.SingletonService;
import com.openexchange.documentconverter.impl.api.BackendType;
import com.openexchange.documentconverter.impl.api.CacheDescriptor;
import com.openexchange.documentconverter.impl.api.DataSourceFileInputStream;
import com.openexchange.documentconverter.impl.api.IJob;
import com.openexchange.documentconverter.impl.api.IQueueMonitor;
import com.openexchange.documentconverter.impl.api.IdLocker;
import com.openexchange.documentconverter.impl.api.REDescriptor;
import com.openexchange.exception.OXException;
import com.openexchange.tools.net.URITools;

/**
 * {@link ServerManager}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.8.0
 */
@SingletonService
public class ServerManager extends DocumentConverterManager implements IDocumentConverter {

    // - UserData --------------------------------------------------------------

    final public static String USERDATA_KEY_STATISTICS = "DC_MANAGER_USERDATA_STATISTICS";

    // -------------------------------------------------------------------------

    final public static Integer IMAGE_RESOLUTION_ORIGINAL = Integer.valueOf(-1);

    // -------------------------------------------------------------------------

    final public static String ERROR_FILE_SUBDIR = "error";

    final public static String TIMEOUT_FILE_SUBDIR = "timeout";

    final public static String DEBUG_FILE_SUBDIR = "debug";

    // -------------------------------------------------------------------------

    final public static String LTS_FILENAME = "oxdc.ltsdata";

    /**
     * @param services
     * @param config
     * @return
     */
    @NonNull static public synchronized ServerManager newInstance(@NonNull ServerConfig config) {
        if (null == DC_MANAGER) {
            DC_MANAGER = new ServerManager(config);
        }

        return DC_MANAGER;
    }

    /**
     * @return
     */
    @Nullable static public synchronized ServerManager get() {
        return DC_MANAGER;
    }


    /**
     * Initializes a new {@link ServerManager}.
     *
     * @param config
     */
    private ServerManager(@NonNull ServerConfig serverConfig) {
        super(new File(serverConfig.SCRATCHDIR));

        DC_MANAGER = this;
        m_serverConfig = serverConfig;

        implInit();

        // initialize timer to clean up temp. files created by resolved URLs
        m_timerExecutor.scheduleAtFixedRate(() -> {
            final long curTimeMillis = System.currentTimeMillis();

            synchronized (URL_RESOLVED_FILE_LIST) {
                for (final Iterator<File> curFileIter = URL_RESOLVED_FILE_LIST.iterator(); curFileIter.hasNext();) {
                    final File curFile = curFileIter.next();

                    if (null != curFile) {
                        if ((curTimeMillis - curFile.lastModified()) > URL_RESOLVED_FILE_CLEANUP_TIMEOUT_MILLIS) {
                            curFileIter.remove();
                            FileUtils.deleteQuietly(curFile);
                        } else {
                            // leave loop as items are ordered by time
                            break;
                        }
                    }
                }
            }
        }, URL_RESOLVED_FILE_CLEANUP_TIMEOUT_MILLIS, URL_RESOLVED_FILE_CLEANUP_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS);

    }

    /**
     * @param config
     * @return
     */
    public PDFExtractor getPDFExtractor() {
        return m_pdfExtractor;
    }

    /**
     *
     */
    public void shutdown() {
        if ((null != m_jobProcessor) && m_isRunning.compareAndSet(true, false)) {
            // terminate everything
            long shutdownStartTime = 0;

            if (isLogInfo()) {
                shutdownStartTime = System.currentTimeMillis();
                logInfo("DC ServerManager starting shutdown...");
            }

            if (null != m_timerExecutor) {
                m_timerExecutor.shutdownNow();
            }

            if (null != m_pageConverter) {
                m_pageConverter.shutdown();
            }

            if (null != m_asyncExecutor) {
                m_asyncExecutor.shutdown();
                m_asyncExecutor = null;
            }

            if (null != m_jobProcessor) {
                m_jobProcessor.shutdown();
                m_jobProcessor = null;
            }

            if (null != m_cache) {
                m_cache.shutdown();
                m_cache = null;
            }

            // shutdown statistics related resources (e.g. timer threads)
            if (null != m_statistics) {
                m_statistics.terminate();
                m_statistics = null;
            }

            if (isLogInfo()) {
                logInfo(new StringBuilder(256).
                    append("DC ServerManager finished shutdown: ").
                    append(System.currentTimeMillis() - shutdownStartTime).append("ms").
                    append('!').toString());
            }
        }

        // delete scratch directory
        synchronized (this) {
            if (null != m_engineScratchDir) {
                try {
                    FileUtils.deleteDirectory(m_engineScratchDir);
                } catch (final IOException e) {
                    logExcp(e);
                } finally {
                    m_engineScratchDir = null;
                }
            }
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.IDocumentConverter#convert(java.lang.String, java.util.HashMap, java.util.HashMap)
     */
    @Override
    public InputStream convert(String jobType, HashMap<String, Object> jobProperties, HashMap<String, Object> resultProperties) {
        // init measuring, if requested
        final InputStream ret = (new ManagedJob(this, m_jobProcessor, m_queueMonitor)).process(jobType, jobProperties, resultProperties);

        // increment user request count, if indicated
        getStatistics().incrememtUserRequestCount(jobProperties, resultProperties);

        return ret;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverter#beginPageConversion(java.lang.String, java.util.HashMap, java.util.HashMap)
     */
    @Override
    public String beginPageConversion(String jobType, HashMap<String, Object> jobProperties, HashMap<String, Object> resultProperties) {
        final String ret = m_pageConverter.beginConversion(jobType, jobProperties, resultProperties);

        // increment user request count, if indicated
        getStatistics().incrememtUserRequestCount(jobProperties, resultProperties);

        return ret;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverter#getConversionPage(java.lang.String, int, java.util.HashMap, java.util.HashMap)
     */
    @Override
    public InputStream getConversionPage(String jobId, int pageNumber, HashMap<String, Object> jobProperties, HashMap<String, Object> resultProperties) {
        return (null != jobId) ?
            m_pageConverter.getPage(jobId, pageNumber, jobProperties, resultProperties) :
                null;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.documentconverter.IDocumentConverter#endPageConversion(java.lang.String, java.util.HashMap, java.util.HashMap)
     */
    @Override
    public void endPageConversion(String jobId, HashMap<String, Object> jobProperties, HashMap<String, Object> resultProperties) {
        if (null != jobId) {
            m_pageConverter.endConversion(jobId, resultProperties);
        }
    }

    // - Public API ------------------------------------------------------------

    /**
     *
     */
    public boolean isRunning() {
        return (m_isRunning.get() && (null != m_jobProcessor));
    }

    /**
     * @return
     */
    public Cache getCache() {
        return m_cache;
    }

    /**
     * @return
     */
    public ServerConfig getServerConfig() {
        return m_serverConfig;
    }

    /**
     * @param documentConverterInformation
     */
    public void setMBean(final DocumentConverterInformation documentConverterInformation) {
        m_dcMBean = documentConverterInformation;
    }

    /**
     * @return
     */
    public DocumentConverterInformation getMBean() {
        return m_dcMBean;
    }


    /**
     * @return
     */
    public LTStatistics getStatistics() {
        return m_statistics;
    }

    /**
     * @return
     */
    public LogData[] getAsyncLogData() {
        return ((null != m_asyncExecutor) ? m_asyncExecutor.getLogData() : new LogData[] {});
    }

    /**
     * @return
     */
    public @NonNull Map<BackendType, EngineStatus> getEngineStatus() {
        final Map<BackendType, EngineStatus> engineStatusMap = new HashMap<>();

        for (final BackendType curBackendType : BackendType.values()) {
            engineStatusMap.put(curBackendType, (null != m_jobProcessor) ? m_jobProcessor.getEngineStatus(curBackendType) : new EngineStatus() { /* using interface defaults */ });
        }

        return engineStatusMap;
    }

    /**
     * @return
     */
    public @NonNull List<String> getServerErrors() {
        return m_serverErrors;
    }

    /**
     * @param jobType
     * @param jobProperties
     * @param resultProperties
     */
    public void triggerAsyncConvert(String jobType, HashMap<String, Object> jobProperties, HashMap<String, Object> resultProperties) {
        m_asyncExecutor.triggerExecution(jobType, jobProperties, resultProperties);
    }

    /**
     * @param _jobType
     * @param jobProperties
     * @param resultProperties
     * @return
     */
    public IJob createServerJob(String _jobType, HashMap<String, Object> jobProperties, HashMap<String, Object> resultProperties) {
        IJob job = null;

        if (null != _jobType) {
            final String jobType = _jobType.toLowerCase();

            jobProperties.put(Properties.PROP_READERENGINE_ROOT, m_serverConfig.RE_INSTALLDIR);

            if ("pdf".equals(jobType) || "pdfa".equals(jobType)) {
                jobProperties.put(Properties.PROP_FILTER_SHORT_NAME, "pdf");
                job = new DocumentConverterJob(jobProperties, resultProperties, jobType);
            } else if (jobType.startsWith("pdf2svg")) {
                job = new PDF2SVGConverterJob(jobProperties, resultProperties);
            } else if (jobType.startsWith("pdf2grf")) {
                job = new PDF2GraphicConverterJob(jobProperties, resultProperties);
            } else if (jobType.startsWith("pdf2html")) {
                job = new PDF2HTMLConverterJob(jobProperties, resultProperties);
            } else if ("ooxml".equals(jobType)) {
                jobProperties.put(Properties.PROP_FILTER_SHORT_NAME, "ooxml");
                job = new DocumentConverterJob(jobProperties, resultProperties, jobType);
            } else if ("odf".equals(jobType)) {
                jobProperties.put(Properties.PROP_FILTER_SHORT_NAME, "odf");
                job = new DocumentConverterJob(jobProperties, resultProperties, jobType);
            } else if ("image".equals(jobType)) {
                job = new ImageConverterJob(jobProperties, resultProperties);
            } else if ("graphic".equals(jobType)) {
                job = new GraphicConverterJob(jobProperties, resultProperties);
            } else if ("shape2png".equals(jobType)) {
                job = new Shape2PNGConverterJob(jobProperties, resultProperties);
            } else if ("imagetransformation".equals(jobType)) {
                job = new ImageConverterJob(jobProperties, resultProperties);
            }
        }

        return job;
    }

    // - Implementation --------------------------------------------------------

    /**
     *
     */
    private void implInit() {
        final ServerConfig usedConfig = m_serverConfig;
        final String engineInstallDir = usedConfig.RE_INSTALLDIR;
        final String engineScratchDir = usedConfig.SCRATCHDIR;
        final String engineErrorDir = usedConfig.ERRORDIR;

        // register class name for compatibility streaming puposes
        DataSourceFileInputStream.registerDCDataSourceClassName(CacheEntry.class.getName());
        DataSourceFileInputStream.registerDCDataSourceClassName(DataSourceFile.class.getName());

        // create scratch directory in every case in order to created temp. files within
        if ((null != engineScratchDir) && (engineScratchDir.length() > 0)) {
            // Scratch dir
            final File scratchDir = new File(engineScratchDir);

            try {
                // try to delete existing one
                FileUtils.deleteDirectory(scratchDir);
            } catch (@SuppressWarnings("unused") final IOException e) {
                // may not exist before => ok
            }

            try {
                FileUtils.forceMkdir(scratchDir);
            } catch (@SuppressWarnings("unused") final IOException e) {
                //
            }

            if (!scratchDir.canWrite()) {
                final String curError = "DC scratch directory could not be created or is write protected => Installation is broken: " + engineScratchDir;

                m_serverErrors.add(curError);
                logError(curError);
            } else {
                m_engineScratchDir = scratchDir;
            }
        }

        // set root tmp directory based on scratch dir, if availble, get default otherwise
        m_tmpRootDir = ((null != m_engineScratchDir) && m_engineScratchDir.canWrite()) ?
            m_engineScratchDir :
                FileUtils.getTempDirectory();

        // create error and directory in case the entry is set within the the config
        if ((null != engineErrorDir) && (engineErrorDir.length() > 0)) {
            // Error dir
            final File errorDir = new File(engineErrorDir, ERROR_FILE_SUBDIR);

            try {
                FileUtils.forceMkdir(errorDir);
            } catch (@SuppressWarnings("unused") final IOException e) {
                //
            }

            if (!errorDir.canWrite()) {
                final String curError = "DC error directory could not be created or is write protected => Error files can't be be saved to: " + errorDir;

                m_serverErrors.add(curError);
                logError(curError);
            } else {
                // Timeout dir
                final File timeoutDir = new File(engineErrorDir, TIMEOUT_FILE_SUBDIR);

                try {
                    FileUtils.forceMkdir(timeoutDir);
                } catch (@SuppressWarnings("unused") final IOException e) {
                    //fromJson
                }

                if (!timeoutDir.canWrite()) {
                    final String curError = "DC timeout directory could not be created or is write protected => Timeout files can't be saved to: " + timeoutDir;

                    m_serverErrors.add(curError);
                    logError(curError);
                }
            }
        }

        if (null != m_engineScratchDir) {
            if (!new File(engineInstallDir).canRead()) {
                final String curError = "DC ReaderEngine install path is not readable => DC will not be available: " + engineInstallDir;

                // Local converter error case
                m_serverErrors.add(curError);
                logError(curError);
            } else {
                final REDescriptor engineDescriptor = new REDescriptor();

                // set engine parameters
                engineDescriptor.installPath = engineInstallDir;
                engineDescriptor.userPath = m_engineScratchDir.getPath();
                engineDescriptor.maxVMemMB = usedConfig.MAX_VMEM_MB;
                engineDescriptor.restartCount = usedConfig.JOB_RESTART_COUNT;
                engineDescriptor.blacklistFile = usedConfig.URL_BLACKLIST_FILE;
                engineDescriptor.whitelistFile = usedConfig.URL_WHITELIST_FILE;
                engineDescriptor.urlLinkLimit = usedConfig.URL_LINK_LIMIT;
                engineDescriptor.urlLinkProxy = usedConfig.URL_LINK_PROXY;

                implFillPatternList(URL_BLACKLIST_PATTERNS, new File(engineDescriptor.blacklistFile));
                implFillPatternList(URL_WHITELIST_PATTERNS, new File(engineDescriptor.whitelistFile));

                // set cache parameters
                final CacheDescriptor cacheDescriptor = new CacheDescriptor();

                cacheDescriptor.cacheDirectory = usedConfig.CACHEDIR;
                cacheDescriptor.cacheDirectoryDepth = usedConfig.CACHEDIR_DEPTH;
                cacheDescriptor.minimumFreeVolumeSize = usedConfig.MIN_FREE_VOLUMESIZE_MB * 1024 * 1024;
                cacheDescriptor.maximumPersistSize = (usedConfig.MAX_CACHESIZE_MB > 0) ?
                    usedConfig.MAX_CACHESIZE_MB * 1024 * 1024 :
                        usedConfig.MAX_CACHESIZE_MB;
                cacheDescriptor.maximumEntryCount = usedConfig.MAX_CACHEENTRIES;
                cacheDescriptor.cleanupThresholdUpperPercentage = usedConfig.CACHE_CLEANUP_THRESHOLD_UPPER_PERCENTAGE;
                cacheDescriptor.cleanupThresholdLowerPercentage = usedConfig.CACHE_CLEANUP_THRESHOLD_LOWER_PERCENTAGE;
                cacheDescriptor.timeoutMillis = usedConfig.CACHEENTRY_TIMEOUT_SECONDS * 1000;
                cacheDescriptor.cleanupPeriodMillis = usedConfig.CACHE_CLEANUP_PERIOD_SECONDS * 1000;
                cacheDescriptor.recoverPeriodMillis = usedConfig.READINESS_UP_RECOVER_PERIOD_SECONDS * 1000;
                cacheDescriptor.remoteCacheUrls = usedConfig.REMOTE_CACHEURLS;
                cacheDescriptor.sslContext = usedConfig.SSL_CONTEXT;

                // create Cache
                m_cache = new Cache(this, cacheDescriptor);

                // create JobProcessor
                m_jobProcessor = new JobProcessor(this, engineDescriptor, m_cache);
                m_queueMonitor = m_jobProcessor.getQueueMonitor();

                if (isLogWarn()) {
                    final String blacklistFile = engineDescriptor.blacklistFile;
                    final String whitelistFile = engineDescriptor.whitelistFile;

                    if ((m_serverConfig.MAX_VMEM_MB < 1500)) {
                        logWarn("com.openexchange.documentconverter.maxVMemMB is set to a very low value of " + m_serverConfig.MAX_VMEM_MB + " (MB). Some conversions of large documents may give an error. Please consider setting a higher value of at least 1500 (MB)");
                    }

                    if ((m_serverConfig.PDFTOOL_MAX_VMEM_MB < 50)) {
                        logWarn("com.openexchange.documentconverter.pdftoolMaxVMemMB is set to a very low value of " + m_serverConfig.PDFTOOL_MAX_VMEM_MB + " (MB). Some processing of large PDF documents may give an error. Please consider setting a higher value of at least 50 (MB)");
                    }

                    if ((null == blacklistFile) || !new File(blacklistFile).canRead()) {
                        logWarn("DC URL blacklist file is not readable", new LogData("filename", (null != blacklistFile) ? blacklistFile : "null"));
                    }

                    if ((null == whitelistFile) || !new File(whitelistFile).canRead()) {
                        logWarn("DC URL whitelist file is not readable", new LogData("filename", (null != whitelistFile) ? whitelistFile : "null"));
                    }
                }

                if (isLogInfo()) {
                    logInfo("DC manager launched");
                }
            }
        }

        // create statistics instance in any case
        m_statistics = new LTStatistics(this, m_jobProcessor, m_cache);

        // create PDFExtractor helper
        final File pdfExtractorExecutable = new File(m_serverConfig.RE_INSTALLDIR, "/program/pdf2svg");

        try {
            m_pdfExtractor = new PDFExtractor(this, pdfExtractorExecutable, usedConfig.PDFTOOL_MAX_VMEM_MB, usedConfig.JOB_EXECUTION_TIMEOUT_MILLISECONDS);
        } catch (@SuppressWarnings("unused") IOException e) {
            logError("DC client PDFExtractor could not be initiaized correctly: " + pdfExtractorExecutable.getAbsolutePath());
        }

        // create PageConverter helper
        m_pageConverter = new PageConverter(
            this,
            m_jobProcessor,
            m_pdfExtractor,
            m_queueMonitor,
            m_serverConfig.AUTO_CLEANUP_TIMEOUT_SECONDS * 1000L);

        // create an executor with (jobProcessorCount - 1) working threads in parallel (minimum 1)
        final int asyncThreadCount = Math.max(1, usedConfig.JOB_PROCESSOR_COUNT - 1);
        final int asyncQueueCountLimitHigh = usedConfig.JOB_ASYNC_QUEUE_COUNT_LIMIT_HIGH;

        m_asyncExecutor = new AsyncServerExecutor(this, this, asyncThreadCount, asyncQueueCountLimitHigh, m_statistics);

        if (isLogInfo()) {
            logInfo("DC asynchronous executor using " + asyncThreadCount + " thread(s)");
        }
    }

    // - Static functions ------------------------------------------------------

    /**
     * @param cache
     * @param hash
     * @param resultProperties
     * @return
     */
    public static InputStream getCachedResult(
        final @NonNull Cache cache,
        final @NonNull String hash,
        final @Nullable String filename,
        final @NonNull HashMap<String, Object> resultProperties,
        final boolean updateCacheHitStatistic) {

        if (!Cache.isValidHash(hash)) {
            return null;
        }

        CacheEntry cacheEntry = null;
        InputStream ret = null;

        IdLocker.lock(hash);

        try {
            cacheEntry = (CacheEntry) cache.getEntry(hash, filename);

            if (null != cacheEntry) {
                final Map<String, Object> result = cacheEntry.getJobResult();

                // try to read result from stored cache entry and assign
                // job result properties to given result properties
                if (null != result) {
                    resultProperties.putAll(result);

                    final byte[] resultBuffer = (byte[]) resultProperties.get(Properties.PROP_RESULT_BUFFER);

                    if (null != resultBuffer) {
                        ret = new ByteArrayInputStream(resultBuffer);
                    }

                    // Logging
                    if (ServerManager.isLogTrace()) {
                        ServerManager.logTrace("DC cache hit => job result read from local cache.",
                            new LogData("hash", hash),
                            new LogData("filename", (null != filename) ? filename : DocumentConverterUtil.STR_UNKNOWN));
                    }
                }

                if (null == ret) {
                    // if a cache entry is found, but the result is not readable from the entry,
                    // remove the (invalid) entry from the cache; it will be recreated afterwards
                    cache.removeEntry(hash);

                    // Logging
                    if (ServerManager.isLogTrace()) {
                        ServerManager.logTrace("DC cache hit, but job result not readable locally => removing entry!",
                            new LogData("hash", hash),
                            new LogData("filename", (null != filename) ? filename : DocumentConverterUtil.STR_UNKNOWN));
                    }
                }
            }
        } finally {
            IdLocker.unlock(hash);
        }

        // update conversion cache hit counter in case of success and if requested
        if (updateCacheHitStatistic && (null != ret)) {
            final ServerManager manager = ServerManager.get();
            final LTStatistics statistics = (null != manager) ? manager.getStatistics() : null;

            if (null != statistics) {
                statistics.incrementProcessedJobCount(new JobErrorEx(), true);
            }
        }

        return ret;
    }

    /**
     * @param jobProperties
     */
    public static void ensureInputTypeSet(HashMap<String, Object> jobProperties) {
        String inputType = (String) jobProperties.get(Properties.PROP_INPUT_TYPE);

        // try to get a valid input type of the source file
        if (StringUtils.isEmpty(inputType)) {
            String fileName = (String) jobProperties.get(Properties.PROP_INFO_FILENAME);

            if (null != fileName) {
                jobProperties.put(Properties.PROP_INPUT_TYPE, inputType = FilenameUtils.getExtension(fileName).trim().toLowerCase());
            }
        }
    }

    /**
     * @param file
     * @return
     */
    public static String getAdjustedFileURL(File file) {
        String ret = null;

        if (null != file) {
            try {
                ret = "file://" + file.toURI().toURL().getPath();
            } catch (final MalformedURLException e) {
                ServerManager.logExcp(e);
            }
        }

        return ret;
    }

    /**
     * @param sleepTimeMillis
     */
    public static void sleepThread(long sleepTimeMillis, boolean force) {
        long curTimeMillis = System.currentTimeMillis();
        final long endTimeMillis =  curTimeMillis + Math.max(sleepTimeMillis, 1);

        do {
            final long curWaitTimeMillis = endTimeMillis - curTimeMillis;

            if (curWaitTimeMillis > 0) {
                try {
                    Thread.sleep(curWaitTimeMillis);
                } catch (@SuppressWarnings("unused") final InterruptedException e) {
                    // ok
                }
            }
        } while (force && ((curTimeMillis = System.currentTimeMillis()) < endTimeMillis));
    }

    /**
     * Returns a default map with a run count of 1 for
     * every {@link JobError} value
     * @return the default error run count map
     */
    public static Map<JobError, Integer> getDefaultSupportedBackendRuns() {
        return DEFAULT_ERROR_RUNCOUNT_MAP;
    }

    /**
     * @param urlString
     * @return
     */
    @Nullable public static URLConnection getValidatedFinalURLConnection(final String urlString, final Optional<Function<URLConnection, Optional<OXException>>> httpGetConnectionDecorator) {
        try {
            // use URITools.getFinalURL call with provided URL_VALIDATOR_FUNC to
            // validate each hop URL of possibly redirected URLs and return final URL
            return URITools.getTerminalConnection(urlString.trim(), Optional.of(URL_VALIDATOR_FUNC), httpGetConnectionDecorator);
        } catch (Exception e) {
            logWarn("DC detected invalid URL (" + e.getMessage() + ")");
        }

        return null;
    }

    /**
     * @param command The command to execute
     * @return The array of output lines of the command.
     *  If command fails, the array of output lines is empty.
     */
    @NonNull public static List<String> executeBashCommand(final String command) {
        final List<String> outputLineList = new LinkedList<>();

        if (null != command) {
            for (String curBashPath : BASH_SEARCH_PATHES) {
                if (new File(curBashPath).canExecute()) {
                    try {
                        final ProcessBuilder processBuilder = new ProcessBuilder(new String[] { curBashPath, "-c", command });
                        final Process process = processBuilder.start();
                        String curLine = null;

                        try (final BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
                            while (null != (curLine = reader.readLine())) {
                                outputLineList.add(curLine);
                            }
                        }

                        break;
                    } catch (IOException e) {
                        logExcp(e);
                    }
                }
            }
        }

        return outputLineList;
    }

    /**
     * @param url
     * @param targetFile
     * @param extensionWrapper
     * @return
     */
    @Nullable public static File resolveURL(final String url, final File targetFile, final MutableWrapper<String> extensionWrapper) {
        URLConnection urlConnection = null;
        File ret = null;

        if (StringUtils.isNotBlank(url) && (null != targetFile) &&
            ((urlConnection = ServerManager.getValidatedFinalURLConnection(url.replace(" ", "%20"), Optional.of(HTTP_GET_CONNECTION_DECORATOR)))
                instanceof HttpURLConnection)) {

             final HttpURLConnection httpURLConnection = (HttpURLConnection) urlConnection;
             final HttpHelper httpHelper = HttpHelper.get();
             final SSLSocketFactory sslSocketFactory = (null != httpHelper) ? httpHelper.getSSLSocketFactory() : null;

             if ((httpURLConnection instanceof HttpsURLConnection) && (null != sslSocketFactory)) {
                 try {
                     ((HttpsURLConnection) httpURLConnection).setSSLSocketFactory(sslSocketFactory);
                 } catch (@SuppressWarnings("unused") Exception e) {
                     logWarn("DC is not able to establish Http(s) connection with URL: " + url);
                 }
             }

             if (null != extensionWrapper) {
                 extensionWrapper.set(StringUtils.EMPTY);

                 final int extPos = url.lastIndexOf('.');

                 if ((extPos > -1) && (extPos < (url.length() - 2))) {
                     extensionWrapper.set(url.substring(extPos + 1).toLowerCase());
                 }
             }

             try {
                 httpURLConnection.connect();

                 try (InputStream httpStm = httpURLConnection.getInputStream()) {
                     if (null != httpStm) {
                         FileUtils.copyInputStreamToFile(httpStm, ret = targetFile);

                         if (ServerManager.isLogWarn() && (!ret.canRead() || (ret.length() <= 0))) {
                             ServerManager.logWarn("DC not able to write HTTP source file to local temp. file: " + url + " => " + ret.getCanonicalPath());

                             FileUtils.deleteQuietly(ret);
                             ret = null;
                         } else {
                             ret.setLastModified(System.currentTimeMillis());

                             synchronized (URL_RESOLVED_FILE_LIST) {
                                 URL_RESOLVED_FILE_LIST.add(ret);
                             }
                        }
                     } else if (ServerManager.isLogWarn()) {
                         ServerManager.logWarn("DC temp. file to store downloaded HTTP source file could not be created: " + url);
                     }
                 }
             } catch (final Exception e) {
                 ServerManager.logExcp(e);
                 FileUtils.deleteQuietly(ret);
                 ret = null;
             } finally {
                 httpURLConnection.disconnect();
             }
        }

        return ret;
    }

    /**
     * @return
     */
    public int getAsyncJobCountScheduled() {
        return m_asyncExecutor.getScheduledCount();
    }

    /**
     * @return
     */
    public long getAsyncJobCountProcessed() {
        return m_asyncExecutor.getProcessedCount();
    }

    /**
     * @return
     */
    public long getAsyncJobCountDropped() {
        return m_asyncExecutor.getDroppedCount();
    }

    // - Implementation --------------------------------------------------------

    /**
     * @param patternList
     * @param file
     */
    private static void implFillPatternList(@NonNull final List<Pattern> patternList, File file) {
        if ((null != file) && file.canRead()) {
            String readLine = null;

            try (final BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
                while (null != (readLine = reader.readLine())) {
                    if (StringUtils.isNotBlank(readLine = readLine.trim()) && !readLine.startsWith("#")) {
                        try {
                            patternList.add(Pattern.compile(readLine));
                        } catch (@SuppressWarnings("unused") PatternSyntaxException e) {
                            logError("DC could not compile provided RegExp pattern:" + readLine + " (file: " + file.getAbsolutePath() + ")");
                        }
                    }
                }
            } catch (@SuppressWarnings("unused") Exception e) {
                logError("DC could not read RegExp pattern file: " + file.getAbsolutePath());
            }
        }
    }

    /**
     * @param patternList
     * @param testString
     * @return
     */
    private static boolean implIsMatchingURLPattern(@NonNull final List<Pattern> patternList, @NonNull final String testString) {
        for (final Pattern curPattern : patternList) {
            if (curPattern.matcher(testString).matches()) {
                return true;
            }
        }

        return false;
    }

    /**
     * HTTP_GET_CONNECTION_DECORATOR
     */
    private static Function<URLConnection, Optional<OXException>> HTTP_GET_CONNECTION_DECORATOR = (urlConnection) -> {
        Optional<OXException> ret = Optional.empty();

        if (urlConnection instanceof HttpURLConnection) {
            final HttpURLConnection httpConnection = (HttpURLConnection) urlConnection;

            try {
                httpConnection.setRequestMethod("GET");
                httpConnection.setConnectTimeout(ServerManager.getHTTPConnectTimeoutMillis(ServerType.DOCUMENTCONVERTER));
                httpConnection.setReadTimeout(ServerManager.getHTTPReadTimeoutMillis(ServerType.DOCUMENTCONVERTER));
            } catch (Exception e) {
                ret = Optional.of(DataExceptionCodes.ERROR.create(e));
            }
        } else {
            ret = Optional.of(DataExceptionCodes.INVALID_ARGUMENT.create("No valid HTTP(s) connection"));
        }

        return ret;
    };

    // - Static members --------------------------------------------------------

    private static ServerManager DC_MANAGER = null;

    final private static Map<JobError, Integer> DEFAULT_ERROR_RUNCOUNT_MAP = new HashMap<>();

    final private static List<Pattern> URL_BLACKLIST_PATTERNS = new ArrayList<>();

    final private static List<Pattern> URL_WHITELIST_PATTERNS = new ArrayList<>();

    final private static Function<URL, Optional<OXException>> URL_VALIDATOR_FUNC = (hopURL) -> {
        final String hopURLString = hopURL.toString().trim();
        Optional<OXException> ret = Optional.empty();

        if ((StringUtils.isBlank(hopURLString)) ||
            ((true == implIsMatchingURLPattern(URL_BLACKLIST_PATTERNS, hopURLString)) &&
             (false == implIsMatchingURLPattern(URL_WHITELIST_PATTERNS, hopURLString)))) {

            ret = Optional.of(DataExceptionCodes.INVALID_ARGUMENT.create("url", hopURLString));
        }

        return ret;
    };

    final private static List<File> URL_RESOLVED_FILE_LIST = new LinkedList<>();

    // set cleanup timer to 5 minutes
    final private static long URL_RESOLVED_FILE_CLEANUP_TIMEOUT_MILLIS = 5L * 60L * 1000L;

    // - Static init. ----------------------------------------------------------

    static {
        for (final JobError curJobError : JobError.values()) {
            DEFAULT_ERROR_RUNCOUNT_MAP.put(curJobError, Integer.valueOf(1));
        }
    }

    // - members ---------------------------------------------------------------

    final private ScheduledExecutorService m_timerExecutor = Executors.newScheduledThreadPool(1);

    private ServerConfig m_serverConfig;

    private AsyncExecutor m_asyncExecutor;

    private PDFExtractor m_pdfExtractor;

    private PageConverter m_pageConverter;

    private AtomicBoolean m_isRunning = new AtomicBoolean(true);

    private JobProcessor m_jobProcessor = null;

    private IQueueMonitor m_queueMonitor = null;

    private LTStatistics m_statistics = null;

    private DocumentConverterInformation m_dcMBean = null;

    private Cache m_cache = null;

    private File m_engineScratchDir = null;

    private List<String> m_serverErrors = new ArrayList<>();
}
