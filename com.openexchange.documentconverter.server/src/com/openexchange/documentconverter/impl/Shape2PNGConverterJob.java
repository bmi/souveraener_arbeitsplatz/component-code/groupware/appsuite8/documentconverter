/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONException;
import org.json.JSONObject;
import com.google.common.base.Throwables;
import com.openexchange.documentconverter.JobError;
import com.openexchange.documentconverter.JobErrorEx;
import com.openexchange.documentconverter.Properties;
import com.openexchange.documentconverter.impl.api.BackendType;
import com.openexchange.documentconverter.impl.api.ConverterStatus;
import com.openexchange.documentconverter.impl.api.ICacheEntry;
import com.openexchange.exception.ExceptionUtils;
import com.sun.star.beans.PropertyValue;
import com.sun.star.container.XIndexAccess;
import com.sun.star.drawing.XDrawPage;
import com.sun.star.drawing.XDrawPageSupplier;
import com.sun.star.drawing.XDrawPages;
import com.sun.star.drawing.XDrawPagesSupplier;
import com.sun.star.drawing.XGraphicExportFilter;
import com.sun.star.drawing.XShape;
import com.sun.star.drawing.XShapes;
import com.sun.star.lang.XComponent;
import com.sun.star.lang.XMultiComponentFactory;
import com.sun.star.lang.XServiceInfo;
import com.sun.star.sheet.XSpreadsheetDocument;
import com.sun.star.uno.Exception;
import com.sun.star.uno.UnoRuntime;

//-------------------------------
//- class Shape2PNGConverterJob -
//-------------------------------

class Shape2PNGConverterJob extends ConverterJob {

    /**
     * Initializes a new {@link Shape2PNGConverterJob}.
     * @param jobProperties
     */
    Shape2PNGConverterJob(HashMap<String, Object> jobProperties, HashMap<String, Object> resultProperties) {
        super(jobProperties, resultProperties);
        applyJobProperties();
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.IJob#needsInstance()
     */
    @Override
    public BackendType backendTypeNeeded() {
        return BackendType.READERENGINE;
    }

    /* (non-Javadoc)
     * @see com.Jopenexchange.documentconverter.impl.ConverterJob#doExecute(com.openexchange.documentconverter.IInstance)
     */
    @Override
    protected ConverterStatus doExecute(Object jobExecutionData) throws com.sun.star.lang.DisposedException, Exception {

        ConverterStatus converterStatus = ConverterStatus.ERROR;

        final REInstance instance = (jobExecutionData instanceof REInstance) ? (REInstance) jobExecutionData : null;

        XComponent component = null;
        if(m_shapeNumber > 0) {
            // if shape number is given then this job is saving the replacement graphic into the output file
            component = super.loadComponent(instance, m_pageRange);
            m_resultJobErrorEx = getShape(instance, component, m_outputFile, m_pageNumber, m_shapeNumber);

        } else {
            // this job tries to get the list of replacements from the input document and is adding each replacement into the cache
            converterStatus = ConverterStatus.OK;
            m_resultJobErrorEx = new JobErrorEx(JobError.NONE);
            final Cache cache = ServerManager.get().getCache();
            final Map<String, String> replacements = implGetReplacements(cache);
            if(replacements != null && !replacements.isEmpty()) {
                component = super.loadComponent(instance, m_pageRange);
                cacheReplacements(instance, component, cache, replacements);
            }
        }
        if(component != null) {
            try {
                closeComponent(component);
                converterStatus = ConverterStatus.OK;
            } catch (Throwable e) {
                ExceptionUtils.handleThrowable(e);

                if (m_resultJobErrorEx.hasNoError()) {
                    converterStatus = ConverterStatus.OK_WITH_ENGINE_ERROR;
                } else {
                    throw e;
                }
            }
        }
        return converterStatus;
    }

    private static JobErrorEx getShape(REInstance instance, XComponent component, File outputFile, int pageNumber, int shapeNumber) throws Exception {
        XDrawPage drawPage = null;
        if (null != component) {
            final XServiceInfo info = UnoRuntime.queryInterface(XServiceInfo.class, component);

            if (null != info) {
                if (info.supportsService("com.sun.star.text.TextDocument")) {
                    final XDrawPageSupplier drawPageSupplier = UnoRuntime.queryInterface(XDrawPageSupplier.class, component);
                    if(drawPageSupplier != null) {
                        drawPage = drawPageSupplier.getDrawPage();
                    }
                } else if (pageNumber > 0 && info.supportsService("com.sun.star.sheet.SpreadsheetDocument")) {
                    final XSpreadsheetDocument spreadsheetDocument = UnoRuntime.queryInterface(XSpreadsheetDocument.class, component);
                    if(spreadsheetDocument != null) {
                        final XIndexAccess sheets = UnoRuntime.queryInterface(XIndexAccess.class, spreadsheetDocument.getSheets());
                        if(sheets != null) {
                            final XDrawPageSupplier drawPageSupplier = UnoRuntime.queryInterface(XDrawPageSupplier.class, sheets.getByIndex(pageNumber - 1));
                            if(drawPageSupplier != null) {
                                drawPage = drawPageSupplier.getDrawPage();
                            }
                        }
                    }
                } else if (pageNumber > 0 && info.supportsService("com.sun.star.presentation.PresentationDocument") || info.supportsService("com.sun.star.drawing.DrawingDocument")) {
                    final XDrawPagesSupplier drawPagesSupplier = UnoRuntime.queryInterface(XDrawPagesSupplier.class, component);
                    if(drawPagesSupplier != null) {
                        final XDrawPages drawPages = drawPagesSupplier.getDrawPages();
                        if(drawPages != null) {
                            drawPage = UnoRuntime.queryInterface(XDrawPage.class, drawPages.getByIndex(pageNumber - 1));
                        }
                    }
                }
            }
        }

        if(drawPage != null) {
            XMultiComponentFactory componentFactory = null;
            if ((null != instance) && (null != (componentFactory = instance.getComponentFactory()))) {
                final XGraphicExportFilter exportFilter = UnoRuntime.queryInterface(XGraphicExportFilter.class, componentFactory.createInstanceWithContext(
                    "com.sun.star.drawing.GraphicExportFilter", instance.getDefaultContext()));
                if(exportFilter!=null) {
                    final XShapes shapes = UnoRuntime.queryInterface(XShapes.class, drawPage);
                    if(shapes != null) {
                        final XShape shape = UnoRuntime.queryInterface(XShape.class, shapes.getByIndex(shapeNumber - 1));
                        if(shape != null) {
                            final XComponent shapeComponent = UnoRuntime.queryInterface(XComponent.class, shape);
                            if(shapeComponent != null) {
                                exportFilter.setSourceDocument(shapeComponent);

                                final PropertyValue[] filterData = new PropertyValue[1];

                                // TODO: fixed resolution needed ? m_pixelHeight, m_pixelWidth
                                filterData[0] = new PropertyValue();
                                filterData[0].Name = "ImageResolution";
                                filterData[0].Value = Integer.valueOf(300);

                                // set store properties
                                final PropertyValue outputArgs[] = new PropertyValue[4];

                                outputArgs[0] = new PropertyValue();
                                outputArgs[0].Name = "FilterName";
                                outputArgs[0].Value = "PNG";

                                outputArgs[1] = new PropertyValue();
                                outputArgs[1].Name = "URL";
                                outputArgs[1].Value = ServerManager.getAdjustedFileURL(outputFile);

                                outputArgs[2] = new PropertyValue();
                                outputArgs[2].Name = "Overwrite";
                                outputArgs[2].Value = Boolean.TRUE;

                                outputArgs[3] = new PropertyValue();
                                outputArgs[3].Name = "FilterData";
                                outputArgs[3].Value = filterData;

                                exportFilter.filter(outputArgs);
                            }
                        }
                    }
                }
            }
        }
        return new JobErrorEx((outputFile.length() > 0) ? JobError.NONE : JobError.GENERAL);
    }

    private void cacheReplacements(REInstance instance, XComponent component, Cache cache, Map<String, String> replacements) throws Exception {
        if(cache != null) {
            for(Entry<String, String> entry:replacements.entrySet()) {

                final String hash = entry.getKey();
                final String resourceName = entry.getValue();

                final String[] params = resourceName.split("\\.", -1);
                try {
                    final int l = params.length;
                    if(params[l - 5].startsWith("h") && params[l - 4].startsWith("p") && params[l - 3].startsWith("s") && params[l - 2].startsWith("i")) {

                        final int pageIndex = Integer.parseInt(params[l - 4].substring(1));
                        final int shapeIndex = Integer.parseInt(params[l - 3].substring(1));

                        final ICacheEntry newCacheEntry = cache.startNewEntry(hash);
                        if(newCacheEntry != null) {
                            try {
                                final JobErrorEx jobError = getShape(instance, component, newCacheEntry.getResultFile(), pageIndex + 1, shapeIndex + 1);
                                if(jobError.hasNoError()) {
                                    if(!cache.addEntry(newCacheEntry, true, null)) {
                                        ServerManager.logTrace("DC server, shape2png, cache entry could not been added.");
                                    }
                                }
                            }
                            finally {
                                cache.endNewEntry(newCacheEntry);
                            }
                        }
                    }
                }
                catch(RuntimeException e) {
                    ServerManager.logTrace("DC server, shape2png, wrong syntax in replacement map: " +
                        Throwables.getRootCause(e).getMessage());
                }
            }
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.ConverterJob#applyJobProperties(java.util.HashMap)
     */
    @Override
    protected void applyJobProperties() {
        super.applyJobProperties();

        // shape replacements
        Object shapeReplacementsObj = m_jobProperties.get(Properties.PROP_SHAPE_REPLACEMENTS);
        if (shapeReplacementsObj instanceof String) {
            m_shapeReplacements = (String) shapeReplacementsObj;
        }

        // pixel width and height
        Object extentObj = m_jobProperties.get(Properties.PROP_PAGE_NUMBER);
        if (extentObj instanceof Integer) {
            m_pageNumber = ((Integer)extentObj).intValue();
        }

        extentObj = m_jobProperties.get(Properties.PROP_SHAPE_NUMBER);
        if (extentObj instanceof Integer) {
            m_shapeNumber = ((Integer)extentObj).intValue();
        }
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.documentconverter.impl.ConverterJob#getMeasurementType()
     */
    @Override
    public ConverterMeasurement getMeasurementType() {
        return ConverterMeasurement.CONVERT_DOC_TO_GRAPHIC;
    }

    /**
     * implGetReplacements, retrieving map of replacements (which are not cached already) 
     *
     * @return
     */
    private Map<String, String> implGetReplacements(Cache cache) {
        if (null != m_shapeReplacements) {
            try {
                final Map<String, String> replacementMap = new HashMap<>();
                final Iterator<Entry<String, Object>> iter = (new JSONObject(m_shapeReplacements)).stream().iterator();

                while (iter.hasNext()) {
                    final Entry<String, Object> curEntry = iter.next();
                    if(cache.getEntry(curEntry.getKey(), null) == null) {
                        replacementMap.put(curEntry.getKey(), curEntry.getValue().toString());
                    }
                }

                return replacementMap;
            } catch (JSONException e) {
                ServerManager.logExcp(e);
            }
        }

        return null;
    }

    // - Members ---------------------------------------------------------------

    private String m_shapeReplacements;

    // represents the page number (starting at 1) of presentation or the sheet number of spreadsheet documents, for text documents this member is 0
    private int m_pageNumber;

    // shape number (starting at 1) that has to be exported
    private int m_shapeNumber;
}
