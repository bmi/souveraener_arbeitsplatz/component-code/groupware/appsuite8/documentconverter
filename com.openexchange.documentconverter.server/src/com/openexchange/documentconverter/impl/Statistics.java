/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import com.openexchange.documentconverter.JobError;
import com.openexchange.documentconverter.JobErrorEx;
import com.openexchange.documentconverter.JobPriority;
import com.openexchange.documentconverter.NonNull;
import com.openexchange.documentconverter.Nullable;
import com.openexchange.documentconverter.impl.api.BackendType;
import com.openexchange.documentconverter.impl.api.IJobStatusListener;
import com.openexchange.documentconverter.impl.api.IQueueMonitor;
import com.openexchange.documentconverter.impl.api.JobEntry;
import com.openexchange.documentconverter.impl.api.JobStatus;

/**
 * {@link Statistics}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
public class Statistics implements IJobStatusListener {

    private final static String DC_STATISTICS_TIMER_NAME = "DC Statistics timer";

    // set period of reset task to 5min, which equals to the munin standard period
    private final long RESET_TIMER_PERIOD_MILLISECONDS = 5 * 60 * 1000;

    // set ahead time to periodically retrieve results this time
    // span before the next external and periodic retrieval
    private final long RESET_TIMER_AHEAD_TIME = 30 * 1000;

    /**
     * Initializes a new {@link Statistics}.
     */
    public Statistics(@NonNull ServerManager serverManager, @Nullable final JobProcessor processor, @Nullable final Cache cache) {
        super();

        m_serverManager = serverManager;
        m_jobProcessor = processor;
        m_queueMonitor = (null != processor) ? processor.getQueueMonitor() : null;
        m_cache = cache;

        // register ourselfs as listener for any job status changes
        if (null != m_jobProcessor) {
            m_jobProcessor.addJobStatusChangeListener(this);
        }

        // initialize all periodic members
        for (final JobPriority jobPriority : JobPriority.values()) {
            m_curQueueCounts.put(jobPriority, new AtomicInteger());
            m_peakQueueCounts.put(jobPriority, new AtomicInteger());
            m_peakQueueCountsResult.put(jobPriority, new AtomicInteger());

            m_queueTimes.put(jobPriority, new ArrayList<AtomicLong>());
            m_queueTimesResult.put(jobPriority, new AtomicLong());

            m_conversionTimes.put(jobPriority, new ArrayList<AtomicLong>());
            m_conversionTimesResult.put(jobPriority, new AtomicLong());

            m_jobTimes.put(jobPriority, new ArrayList<AtomicLong>());
            m_jobTimesResult.put(jobPriority, new AtomicLong());
        }

        // initialize
        for (final JobError jobError : JobError.values()) {
            m_conversionResult.put(jobError, new AtomicLong());
        }

        for (final BackendType backendType : BackendType.values()) {
            m_conversionCounts.put(backendType, new AtomicLong());
        }
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.documentconverter.IJobStatusListener#statusChanged(java .lang.String)
     */
    @Override
    public synchronized void statusChanged(String jobId, Object data) {
        if (data instanceof JobEntry) {
            final JobEntry changedJobEntry = (JobEntry) data;
            final JobStatus changedJobStatus = changedJobEntry.getJobStatus();
            final JobPriority changedJobPriority = changedJobEntry.getJobPriority();
            int curQueueCount = 0;

            if (JobStatus.SCHEDULED == changedJobStatus) {
                if (!changedJobEntry.isMultipleSchedule()) {
                    // entry has just been added to queue
                    m_curTotalQueueCount.incrementAndGet();
                    curQueueCount = m_curQueueCounts.get(changedJobPriority).incrementAndGet();
                }
            } else if ((JobStatus.FINISHED == changedJobStatus) || (JobStatus.ERROR == changedJobStatus)) {
                final JobErrorEx jobErrorEx = (JobStatus.FINISHED == changedJobStatus) ? new JobErrorEx() : changedJobEntry.getJobErrorEx();

                // increment general counters
                incrementProcessedJobCount(jobErrorEx, changedJobEntry.isCacheHit() || changedJobEntry.isErrorCacheHit());

                // update queue and conversion times
                m_queueTimes.get(changedJobPriority).add(new AtomicLong(changedJobEntry.getQueueTimeMillis()));
                m_conversionTimes.get(changedJobPriority).add(new AtomicLong(changedJobEntry.getConversionTimeMillis()));
                m_jobTimes.get(changedJobPriority).add(new AtomicLong(changedJobEntry.getJobTimeMillis()));
            } else if (JobStatus.REMOVED == changedJobStatus) {
                // entry has just been removed from queue
                if (m_curTotalQueueCount.get() > 0) {
                    m_curTotalQueueCount.decrementAndGet();
                }

                final AtomicInteger curAtomicInteger = m_curQueueCounts.get(changedJobPriority);

                if (curAtomicInteger.get() > 0) {
                    curQueueCount = curAtomicInteger.decrementAndGet();
                }
            }

            // adjust appropriate queue counts
            if (m_curTotalQueueCount.get() > m_peakTotalQueueCount.get()) {
                m_peakTotalQueueCount.set(m_curTotalQueueCount.get());
            }

            final AtomicInteger curPeakQueueCount = m_peakQueueCounts.get(changedJobPriority);

            if (curQueueCount > curPeakQueueCount.get()) {
                curPeakQueueCount.set(curQueueCount);
            }
        }
    }

    /**
     *
     */
    public void terminate() {
        m_resetTimer.cancel();
    }

    /**
     *
     */
    public synchronized void incrementProcessedJobCount(JobErrorEx jobErrorEx, boolean incrementCacheCounts) {
        m_jobsProcessed.incrementAndGet();

        if (jobErrorEx.hasNoError()) {
            if (incrementCacheCounts) {
                m_cacheHits.incrementAndGet();
            }
        } else {
            m_jobErrorsTotal.incrementAndGet();

            if (incrementCacheCounts) {
                m_errorCacheHits.incrementAndGet();

                if (JobError.TIMEOUT == jobErrorEx.getJobError()) {
                    m_jobErrorsTimeout.incrementAndGet();
                }
            }
        }
    }

    /**
     * @param job the numberof total conversions as well as
     *  multiple run count is increment, otherwise
     */
    public void incrementConversionCount(@NonNull BackendType backendType) {
        m_conversionCounts.get(backendType).incrementAndGet();
    }

    /**
     * @param job the numberof total conversions as well as
     *  multiple run count is increment, otherwise
     */
    public void incrementConversionWithMultipleRunsCount() {
        m_multipleConversionRunCount.incrementAndGet();
    }

    /**
     * @param job the number of specific conversion error count
     *  is increment
     */
    public void incrementConversionErrorCount(JobError jobError) {
        m_conversionResult.get(jobError).incrementAndGet();
    }

    /**
     * @param jobPriority
     */
    public synchronized void incrementAsyncQueueCount() {
        final int curAsyncQueueCount = m_curAsyncQueueCount.incrementAndGet();

        if (curAsyncQueueCount > m_peakAsyncQueueCount.get()) {
            m_peakAsyncQueueCount.set(curAsyncQueueCount);
        }
    }

    /**
     * @param jobPriority
     */
    public synchronized void decrementAsyncQueueCount() {
        if (m_curAsyncQueueCount.get() > 0) {
            m_curAsyncQueueCount.decrementAndGet();
        }
    }

    /**
     * incrementStatefulJobCount
     *
     */
    public void incrementStatefulJobCount() {
        m_pendingStatefulJobCount.incrementAndGet();
    }

    /**
     * incrementStatefulJobCount
     *
     */
    public void decrementStatefulJobCount() {
        m_pendingStatefulJobCount.decrementAndGet();
    }

    /**
     * @return The current number of processed jobs
     */
    public long getJobsProcessed() {
        return m_jobsProcessed.get();
    }

    /**
     * @return
     */
    public long getConversionCount(@Nullable BackendType backendType) {
        return (null == backendType) ?
            (m_conversionCounts.get(BackendType.READERENGINE).get() + m_conversionCounts.get(BackendType.PDFTOOL).get()) :
                m_conversionCounts.get(backendType).get();
    }

    /**
     * @return
     */
    public long getConversionCountWithMultipleRuns() {
        return m_multipleConversionRunCount.get();
    }

    /**
     * @return
     */
    public long getConversionErrorsGeneral() {
        return m_conversionResult.get(JobError.GENERAL).get();
    }

    /**
     * @return
     */
    public long getConversionErrorsTimeout() {
        return m_conversionResult.get(JobError.TIMEOUT).get();
    }

    /**
     * @return
     */
    public long getConversionErrorsDisposed() {
        return m_conversionResult.get(JobError.DISPOSED).get();
    }

    /**
     * @return
     */
    public long getConversionErrorsPassword() {
        return m_conversionResult.get(JobError.PASSWORD).get();
    }

    /**
     * @return
     */
    public long getConversionErrorsNoContent() {
        return m_conversionResult.get(JobError.NO_CONTENT).get();
    }

    /**
     * @return
     */
    public long getConversionErrorsOutOfMemory() {
        return m_conversionResult.get(JobError.OUT_OF_MEMORY).get();
    }

    /**
     * @return
     */
    public long getConversionErrorsPDFTool() {
        return m_conversionResult.get(JobError.PDFTOOL).get();
    }

    /**
     * @return
     */
    public long getCacheHits() {
        return ((0 < m_jobsProcessed.get()) ? m_cacheHits.get() : 0);
    }

    /**
     * @return
     */
    public long getLocalCacheEntryCount() {
        return (null != m_cache) ? m_cache.getLocalEntryCount() : 0;
    }

    /**
     * @return
     */
    public long getRemoteCacheEntryCount() {
        return (null != m_cache) ? m_cache.getRemoteEntryCount() : 0;
    }

    /**
     * @return
     */
    public long getLocalCachePersistentSize() {
        return (null != m_cache) ? m_cache.getLocalPersistentSize() : 0;
    }

    /**
     * @return
     */
    public long getRemoteCachePersistentSize() {
        return (null != m_cache) ? m_cache.getRemotePersistentSize() : 0;
    }

    /**
     * @return
     */
    public long getLocalCacheFreeVolumeSize() {
        return (null != m_cache) ? m_cache.getLocalFreeVolumeSize() : 0;
    }

    /**
     * @return
     */
    public long getLocalCacheOldestEntrySeconds() {
        return (null != m_cache) ? m_cache.getLocalOldestEntrySeconds() : 0;
    }

    /**
     * @return
     */
    public long getRemoteCacheOldestEntrySeconds() {
        return (null != m_cache) ? m_cache.getRemoteOldestEntrySeconds() : 0;
    }

    /**
     * @return
     */
    public double getCacheHitRatio() {
        final long jobsProcessed = m_jobsProcessed.get();
        return ((0 < jobsProcessed) ? (m_cacheHits.get() / (double) jobsProcessed) : 1.0);
    }

    /**
     * @return
     */
    public long getErrorCacheHits() {
        return ((0 < m_jobsProcessed.get()) ? m_errorCacheHits.get() : 0);
    }

    /**
     * @return
     */
    public double getErrorCacheHitRatio() {
        final long jobErrorsTotal = m_jobErrorsTotal.get();
        return ((0 < jobErrorsTotal) ? (m_errorCacheHits.get() / (double) jobErrorsTotal) : 0.0);
    }

    /**
     * @return
     */
    public long getJobErrorsTotal() {
        return m_jobErrorsTotal.get();
    }

    /**
     * @return
     */
    public long getJobErrorsTimeout() {
        return m_jobErrorsTimeout.get();
    }

    /**
     * @return
     */
    public int getScheduledJobCountInQueue() {
        return (null != m_queueMonitor) ?
            m_queueMonitor.getScheduledCount() :
                -1;
    }

    /**
     * @return The current number of jobs in queue
     */
    public synchronized int getPeakJobCountInQueue(JobPriority jobPriority) {
        int ret = 0;

        initPeriodicResetTimer();

        if (null == jobPriority) {
            ret = m_peakTotalQueueCountResult.get();
        } else {
            ret = m_peakQueueCountsResult.get(jobPriority).get();
        }

        return ret;
    }

    /**
     * @return
     */
    public int getAsyncJobCountScheduled() {
        return m_serverManager.getAsyncJobCountScheduled();
    }

    /**
     * @return
     */
    public long getAsyncJobCountProcessed() {
        return m_serverManager.getAsyncJobCountProcessed();
    }

    /**
     * @return
     */
    public long getAsyncJobCountDropped() {
        return m_serverManager.getAsyncJobCountDropped();
    }

    /**
     * @return The current number of jobs in queue
     */
    public synchronized int getPeakAsyncJobCount() {
        initPeriodicResetTimer();

        return m_peakAsyncQueueCountResult.get();
    }

    /**
     * @param jobPriority
     * @return
     */
    public synchronized long getMedianQueueTimeMillis(JobPriority jobPriority) {
        initPeriodicResetTimer();

        return (null == jobPriority) ?
            m_queueTimesTotalResult.get() :
                m_queueTimesResult.get(jobPriority).get();
    }

    /**
     * @param jobPriority
     * @return
     */
    public synchronized long getMedianConversionTimeMillis(JobPriority jobPriority) {
        initPeriodicResetTimer();

        return (null == jobPriority) ?
            m_conversionTimesTotalResult.get() :
                m_conversionTimesResult.get(jobPriority).get();
    }

    /**
     * @param jobPriority
     * @return
     */
    public synchronized long getMedianJobTimeMillis(JobPriority jobPriority) {
        initPeriodicResetTimer();

        return (null == jobPriority) ?
            m_jobTimesTotalResult.get() :
                m_jobTimesResult.get(jobPriority).get();
    }

    /**
     * getPendingStatefulJobCount
     *
     * @return
     */
    public long getPendingStatefulJobCount() {
        return m_pendingStatefulJobCount.get();
    }

    /**
     *
     */
    protected void initPeriodicResetTimer() {
        if (null == m_resetTask) {
            // the first values are retrieved => call reset period to retrieve
            // latest results
            resetMeasurementPeriod();

            // the periodically called reset task
            m_resetTask = new TimerTask() {

                /*
                 * (non-Javadoc)
                 * @see com.openexchange.documentconverter.impl.Statistics.ResetTask #run()
                 */
                @Override
                public void run() {
                    resetMeasurementPeriod();
                }
            };

            // start the timer to periodically retrieve further result sets
            // a short amount of time before the next results will be requested
            // with a fixed period from external tools, e.g. 300s with munin
            m_resetTimer.scheduleAtFixedRate(
                m_resetTask,
                RESET_TIMER_PERIOD_MILLISECONDS - RESET_TIMER_AHEAD_TIME,
                RESET_TIMER_PERIOD_MILLISECONDS);
        }
    }

    /**
     *
     */
    protected synchronized void resetMeasurementPeriod() {
        // standard queue
        m_peakTotalQueueCountResult.set(m_peakTotalQueueCount.get());
        m_peakTotalQueueCount.set(m_curTotalQueueCount.get());

        // async queue
        m_peakAsyncQueueCountResult.set(m_peakAsyncQueueCount.get());
        m_peakAsyncQueueCount.set(m_curAsyncQueueCount.get());

        // median total times
        m_queueTimesTotalResult.set(getMedianTimeMillis(m_queueTimes, null));
        m_conversionTimesTotalResult.set(getMedianTimeMillis(m_conversionTimes, null));
        m_jobTimesTotalResult.set(getMedianTimeMillis(m_jobTimes, null));

        for (final JobPriority jobPriority : JobPriority.values()) {
            // save PeakJobCount results and reset working map
            m_peakQueueCountsResult.get(jobPriority).set(m_peakQueueCounts.get(jobPriority).get());
            m_peakQueueCounts.get(jobPriority).set(m_curQueueCounts.get(jobPriority).get());

            // save QueueTimes results and reset working Map
            m_queueTimesResult.put(jobPriority, new AtomicLong(getMedianTimeMillis(m_queueTimes, jobPriority)));
            m_queueTimes.get(jobPriority).clear();

            // save ExecutionTimes results and reset working Map
            m_conversionTimesResult.put(jobPriority, new AtomicLong(getMedianTimeMillis(m_conversionTimes, jobPriority)));
            m_conversionTimes.get(jobPriority).clear();

            // save JobTimes results and reset working Map
            m_jobTimesResult.put(jobPriority, new AtomicLong(getMedianTimeMillis(m_jobTimes, jobPriority)));
            m_jobTimes.get(jobPriority).clear();
        }
    }

    /**
     * @param timesMap
     * @param jobPriority
     * @return
     */
    static protected long getMedianTimeMillis(Map<JobPriority, ArrayList<AtomicLong>> timesMap, JobPriority jobPriority) {
        double ret = 0.0;
        ArrayList<AtomicLong> workList = null;

        if (null == jobPriority) {
            workList = new ArrayList<>();

            for (final JobPriority curPriority : JobPriority.values()) {
                workList.addAll(timesMap.get(curPriority));
            }
        } else {
            workList = timesMap.get(jobPriority);
        }

        if (null != workList) {
            final long[] longArray = new long[workList.size()];

            if (longArray.length > 0) {
                final int medianIndex = longArray.length >> 1;
                int i = 0;

                for (final AtomicLong curEntry : workList) {
                    longArray[i++] = curEntry.get();
                }

                Arrays.sort(longArray);

                // calculate average of left and right values wrt. the median
                // position for even size lists
                ret = (0 == (longArray.length % 2)) ? ((longArray[medianIndex - 1] + longArray[medianIndex]) * 0.5) : longArray[medianIndex];
            }
        }

        return Math.round(ret);
    }

    // - Members

    /**
     * The used ServerManager
     */
    final protected ServerManager m_serverManager;

    /**
     * The used JobProcessor (might be null)
     */
    final protected JobProcessor m_jobProcessor;

    /**
     * The used queue monitor
     */
    final protected IQueueMonitor m_queueMonitor;

    /**
     * The used Cache
     */
    final protected Cache m_cache;

    final private Timer m_resetTimer = new Timer(DC_STATISTICS_TIMER_NAME, true);

    /**
     * The number of jobs that have been processed
     */
    final private AtomicLong m_jobsProcessed = new AtomicLong();

    /**
     * The number of cache hits
     */
    final private AtomicLong m_cacheHits = new AtomicLong();

    /**
     * The number of errorcache hits
     */
    final private AtomicLong m_errorCacheHits = new AtomicLong();

    /**
     * The number of jobs that returned with an error in general
     */
    final private AtomicLong m_jobErrorsTotal = new AtomicLong();

    /**
     * The number of jobs that returned with an error in general
     */
    final private AtomicLong m_jobErrorsTimeout = new AtomicLong();

    /**
     * The total number of jobs that are in the queue
     */
    final private AtomicInteger m_curTotalQueueCount = new AtomicInteger();

    final private AtomicInteger m_peakTotalQueueCount = new AtomicInteger();

    final private AtomicInteger m_peakTotalQueueCountResult = new AtomicInteger();


    /**
     * The total number of jobs that are in the queue
     */
    final private AtomicInteger m_curAsyncQueueCount = new AtomicInteger();

    final private AtomicInteger m_peakAsyncQueueCount = new AtomicInteger();

    final private AtomicInteger m_peakAsyncQueueCountResult = new AtomicInteger();

    /**
     * The number of jobs that are in the queue with the given priority
     */
    final private Map<JobPriority, AtomicInteger> m_curQueueCounts = new EnumMap<>(JobPriority.class);

    final private Map<JobPriority, AtomicInteger> m_peakQueueCounts = new EnumMap<>(JobPriority.class);

    final private Map<JobPriority, AtomicInteger> m_peakQueueCountsResult = new EnumMap<>(JobPriority.class);

    /**
     * The list of queue times of jobs that were in the queue with the given priority during the last period
     */
    final private Map<JobPriority, ArrayList<AtomicLong>> m_queueTimes = new EnumMap<>(JobPriority.class);

    final private Map<JobPriority, AtomicLong> m_queueTimesResult = new EnumMap<>(JobPriority.class);

    final private AtomicLong m_queueTimesTotalResult = new AtomicLong();


    /**
     * The list of conversion times of jobs that were in the queue with the given priority during the last period
     */
    final private Map<JobPriority, ArrayList<AtomicLong>> m_conversionTimes = new EnumMap<>(JobPriority.class);

    final private Map<JobPriority, AtomicLong> m_conversionTimesResult = new EnumMap<>(JobPriority.class);

    final private AtomicLong m_conversionTimesTotalResult = new AtomicLong();

    /**
     * The list of conversion times of jobs that were in the queue with the given priority during the last period
     */
    final private Map<JobPriority, ArrayList<AtomicLong>> m_jobTimes = new EnumMap<>(JobPriority.class);

    final private Map<JobPriority, AtomicLong> m_jobTimesResult = new EnumMap<>(JobPriority.class);

    final private AtomicLong m_jobTimesTotalResult = new AtomicLong();


    final private Map<JobError, AtomicLong> m_conversionResult = new EnumMap<>(JobError.class);

    final private AtomicLong m_multipleConversionRunCount = new AtomicLong();

    /**
     * The number of started conversions for each BackendType
     */
    final private Map<BackendType, AtomicLong> m_conversionCounts = new EnumMap<>(BackendType.class);

    final private AtomicInteger m_pendingStatefulJobCount = new AtomicInteger();

    private TimerTask m_resetTask = null;
}
