/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl;

import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import com.openexchange.documentconverter.JobError;
import com.openexchange.documentconverter.JobErrorEx;
import com.openexchange.documentconverter.NonNull;

/**
 * {@link Watchdog}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
final public class Watchdog extends Thread {

    private final long DEFAULT_CHECK_TIMEOUT_MILLIS = 1000;

    /**
     * {@link WatchdogMode}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v7.10.3
     */
    public enum WatchdogMode {
        TIMEOUT,
        TIMEOUT_AND_DISPOSED
    }

    /**
     * {@link WatchdogEntry}
     *
     * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
     * @since v7.10.3
     */
    protected static class WatchdogEntry {

        /**
         * Initializes a new {@link WatchdogEntry}.
         * @param watchdogHandler
         */
        WatchdogEntry(@NonNull final WatchdogHandler watchdogHandler) {
            m_startTimeMillis = System.currentTimeMillis();
            m_watchdogHandler = watchdogHandler;
        }

        /**
         * @return
         */
        public WatchdogHandler getWatchdogHandler() {
            return m_watchdogHandler;
        }

        /**
         * @return
         */
        public long getStartTimeMillis() {
            return m_startTimeMillis;
        }

        /**
         *
         */
        public void resetStartTimeMillis() {
            m_startTimeMillis = System.currentTimeMillis();
        }

        // - Members -----------------------------------------------------------

        final private WatchdogHandler m_watchdogHandler;

        private long m_startTimeMillis = 0;
    }

    /**
     * Initializes a new {@link Watchdog}.Watchdog
     * @param name
     * @param timeoutMilliSeconds
     * @param watchForDisposed
     */
    public Watchdog(@NonNull String name, @NonNull long timeoutMilliSeconds, WatchdogMode watchdogMode) {
        super(name);

        m_timeoutMillis = timeoutMilliSeconds;
        m_watchdogMode = watchdogMode;
    }

    /**
     * @return The initially set timeout in milliseconds
     */
    public long getTimeoutMilliSeconds() {
        return m_timeoutMillis;
    }

    /**
     * @param watchdogHandler
     * @return The time in milliseconds, the handler was reset or added
     */
    public long addWatchdogHandler(@NonNull WatchdogHandler watchdogHandler) {
        WatchdogEntry affectedEntry = null;

        // don't add handler if we're already terminated
        if (!m_isRunning.get() || (null == watchdogHandler)) {
            return 0;
        }

        synchronized (m_entryQueue) {
            final Iterator<WatchdogEntry> iter = m_entryQueue.iterator();

            while (iter.hasNext()) {
                final WatchdogEntry curEntry = iter.next();

                if (curEntry.getWatchdogHandler() == watchdogHandler) {
                    // remove found entry from current position if
                    // found, reset startTime and put entry to the end
                    iter.remove();

                    (affectedEntry = curEntry).resetStartTimeMillis();
                    m_entryQueue.add(affectedEntry);

                    break;
                }
            }

            if (null == affectedEntry) {
                m_entryQueue.add(affectedEntry = new WatchdogEntry(watchdogHandler));
            }
        }

        return affectedEntry.getStartTimeMillis();
    }

    /**
     * @param watchdogHandler
     */
    public void removeWatchdogHandler(@NonNull WatchdogHandler watchdogHandler) {
        if (null != watchdogHandler) {
            synchronized (m_entryQueue) {
                final Iterator<WatchdogEntry> iter = m_entryQueue.iterator();

                while (iter.hasNext()) {
                    if (iter.next().getWatchdogHandler() == watchdogHandler) {
                        iter.remove();
                        break;
                    }
                }
            }
        }
    }

    /**
     *
     */
    public void terminate() {
        if (m_isRunning.compareAndSet(true, false)) {
            final String threadName = getName();
            final boolean trace = ServerManager.isLogTrace();
            long traceStartTimeMillis = 0;

            if (trace) {
                traceStartTimeMillis = System.currentTimeMillis();
                ServerManager.logTrace(new StringBuilder(256).
                    append("DC starting shutdown ").append(threadName).
                    append("...").toString());

            }

            if (trace) {
                ServerManager.logTrace(new StringBuilder(256).
                    append("DC finished shutdown ").append(threadName).append(": ").
                    append(System.currentTimeMillis() - traceStartTimeMillis).append("ms").
                    append('!').toString());
            }
        }
    }

    /**
     * @return
     */
    boolean isRunning() {
        return m_isRunning.get();
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Thread#run()
     */
    @Override
    public void run() {
        long curTimeMillis = 0;

        if (ServerManager.isLogInfo()) {
            ServerManager.logInfo(getName() + " running");
        }

        while (isRunning()) {
            final boolean timeoutOnly = implIsTimeoutOnly();
            long nextTimeoutMillis = DEFAULT_CHECK_TIMEOUT_MILLIS;

            if (null != m_entryQueue.peek()) {
                curTimeMillis = System.currentTimeMillis();

                synchronized (m_entryQueue) {
                    // check for disposed  and timeout processes in this order
                    final Iterator<WatchdogEntry> iter = m_entryQueue.iterator();

                    while (iter.hasNext()) {
                        final WatchdogEntry curEntry = iter.next();

                        // check for TIMEOUT or DISPOSED status, with
                        // TIMEOUT having  higher priority than DISPOSED
                        final JobErrorEx notificationJobErrorEx = ((curTimeMillis - curEntry.getStartTimeMillis()) >= m_timeoutMillis) ?
                            new JobErrorEx(JobError.TIMEOUT) :
                                (!implIsTimeoutOnly() && curEntry.getWatchdogHandler().isDisposed()) ?
                                    new JobErrorEx(JobError.DISPOSED) :
                                        new JobErrorEx(JobError.NONE);

                        // remove handler from queue and notify handler
                        // in case of found watchdog condition
                        if (notificationJobErrorEx.hasError()) {
                            iter.remove();

                            curEntry.getWatchdogHandler().handleWatchdogNotification(
                                curTimeMillis,
                                curEntry.getStartTimeMillis(),
                                notificationJobErrorEx);
                        } else if (timeoutOnly) {
                            // leave loop if we only check for timeouts (sorted moded)
                            // and the first entry didn't reach its timeout
                            break;
                        }
                    }

                    // calculate next timeout value in case of TIMEOUT only mode
                    // with an entry queue having its entries in sorted order
                    if (timeoutOnly) {
                        final WatchdogEntry oldestSortedEntry = m_entryQueue.peek();

                        nextTimeoutMillis = (null != oldestSortedEntry) ?
                            Math.max(0, m_timeoutMillis - (curTimeMillis - oldestSortedEntry.m_startTimeMillis)) :
                                m_timeoutMillis;
                    }
                }
            }

            // sleep thread for the calculated nextTimeoutMillis time until next test
            ServerManager.sleepThread(nextTimeoutMillis, false);
        }

        // when thread has been terminated, notify
        // all pending entries of a timeout
        synchronized (m_entryQueue) {
            WatchdogEntry curEntry = null;

            curTimeMillis = System.currentTimeMillis();

            while (null != (curEntry = m_entryQueue.poll())) {
                curEntry.getWatchdogHandler().handleWatchdogNotification(
                    curTimeMillis,
                    curEntry.getStartTimeMillis(),
                    new JobErrorEx(JobError.TIMEOUT));
            }
        }

        if (ServerManager.isLogInfo()) {
            ServerManager.logInfo(getName() +  " finished");
        }
    }

    // - Implementation --------------------------------------------------------

    /**
     * implIsSortedMode
     *
     * @return <code>true</code> if it is ensured, that all entries are sorted
     *  in a descending mode, so that iterating over all entries within the watchdog
     *  worker can be stopped when the first entry did not reach its timeout.
     */
    private boolean implIsTimeoutOnly() {
        return (WatchdogMode.TIMEOUT == m_watchdogMode);
    }

    // -------------------------------------------------------------------------

    final private Queue<WatchdogEntry> m_entryQueue = new ConcurrentLinkedQueue<>();

    final private AtomicBoolean m_isRunning = new AtomicBoolean(true);

    final private WatchdogMode m_watchdogMode;

    private long m_timeoutMillis = 0;
}
