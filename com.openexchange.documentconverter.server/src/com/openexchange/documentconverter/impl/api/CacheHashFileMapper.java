/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl.api;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import com.openexchange.documentconverter.NonNull;


/**
 * {@link CacheHashFileMapper}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.5
 */
public class CacheHashFileMapper implements ICacheHashFileMapper {

    final private static char PATH_DELIMITER = File.separatorChar;

    /**
     * Unused
     *
     * Initializes a new {@link CacheHashFileMapper}.
     */
    @SuppressWarnings("unused")
    private CacheHashFileMapper() {
        super();

        // Unused
        m_rootDir = null;
        m_rootDirString = null;
        m_depth = 0;

    }

    /**
     * Initializes a new {@link CacheHashFileMapper}.
     */
    public CacheHashFileMapper(@NonNull File rootDir, int depth) {
        super();

        m_rootDir = rootDir;
        m_depth = Math.min(Math.max(depth, 0), 4);

        String rootDirString = null;

        // obtain root dir as string and remove possible ending path delimiters
        try {
            rootDirString = rootDir.getCanonicalPath();
        } catch (@SuppressWarnings("unused") IOException e) {
            rootDirString = rootDir.getAbsolutePath();
        }

        while ((rootDirString.length() > 0) && (rootDirString.charAt(rootDirString.length() - 1) == PATH_DELIMITER)) {
            rootDirString = rootDirString.substring(0, rootDirString.length() - 1);
        }

        m_rootDirString = rootDirString;
    }

    // - ICacheHashFileMapper --------------------------------------------------

    /**
     *
     */
    @Override
    public File mapHashToFile(@NonNull final String hash) {
        final StringBuilder cacheFileNameBuilder = implAcquireStringBuilder(m_rootDirString);

        try {
            // always treat hash code as unsigned (int) value
            final long hashCode = Integer.toUnsignedLong(hash.hashCode());

            // create entry directory tree by using 1 byte each (LSB to MSB) for every
            // depth level and mapping via Hex string LUT for each possble byte
            for (int curShift = 0, maxShift = (m_depth * 8); curShift < maxShift; curShift += 8) {
                cacheFileNameBuilder.append(PATH_DELIMITER).append(BYTE_TO_HEX_MAP[(int)((hashCode >> curShift) & 0xFF)]);
            }

            return new File(cacheFileNameBuilder.append(PATH_DELIMITER).append(hash).toString());
        } finally {
            implReleaseStringBuilder(cacheFileNameBuilder);
        }
    }

    /**
     *
     */
    @Override
    public File getRootDir() {
        return m_rootDir;
    }

    /**
     *
     */
    @Override
    public int getDepth() {
        return m_depth;
    }

    // - Implementation --------------------------------------------------------

    private StringBuilder implAcquireStringBuilder(@NonNull final String rootDir) {
        StringBuilder ret = null;

        synchronized (STRING_BUILDERS) {
            ret = STRING_BUILDERS.poll();
        }

        return ((null != ret) ? ret : new StringBuilder(512)).append(rootDir);
    }

    // - Implementation --------------------------------------------------------

    private void implReleaseStringBuilder(@NonNull final StringBuilder stringBuilder) {
        stringBuilder.setLength(0);

        synchronized (STRING_BUILDERS) {
            STRING_BUILDERS.add(stringBuilder);
        }
    }

    // - Static members --------------------------------------------------------

    final private static String[] BYTE_TO_HEX_MAP = new String[256];

    final private static LinkedList<StringBuilder> STRING_BUILDERS = new LinkedList<>();

    // static init. of BYTE_TO_HEX_MAP entries and STRING_BUILDERS
    static {
        for (int i = 0, count = BYTE_TO_HEX_MAP.length; i < count; ++i) {
            final String curHexValue = Integer.toHexString(i);

            BYTE_TO_HEX_MAP[i] = (curHexValue.length() < 2) ? ("0" + curHexValue) : curHexValue;
        }
    }

    // - Members ---------------------------------------------------------------

    final private File m_rootDir;

    final private String m_rootDirString;

    final private int m_depth;

}
