/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl.api;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import com.openexchange.documentconverter.NonNull;

/**
 * {@link DataSourceFileInputStream}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.8.2
 */
public class DataSourceFileInputStream extends ObjectInputStream {

    /**
     * @param className
     */
    public static void registerDCDataSourceClassName(@NonNull final String className) {
        DC_DATASOURCE_CLASS_NAMES.add(className);
    }

    /**
     * Initializes a new {@link DataSourceFileInputStream}.
     * @throws IOException
     * @throws SecurityException
     */
    protected DataSourceFileInputStream() throws IOException, SecurityException {
        super();
    }

    /**
     * Initializes a new {@link DataSourceFileInputStream}.
     * @param inputStm
     * @throws IOException
     */
    public DataSourceFileInputStream(InputStream inputStm) throws IOException {
        super(inputStm);
    }

    /* (non-Javadoc)
     * @see java.io.ObjectInputStream#readClassDescriptor()
     */
    @Override
    public ObjectStreamClass readClassDescriptor() throws IOException, ClassNotFoundException {
        final ObjectStreamClass objStmClass = super.readClassDescriptor();
        final String className = (null != objStmClass) ? objStmClass.getName() : null;
        final boolean isInternalClass = (null != className) && className.startsWith("com.openexchange.");

        // try to map unprefixed class name to actually available class (release compat.)
        if ((null != className) && isInternalClass && !DC_DATASOURCE_CLASS_NAMES.contains(className)) {
            final int lastDotPos = className.lastIndexOf('.');
            final String unprefixedClassName = (lastDotPos > -1) && (lastDotPos < className.length()) ?
                className.substring(lastDotPos + 1) :
                    null;

            if (null != unprefixedClassName) {
                synchronized (DC_DATASOURCE_CLASS_NAMES) {
                    for (final String curClassName : DC_DATASOURCE_CLASS_NAMES) {
                        // use class, whose unprefixed class name matches
                        if (curClassName.endsWith(unprefixedClassName)) {
                            return ObjectStreamClass.lookup(Class.forName(curClassName));
                        }
                    }
                }
            }
        }

        if (!isInternalClass && (null != className) && !DC_DATASOURCE_ALLOWED_JRE_CLASSES.contains(className)) {
            throw new IOException("DC detected JRE class that is not allowed to be deserialized: " + className);
        }

        return objStmClass;
    }

    // Static Members ----------------------------------------------------------

    final private static Set<String> DC_DATASOURCE_CLASS_NAMES = Collections.synchronizedSet(new HashSet<>());

    final private static Set<String> DC_DATASOURCE_ALLOWED_JRE_CLASSES = new HashSet<>();

    static {
        final String[] allowedJREClasses = {
            "[B",
            "java.lang.Boolean",
            "java.lang.Double",
            "java.lang.Integer",
            "java.lang.Long",
            "java.lang.Number",
            "java.lang.Short",
            "java.lang.String",
            "java.util.Date",
            "java.util.HashMap"
        };

        DC_DATASOURCE_ALLOWED_JRE_CLASSES.addAll(Arrays.asList(allowedJREClasses));
    }
}
