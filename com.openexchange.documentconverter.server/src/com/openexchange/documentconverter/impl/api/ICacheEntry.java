/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl.api;

import java.io.File;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;

/**
 * {@link ICacheEntry}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
public interface ICacheEntry extends Serializable {

    /**
     * @return True if this is a valid cache entry.
     */
    boolean isValid();

    /**
     * Frees resources associated with this entry. The entry is not valid after this call anymore
     * and all persistent data is removed.
     */
    void clear();

    /**
     * @return The hash value of this entry
     */
    String getHash();

    /**
     * @return The result properties of this entry;
     */
    HashMap<String, Object> getResultProperties();

    /**
     * @return The result content of this entry;
     */
    byte[] getResultContent();

    /**
     * @return The path to the file containing the result data of this entry
     */
    File getResultFile();

    /**
     * @return The local filesystem directory, where the entries persistent data is stored
     */
    File getPersistentDirectory();

    /**
     * @return The size of the entries persistent directory on its local file system
     */
    long getPersistentSize();

    /**
     * @return The point of time, when the entry was accessed the last time
     */
    Date getAccessTime();

    /**
     * Updates the result property map of this entry and
     * makes the data of this entry persistent, so that it can be completely recreated
     * at a later point of time
     *
     * @return true, if the file has been updated and made persistent (even in a previous call)
     */
    boolean updateAndMakePersistent(HashMap<String, Object> resultProperties);
}
