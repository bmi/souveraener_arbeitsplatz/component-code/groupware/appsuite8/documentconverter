/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl.api;

import java.io.File;
import org.apache.commons.io.FileUtils;

/**
 * {@link ICacheHashFileMapper}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.5
 */
public interface ICacheHashFileMapper {

    String HASH_INVALID = "";

    File CACHE_FALLBACK_DIR = new File(FileUtils.getTempDirectory(), "oxdc.cache");

    /**
     * Mappping a given hash string value to a target file.
     *
     * @param hash The hash value to be mapped to a file.
     * @return The file that maps is mapped to the given hash value.
     */
    File mapHashToFile(String hash);

    /**
     * Mappping a given hash string value to a target file.
     * The default implementation of this interface returns the
     * last filen ame part of the given file as hash string value.
     *
     * @param file The file to be mapped to a hash string value.
     * @return The hash string value that maps to the given file.
     */
    default String mapFileToHash(File file) {
        return (null != file) ?
            file.getName() :
                HASH_INVALID;
    }

    /**
     * Retrieving the root directory of the cache persistent storage.
     *
     * @return The root directory of the cache persistent storage.
     */
    default File getRootDir() {
        return CACHE_FALLBACK_DIR;
    }

    /**
     * Retrieving the number of of subdirectories below the root directory,
     * that are created to contain the target hash entry directory.
     *
     * @return The number of subdirectories below the root directory,
     *  that are created to contain the target hash entry directory.
     */
    default int getDepth() {
        return 0;
    }

}
