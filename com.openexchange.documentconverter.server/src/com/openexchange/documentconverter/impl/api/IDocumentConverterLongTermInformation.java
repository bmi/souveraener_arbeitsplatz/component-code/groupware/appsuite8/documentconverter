/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl.api;

import com.openexchange.management.MBeanMethodAnnotation;

/**
 * {@link IDocumentConverterLongTermInformation}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 * @since v7.10.0
 */
public interface IDocumentConverterLongTermInformation extends IDocumentConverterInformationMBean {

    /**
     * @return
     */
    @MBeanMethodAnnotation (description="The runtime in seconds for the current DocumentConverter server instance", parameterDescriptions = {}, parameters = {})
    long getRuntimeSecondsInstance();

    /**
     * @return
     */
    @MBeanMethodAnnotation (description="The accumulated runtime in seconds for all previously started DocumentConverter server instances and the current DocumentConverter server instance", parameterDescriptions = {}, parameters = {})
    long getRuntimeSecondsLongTerm();

    // -------------------------------------------------------------------------

    /**
     * @return
     */
    @MBeanMethodAnnotation (description="The number of all WebService requests, that have the user flag set to true, for the current DocumentConverter server instance", parameterDescriptions = {}, parameters = {})
    long getUserRequestCountInstance_Total();

    /**
     * @return
     */
    @MBeanMethodAnnotation (description="The number of text document related WebService requests, that have the user flag set to true, for the current DocumentConverter server instance", parameterDescriptions = {}, parameters = {})
    long getUserRequestCountInstance_Text();

    /**
     * @return
     */
    @MBeanMethodAnnotation (description="The number of spreadsheet document related WebService requests, that have the user flag set to true, for the current DocumentConverter server instance", parameterDescriptions = {}, parameters = {})
    long getUserRequestCountInstance_Spreadsheet();

    /**
     * @return
     */
    @MBeanMethodAnnotation (description="The number of presentation document related WebService requests, that have the user flag set to true, for the current DocumentConverter server instance", parameterDescriptions = {}, parameters = {})
    long getUserRequestCountInstance_Presentation();

    /**
     * @return
     */
    @MBeanMethodAnnotation (description="The number of image related WebService requests, that have the user flag set to true, for the current DocumentConverter server instance", parameterDescriptions = {}, parameters = {})
    long getUserRequestCountInstance_Image();

    // -------------------------------------------------------------------------

    /**
     * @return
     */
    @MBeanMethodAnnotation (description="The accumulated number of all WebService requests, that have the user flag set to true, for all previously started DocumentConverter server instances and the current DocumentConverter server instance", parameterDescriptions = {}, parameters = {})
    long getUserRequestCountLongTerm_Total();

    /**
     * @return
     */
    @MBeanMethodAnnotation (description="The accumulated number of text document related WebService requests, that have the user flag set to true, for all previously started DocumentConverter server instances and the current DocumentConverter server instance", parameterDescriptions = {}, parameters = {})
    long getUserRequestCountLongTerm_Text();

    /**
     * @return
     */
    @MBeanMethodAnnotation (description="The accumulated number of spreadsheet document related WebService requests, that have the user flag set to true, for all previously started DocumentConverter server instances and the current DocumentConverter server instance", parameterDescriptions = {}, parameters = {})
    long getUserRequestCountLongTerm_Spreadsheet();

    /**
     * @return
     */
    @MBeanMethodAnnotation (description="The accumulated number of presentation document related WebService requests, that have the user flag set to true, for all previously started DocumentConverter server instances and the current DocumentConverter server instance", parameterDescriptions = {}, parameters = {})
    long getUserRequestCountLongTerm_Presentation();

    /**
     * @return
     */
    @MBeanMethodAnnotation (description="The accumulated number of image related WebService requests, that have the user flag set to true, for all previously started DocumentConverter server instances and the current DocumentConverter server instance", parameterDescriptions = {}, parameters = {})
    long getUserRequestCountLongTerm_Image();

    // -------------------------------------------------------------------------

    /**
     * @return
     */
    @MBeanMethodAnnotation (description="The number of all WebService requests, that failed with an error and that have the user flag set to true, for the current DocumentConverter server instance", parameterDescriptions = {}, parameters = {})
    long getUserRequestCountInstanceFailed_Total();

    /**
     * @return
     */
    @MBeanMethodAnnotation (description="The number of text document related WebService requests, that failed with an error and that have the user flag set to true, for the current DocumentConverter server instance", parameterDescriptions = {}, parameters = {})
    long getUserRequestCountInstanceFailed_Text();

    /**
     * @return
     */
    @MBeanMethodAnnotation (description="The number of spreadsheet document related WebService requests, that failed with an error and that have the user flag set to true, for the current DocumentConverter server instance", parameterDescriptions = {}, parameters = {})
    long getUserRequestCountInstanceFailed_Spreadsheet();

    /**
     * @return
     */
    @MBeanMethodAnnotation (description="The number of presentation document related WebService requests, that failed with an error and that have the user flag set to true, for the current DocumentConverter server instance", parameterDescriptions = {}, parameters = {})
    long getUserRequestCountInstanceFailed_Presentation();

    /**
     * @return
     */
    @MBeanMethodAnnotation (description="The number of image related WebService requests, that failed with an error and that have the user flag set to true, for the current DocumentConverter server instance", parameterDescriptions = {}, parameters = {})
    long getUserRequestCountInstanceFailed_Image();

    // -------------------------------------------------------------------------

    /**
     * @return
     */
    @MBeanMethodAnnotation (description="The accmulated number of all WebService requests, that failed with an error and that have the user flag set to true, for all previously started DocumentConverter server instances and the current DocumentConverter server instance", parameterDescriptions = {}, parameters = {})
    long getUserRequestCountLongTermFailed_Total();

    /**
     * @return
     */
    @MBeanMethodAnnotation (description="The accmulated number of text document related WebService requests, that failed with an error and that have the user flag set to true, for all previously started DocumentConverter server instances and the current DocumentConverter server instance", parameterDescriptions = {}, parameters = {})
    long getUserRequestCountLongTermFailed_Text();

    /**
     * @return
     */
    @MBeanMethodAnnotation (description="The accmulated number of spreadsheet document related WebService requests, that failed with an error and that have the user flag set to true, for all previously started DocumentConverter server instances and the current DocumentConverter server instance", parameterDescriptions = {}, parameters = {})
    long getUserRequestCountLongTermFailed_Spreadsheet();

    /**
     * @return
     */
    @MBeanMethodAnnotation (description="The accmulated number of presentation document related WebService requests, that failed with an error and that have the user flag set to true, for all previously started DocumentConverter server instances and the current DocumentConverter server instance", parameterDescriptions = {}, parameters = {})
    long getUserRequestCountLongTermFailed_Presentation();

    /**
     * @return
     */
    @MBeanMethodAnnotation (description="The accmulated number of image related WebService requests, that failed with an error and that have the user flag set to true, for all previously started DocumentConverter server instances and the current DocumentConverter server instance", parameterDescriptions = {}, parameters = {})
    long getUserRequestCountLongTermFailed_Image();

    // -------------------------------------------------------------------------

    /**
     * @return
     */
    @MBeanMethodAnnotation (description="The ratio of successful WebService requests against failed WebService requests, that have the user flag set to true, for the current DocumentConverter server instance", parameterDescriptions = {}, parameters = {})
    double getUserRequestSuccessRatioInstance_Total();

    /**
     * @return
     */
    @MBeanMethodAnnotation (description="The ratio of successful text document related WebService requests against failed text document related WebService requests, that have the user flag set to true, for the current DocumentConverter server instance", parameterDescriptions = {}, parameters = {})
    double getUserRequestSuccessRatioInstance_Text();

    /**
     * @return
     */
    @MBeanMethodAnnotation (description="The ratio of successful spreadsheet document related WebService requests against failed spreadsheet document related WebService requests, that have the user flag set to true, for the current DocumentConverter server instance", parameterDescriptions = {}, parameters = {})
    double getUserRequestSuccessRatioInstance_Spreadsheet();

    /**
     * @return
     */
    @MBeanMethodAnnotation (description="The ratio of successful presentation document related WebService requests against failed presentation document related WebService requests, that have the user flag set to true, for the current DocumentConverter server instance", parameterDescriptions = {}, parameters = {})
    double getUserRequestSuccessRatioInstance_Presentation();

    /**
     * @return
     */
    @MBeanMethodAnnotation (description="The ratio of successful image related WebService requests against failed image related WebService requests, that have the user flag set to true, for the current DocumentConverter server instance", parameterDescriptions = {}, parameters = {})
    double getUserRequestSuccessRatioInstance_Image();

    // -------------------------------------------------------------------------

    /**
     * @return
     */
    @MBeanMethodAnnotation (description="The accumulated ratio of successful WebService requests against failed WebService requests, that have the user flag set to true, for all previously started DocumentConverter server instances and the current DocumentConverter server instance", parameterDescriptions = {}, parameters = {})
    double getUserRequestSuccessRatioLongTerm_Total();

    /**
     * @return
     */
    @MBeanMethodAnnotation (description="The accumulated ratio of successful text document related WebService requests against failed text document related WebService requests, that have the user flag set to true, for all previously started DocumentConverter server instances and the current DocumentConverter server instance", parameterDescriptions = {}, parameters = {})
    double getUserRequestSuccessRatioLongTerm_Text();

    /**
     * @return
     */
    @MBeanMethodAnnotation (description="The accumulated ratio of successful spreadsheet document related WebService requests against failed spreadsheet document related WebService requests, that have the user flag set to true, for all previously started DocumentConverter server instances and the current DocumentConverter server instance", parameterDescriptions = {}, parameters = {})
    double getUserRequestSuccessRatioLongTerm_Spreadsheet();

    /**
     * @return
     */
    @MBeanMethodAnnotation (description="The accumulated ratio of successful presentation document related WebService requests against failed presentation document related WebService requests, that have the user flag set to true, for all previously started DocumentConverter server instances and the current DocumentConverter server instance", parameterDescriptions = {}, parameters = {})
    double getUserRequestSuccessRatioLongTerm_Presentation();

    /**
     * @return
     */
    @MBeanMethodAnnotation (description="The accumulated ratio of successful image related WebService requests against failed image related WebService requests, that have the user flag set to true, for all previously started DocumentConverter server instances and the current DocumentConverter server instance", parameterDescriptions = {}, parameters = {})
    double getUserRequestSuccessRatioLongTerm_Image();

    // -------------------------------------------------------------------------

    /**
     * @return
     */
    @MBeanMethodAnnotation (description="The accumulated number of times the job queue high limit has been reached for the current DocumentConverter server instance", parameterDescriptions = {}, parameters = {})
    long getQueueLimitHighReachedCountInstance();

    /**
     * @return
     */
    @MBeanMethodAnnotation (description="The accumulated number of times the job queue low limit has been reached for the current DocumentConverter server instance", parameterDescriptions = {}, parameters = {})
    long getQueueLimitLowReachedCountInstance();

    /**
     * @return
     */
    @MBeanMethodAnnotation (description="The accumulated number of times the job queue high limit has been reached for all previously started DocumentConverter server instances and the current DocumentConverter server instance", parameterDescriptions = {}, parameters = {})
    long getQueueLimitHighReachedCountLongTerm();

    /**
     * @return
     */
    @MBeanMethodAnnotation (description="The accumulated number of times the job queue low limit has been reached all previously started DocumentConverter server instances and the current DocumentConverter server instance", parameterDescriptions = {}, parameters = {})
    long getQueueLimitLowReachedCountLongTerm();

    /**
     * @return
     */
    @MBeanMethodAnnotation (description="The accumulated number of times the job queue timeout has been reached for the current DocumentConverter server instance", parameterDescriptions = {}, parameters = {})
    long getQueueTimeoutCountInstance();

    /**
     * @return
     */
    @MBeanMethodAnnotation (description="The accumulated number of times the job queue timeout has been reached for all previously started DocumentConverter server instances and the current DocumentConverter server instance", parameterDescriptions = {}, parameters = {})
    long getQueueTimeoutCountLongTerm();
}
