/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl.api;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;

//--------
//- IJob -
//--------

/**
 * {@link IJob}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
public interface IJob {

    /**
     * @return The type of engine, the current job is assigned to
     */
    BackendType backendTypeNeeded();

    /**
     * This method will be called in order to let the job do the real work
     *
     * @param jobExecutionData
     * @return
     * @throws InterruptedException
     * @throws IOException
     * @throws MalformedURLException
     * @throws Exception
     */
    ConverterStatus execute(Object jobExecutionData) throws InterruptedException, IOException, MalformedURLException, Exception;

    /**
     * Will be called, if the engine, used by the implementation should abort the current job
     */
    void kill() throws Exception;

    /**
     * @return The hash value, upon which this job (-result) can be uniquely identified
     */
    String getHash();

    /**
     * @param jobProperties Additional job properties to be added to this job; may be called more than once
     */
    void addJobProperties(HashMap<String, Object> jobProperties);

    /**
     * @return The current job properties
     */
    HashMap<String, Object> getJobProperties();

    /**
     * @return The current job properties
     */
    HashMap<String, Object> getResultProperties();
}
