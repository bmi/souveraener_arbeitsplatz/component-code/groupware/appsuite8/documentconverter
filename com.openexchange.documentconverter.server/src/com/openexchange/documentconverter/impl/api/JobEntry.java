/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.impl.api;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import com.openexchange.documentconverter.JobErrorEx;
import com.openexchange.documentconverter.JobPriority;
import com.openexchange.documentconverter.NonNull;
import com.openexchange.documentconverter.Properties;
import com.openexchange.documentconverter.impl.Cache;
import com.openexchange.documentconverter.impl.JobProcessor;

//------------------
//- class JobEntry -
//------------------

/**
 * {@link JobEntry}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
final public class JobEntry {

    /**
     * Initializes a new {@link JobEntry}.
     *
     * @param nJobId
     * @param aServerJob
     */
    public JobEntry(@NonNull String jobId, @NonNull IJob serverJob, @NonNull JobPriority jobPriority, @NonNull IJobStatusListener jobStatusListener) {
        m_jobId = jobId;
        m_job = serverJob;
        m_jobPriority = jobPriority;
        m_jobStatusListener = jobStatusListener;

        setJobStatus(JobStatus.NEW);

        if (null != serverJob) {
            final HashMap<String, Object> jobProperties = serverJob.getJobProperties();

            if (null != jobProperties) {
                jobProperties.put(Properties.PROP_JOBID, jobId);
            }
        }
    }

    /**
     * @return The unique id of this Job
     */
    public String getJobId() {
        return m_jobId;
    }

    /**
     * @return
     */
    public String getHash() {
        return m_job.getHash();
    }

    /**
     * @return
     */
    public String getQueueIdentifier() {
        final String hash = getHash();

        return (Cache.isValidHash(hash) ? hash : getJobId());
    }

    /**
     * @return
     */
    public HashMap<String, Object> getJobProperties() {
        return m_job.getJobProperties();
    }

    /**
     * @return
     */
    public HashMap<String, Object> getResultProperties() {
        return m_job.getResultProperties();
    }

    /**
     * @return The corresponding Job within the {@link JobProcessor}
     */
    public IJob getJob() {
        return m_job;
    }

    /**
     * @return the current status of the Job
     */
    public synchronized JobStatus getJobStatus() {
        return m_jobStatus;
    }

    /**
     * @param eStatus
     */
    public synchronized void setJobStatus(JobStatus status) {
        if (status != m_jobStatus) {
            m_jobStatus = status;

            // the timestamp of a new status will only be tracked once
            if (0 == m_statusTimeMillis[status.ordinal()]) {
                m_statusTimeMillis[status.ordinal()] = System.currentTimeMillis();
            } else if (JobStatus.SCHEDULED == status) {
                // set flag that SCHEDULE status has been set more than once
                m_multipleSchedule.compareAndSet(false, true);
            }
        }
    }

    /**
     * @return the current status of the Job
     */
    public synchronized JobErrorEx getJobErrorEx() {
        return m_jobErrorEx;
    }

    /**
     * @param eStatus
     */
    public synchronized void setJobErrorEx(JobErrorEx jobErrorEx) {
        m_jobErrorEx = jobErrorEx;
    }

    /**
     * @return
     */
    public IJobStatusListener getJobStatusListener() {
        return m_jobStatusListener;
    }

    /**
     * @return
     */
    public JobPriority getJobPriority() {
        return m_jobPriority;
    }

    /**
     * @return The point of time, at which this entry has been scheduled for the first time
     */
    public long getScheduledAtTimeMillis() {
        return m_statusTimeMillis[JobStatus.SCHEDULED.ordinal()];
    }

    /**
     * @return The duration of the job in queue before running, if job has been finished; 0 otherwise
     */
    public long getQueueTimeMillis() {
        final long finishedTimeMillis = Math.max(m_statusTimeMillis[JobStatus.FINISHED.ordinal()], m_statusTimeMillis[JobStatus.ERROR.ordinal()]);

        // job is finished if either ERROR or FINISHED status has been reached
        return ((finishedTimeMillis > 0) ?  (m_statusTimeMillis[JobStatus.RUNNING.ordinal()] - m_statusTimeMillis[JobStatus.SCHEDULED.ordinal()]) : 0);
    }

    /**
     * @return The pure conversion duration of the job, if job has been finished; 0 otherwise
     */
    public long getConversionTimeMillis() {
        final long finishedTimeMillis = Math.max(m_statusTimeMillis[JobStatus.FINISHED.ordinal()], m_statusTimeMillis[JobStatus.ERROR.ordinal()]);

        // job is finished if either ERROR or FINISHED status has been reached
        return ((finishedTimeMillis > 0) ? (finishedTimeMillis -  m_statusTimeMillis[JobStatus.RUNNING.ordinal()]) : 0);
    }

    /**
     * @return The complete processing duration of the job from adding to removing, if job has been finished; 0 otherwise
     */
    public long getJobTimeMillis() {
        final long finishedTimeMillis = Math.max(m_statusTimeMillis[JobStatus.FINISHED.ordinal()], m_statusTimeMillis[JobStatus.ERROR.ordinal()]);

        // job is finished if either ERROR or FINISHED status has been reached
        return ((finishedTimeMillis > 0) ? (finishedTimeMillis -  m_statusTimeMillis[JobStatus.SCHEDULED.ordinal()]) : 0);
    }

    /**
     * @param cacheHit
     */
    public void setCacheHit() {
        m_cacheHit.compareAndSet(false, true);
    }

    /**
     * @return True if the job could be resolved from cache
     */
    public boolean isCacheHit() {
        return m_cacheHit.get();
    }

    /**
     * @param errorCacheHit
     */
    public synchronized void setErrorCacheHit(JobErrorEx jobErrorEx) {
        if (jobErrorEx.hasError()) {
            m_jobErrorEx = jobErrorEx;
            m_errorCacheHit.compareAndSet(false, true);
        }
    }

    /**
     * @return True if the job could be resolved from errorcache
     */
    public boolean isErrorCacheHit() {
        return m_errorCacheHit.get();
    }

    /**
     * @return
     */
    public boolean isMultipleSchedule() {
        return m_multipleSchedule.get();
    }

    /**
     * @return The locale to be used to execute the job or the standard locale
     */
    public String getJobLocale() {
        final HashMap<String, Object> jobProperties = m_job.getJobProperties();
        String locale = null;

        if (null != jobProperties) {
            locale = (String) jobProperties.get(Properties.PROP_LOCALE);
        }

        return ((null != locale) && (locale.length() > 0)) ? locale : "en_US";
    }

    // -------------------------------------------------------------------------

    private String m_jobId = "";

    private JobPriority m_jobPriority = JobPriority.MEDIUM;

    private IJobStatusListener m_jobStatusListener = null;

    private IJob m_job = null;

    private JobStatus m_jobStatus = JobStatus.INVALID;

    private JobErrorEx m_jobErrorEx = new JobErrorEx();

    private volatile long[] m_statusTimeMillis = new long[JobStatus.values().length];

    private AtomicBoolean m_cacheHit = new AtomicBoolean(false);

    private AtomicBoolean m_errorCacheHit = new AtomicBoolean(false);

    private AtomicBoolean m_multipleSchedule = new AtomicBoolean(false);
}
