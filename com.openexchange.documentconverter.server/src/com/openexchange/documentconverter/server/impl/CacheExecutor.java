/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.impl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import com.openexchange.documentconverter.DocumentConverterManager;
import com.openexchange.documentconverter.JobError;
import com.openexchange.documentconverter.LogData;
import com.openexchange.documentconverter.NonNull;
import com.openexchange.documentconverter.Properties;
import com.openexchange.documentconverter.impl.Cache;
import com.openexchange.documentconverter.impl.CacheEntry;
import com.openexchange.documentconverter.impl.ServerManager;
import com.openexchange.documentconverter.impl.api.CacheHashFileMapper;
import com.openexchange.documentconverter.impl.api.ICacheHashFileMapper;
import com.openexchange.documentconverter.impl.api.IdLocker;

/**
 * {@link CacheExecutor}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
public class CacheExecutor extends Executor {

    /**
     * EXTENDED_CACHE_API
     */
    final private static boolean EXTENDED_CACHE_API = false;

    /**
     * Initializes a new {@link JobExecutor}.
     *
     * @param request
     */
    public CacheExecutor(ServerManager manager, DocumentConverterServlet servlet, Cache cache, HttpServletRequest request, HttpServletResponse response) throws InvalidParameterException {
        super(manager, servlet, request, response);

        m_cache = cache;
    }

    // - Overrides -------------------------------------------------------------

    @Override
    protected Map<String, Map<String, String[]>> getParamDictionary() {
        return m_paramDictionary;
    }

    /*
     * (non-Javadoc)
     * @see com.openexchange.documentconverterws.impl.Executor#doExecute(java.util.HashMap, java.util.HashMap)
     */
    @SuppressWarnings("unused")
    @Override
    protected boolean doExecute(Map<String, String> params, HashMap<String, Object> results) {
        boolean ret = false;

        if ((null != m_cache) && (null != params) && (null != results)) {
            final String action = params.get("action");

            if ("getentry".equals(action)) {
                ret = getEntry(params);
            } else if ("addentry".equals(action)) {
                ret = addEntry(params);
            } else if (EXTENDED_CACHE_API && "clear".equals(action)) {
                ret = clearEntries(params);
            } else if (EXTENDED_CACHE_API && "create".equals(action)) {
                ret = createEntries(params);
            }
        } else {
            results.put(Properties.PROP_RESULT_ERROR_CODE, Integer.valueOf(JobError.GENERAL.getErrorCode()));
        }

        return ret;
    }

    // - Implementation

    /**
     * @param params
     * @param results
     * @return true, if request has been processed; true even, if not cached entry could be found, but reques is ok.
     */
    protected boolean getEntry(Map<String, String> params) {
        final String cacheUID = params.get("cuid");
        final String hash = params.get("hash");
        boolean ret = true;

        if (!isSameCacheInstance(cacheUID) && isValidHash(hash)) {
            final File tempDir = m_manager.createTempDir(UUID.randomUUID().toString());
            final CacheEntry tempCacheEntry = implCreateTempCacheEntry(tempDir, hash);

            try {
                if (null != tempCacheEntry) {
                    m_response.reset();
                    m_response.setContentType("application/octet-stream");
                    m_response.setHeader("Content-Disposition", "attachment;filename=ICacheEntry.bin");

                    // write entry via an ObjectOutputStream instance
                    try (final ObjectOutputStream objOutputStm = new ObjectOutputStream(m_response.getOutputStream())) {
                        objOutputStm.writeObject(tempCacheEntry);
                        objOutputStm.flush();

                        if (ServerManager.isLogTrace()) {
                            ServerManager.logTrace("DC returned requested remote cache entry", new LogData("hash", hash));
                        }
                    }
                }
            } catch (Exception e) {
                ret = false;
                ServerManager.logExcp(e);
            } finally {
                FileUtils.deleteQuietly(tempDir);
            }
        }

        return ret;
    }

    /**
     * @param params
     * @param results
     * @return
     */
    protected boolean addEntry(Map<String, String> params) {
        if (!isSameCacheInstance(params.get("cuid")) && (null != m_transferObject)) {
            final Serializable serialObject = m_transferObject.getSerialObject();

            if (serialObject instanceof CacheEntry) {
                final CacheEntry readEntry = (CacheEntry) serialObject;
                final String hash = readEntry.isValid() ? readEntry.getHash() : null;

                // The just read entry is not persistent and does not need to be cleared
                if (isValidHash(hash)) {
                    IdLocker.lock(hash);

                    try {
                        // creating a new local, persistent cache entry with just read data
                        final CacheEntry cacheEntry = CacheEntry.createAndMakePersistent(
                            m_cache.getHashFileMapper(),
                            hash,
                            readEntry.getResultProperties(),
                            readEntry.getResultContent());

                        if (null != cacheEntry) {
                            if (m_cache.addEntry(cacheEntry, false, null)) {
                                if (ServerManager.isLogTrace()) {
                                    ServerManager.logTrace("DC added new remote cache entry", new LogData("hash", readEntry.getHash()));
                                }
                            } else {
                                // clear just created entry if not valid or not added to cache
                                cacheEntry.clear();
                            }
                        }
                    } finally {
                        IdLocker.unlock(hash);
                    }
                }
            }
        }

        // return true in every case, the client is not interested in any response from this request
        return true;
    }

    /**
     * @return
     */
    protected boolean clearEntries(Map<String, String> params) {
        final String countStr = params.get("count");
        new Thread(() -> {
            final long clearStartTimeMillis = System.currentTimeMillis();
            final boolean trace = DocumentConverterManager.isLogTrace();
            long count = 0;

            try {
                count = Math.max(-1, Long.parseLong((null != countStr) ? countStr : "-1"));
            } catch (NumberFormatException e) {
                DocumentConverterManager.logExcp(e);
            }

            if (trace) {
                DocumentConverterManager.logTrace(new StringBuilder(256).append("DC started thread to clear cache...: ").
                    append((count < 0) ? "all" : Long.toString(count)).append(" entries to be removed").toString());
            }

            final long removed = m_cache.clear(count);

            if (trace) {
                final long durationMillis = System.currentTimeMillis() - clearStartTimeMillis;

                DocumentConverterManager.logTrace(new StringBuilder(256).append("DC thread finished clearing cache ").
                    append("(removed: ").append(removed).
                    append(", duration: ").append(durationMillis).append("ms").
                    append(", remove throughput: ").append((long)((removed * 1000.0) / Math.max(durationMillis, 1.0))).append("/s)").toString());
            }
        }).start();

        return true;
    }

    /**
     * @param params
     * @return
     */
    protected boolean createEntries(Map<String, String> params) {
        final String hashRef = params.get("hashref");
        final String prefix = params.get("prefix");
        final String countStr = params.get("count");
        final String offsetStr = params.get("offset");

        if (isValidHash(hashRef)) {
            new Thread(() -> {
                final File tempDir = m_manager.createTempDir(UUID.randomUUID().toString());
                final CacheEntry tempCacheEntry = implCreateTempCacheEntry(tempDir, hashRef);

                try {
                    if (null != tempCacheEntry) {
                        final String usedPrefix = (null != prefix) ? prefix : "";
                        final boolean trace = DocumentConverterManager.isLogTrace();
                        final long count = Math.max(0, Long.valueOf((null != countStr) ? countStr : "0"));
                        final long offset = Math.max(0, Long.valueOf((null != offsetStr) ? offsetStr : "0"));

                        if (trace) {
                            DocumentConverterManager.logTrace(new StringBuilder(256).append("DC started thread to add new cache entries...: ").
                                append(count).
                                append(" (prefix: ").append(prefix).
                                append(", offset: ").append(offset).
                                append(", hashref: ").append(hashRef).
                                append(')').toString());
                        }

                        final long created = implFillCache(tempCacheEntry, usedPrefix, count, offset + 1);

                        if (trace) {
                            DocumentConverterManager.logTrace(new StringBuilder(256).append("DC thread finished adding new cache entries: ").
                                append(created).
                                append(" (total: ").append(m_cache.getLocalEntryCount()).append(')').toString());
                        }
                    }
                } finally {
                    FileUtils.deleteQuietly(tempDir);
                }
            }).start();

            return true;
        }

        return false;
    }

    // - Implementation --------------------------------------------------------

    /**
     * @param hash
     * @param calledRemoteUrl
     * @return
     */
    private CacheEntry implCreateTempCacheEntry(final File rootDir, @NonNull final String hash) {
        HashMap<String, Object> resultProperties = new HashMap<>(8);
        CacheEntry ret = null;

        if (null != rootDir) {
            try (final InputStream resultStm = ServerManager.getCachedResult(m_cache, hash, null, resultProperties, false)) {
                if (null != resultStm) {
                    final byte[] resultContent = IOUtils.toByteArray(resultStm);

                    if (null != resultContent) {
                        ret = CacheEntry.createAndMakePersistent(
                            new CacheHashFileMapper(rootDir, 0),
                            hash,
                            resultProperties,
                            resultContent);
                    }
                }
            } catch (IOException e) {
                ServerManager.logExcp(e);
            }
        }

        return ret;
    }

    /**
     * @param cacheUID
     * @return
     */
    private boolean isSameCacheInstance(final String cacheUID) {
        return (StringUtils.isNotEmpty(cacheUID) ?
            cacheUID.equalsIgnoreCase(m_cache.getUID()) :
                false);
    }

    /**
     * @param tempCacheEntry
     * @param prefix
     * @param count
     * @param startNumber
     * @return
     */
    private long implFillCache(@NonNull final CacheEntry tempCacheEntry, @NonNull final String prefix, final long count, final long startNumber) {
        final String hashRef = tempCacheEntry.getHash();
        final Map<String, Object> resultProperties = tempCacheEntry.getJobResult();
        final byte[] resultBuffer = (byte[]) ((null != resultProperties) ? resultProperties.get(Properties.PROP_RESULT_BUFFER) : null);
        final boolean trace = ServerManager.isLogTrace();
        long created = 0;

        if ((null != resultBuffer) && (count > 0)) {
            final ICacheHashFileMapper hashFileMapper = m_cache.getHashFileMapper();
            final long chunkEntryCount = 100000;
            final int chunkCount = (int) (count / chunkEntryCount + (((count % chunkEntryCount) > 0) ? 1 : 0));
            final long endNumber = startNumber + count - 1;
            long  curNumber = startNumber;
            long startTimeMillis = System.currentTimeMillis();
            long lastChunkStartTimeMillis = startTimeMillis;

            for (int i = 0; i < chunkCount; ++i) {
                final long curCount = Math.min(endNumber - curNumber + 1, chunkEntryCount);
                final long oldCreated = created;

                for (long curEndNumber = curNumber + curCount - 1; curNumber <= curEndNumber; ++curNumber) {
                    final CacheEntry newCacheEntry = CacheEntry.createAndMakePersistent(
                        hashFileMapper,
                        prefix + curNumber,
                        resultProperties,
                        new byte[] { 0 });

                    if (null != newCacheEntry) {
                        if (newCacheEntry.isValid() && m_cache.addEntry(newCacheEntry, false, null)) {
                            ++created;
                        } else {
                            newCacheEntry.clear();
                        }
                    }
                }

                if (trace) {
                    final long curTimeMillis = System.currentTimeMillis();
                    final long curCreated = created - oldCreated;
                    final long durationMillis = curTimeMillis - lastChunkStartTimeMillis;

                    ServerManager.logTrace(new StringBuilder(256).append("DC created chunk of cache entries: ").
                        append(created).append(" / ").append(endNumber - curNumber + 1).append(" left to create").
                        append(" (chunk duration: ").append(durationMillis).append("ms").
                        append(", chunk create throughput: ").append((long)((curCreated * 1000.0) / Math.max(durationMillis, 1.0))).append("/s)").toString());

                    lastChunkStartTimeMillis = curTimeMillis;
                }
            }

            if (trace) {
                final long durationMillis = System.currentTimeMillis() - startTimeMillis;

                ServerManager.logTrace(new StringBuilder(256).append("DC finished adding new cache entries: ").
                    append(created).append(" / ").append(count).
                    append(" (duration: ").append(durationMillis).append("ms").
                    append(", create throughput: ").append((long)((created * 1000.0) / Math.max(durationMillis, 1.0))).append("/s)").toString());
            }
        } else if (trace && (null == resultBuffer)) {
            ServerManager.logTrace("DC cannot read content of reference cache entry to create new entries", new LogData("hash", hashRef));
        }

        return created;
    }

    // - Members ---------------------------------------------------------------

    /**
     * The conversion service manager interface
     */
    protected Cache m_cache = null;

    static private Map<String, Map<String, String[]>> m_paramDictionary = new HashMap<>(4);

    // initialize static m_dictionary member
    static {
        HashMap<String, String[]> curMap = null;
        final String[] arbitraryString = new String[0];
        final String[] defaultSerializedString = { ":serialized", "native" };

        // fill 'addentry' map
        m_paramDictionary.put("addentry", curMap = new HashMap<>());
        curMap.put("multipartfileformat", defaultSerializedString);
        curMap.put("cuid", arbitraryString);

        // fill 'getentry' map
        m_paramDictionary.put("getentry", curMap = new HashMap<>());
        curMap.put("hash", arbitraryString);
        curMap.put("remoteurl", arbitraryString);
        curMap.put("multipartfileformat", defaultSerializedString);
        curMap.put("cuid", arbitraryString);

        // fill 'clear' map
        m_paramDictionary.put("clear", curMap = new HashMap<>());
        curMap.put("count", arbitraryString);

        // fill 'create' map
        m_paramDictionary.put("create", curMap = new HashMap<>());
        curMap.put("hashref", arbitraryString);
        curMap.put("prefix", arbitraryString);
        curMap.put("count", arbitraryString);
        curMap.put("offset", arbitraryString);
}
}

