/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */


package com.openexchange.documentconverter.server.impl;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.security.InvalidParameterException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.documentconverter.DocumentConverterUtil;
import com.openexchange.documentconverter.JobError;
import com.openexchange.documentconverter.LogData;
import com.openexchange.documentconverter.NonNull;
import com.openexchange.documentconverter.Nullable;
import com.openexchange.documentconverter.Properties;
import com.openexchange.documentconverter.impl.Cache;
import com.openexchange.documentconverter.impl.DocumentConverterInformation;
import com.openexchange.documentconverter.impl.EngineStatus;
import com.openexchange.documentconverter.impl.ServerConfig;
import com.openexchange.documentconverter.impl.ServerManager;
import com.openexchange.documentconverter.impl.api.BackendType;

/**
 * {@link DocumentConverter}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
/**
 * {@link DocumentConverterServlet}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
final public class DocumentConverterServlet extends HttpServlet {

    // !!! Do not change string for compatibility reasons (KA, 2014-11-20) !!!
    final private static String OX_DOCUMENTCONVERTER_TITLE_FIXED = "Open-Xchange DC";

    final public static String PATH_SERVLET = "/documentconverterws";

    final public static String PATH_STATUS = "/status";

    final public static String PATH_LIVE = "/live";

    final public static String PATH_READY = "/ready";

    final public static String FORMAT_JSON = "json";

    final private static char LINE_SEPARATOR = '\n';

    final private static String DOCUMENTCONVERTER_SERVER_API_VERSION = "8";

    final private static String[] OX_DEFAULT_RESPONSE_TEXT = { "WebService is running...", "Error Code: 0" };

    final private static String OX_ERRORTEXT_BAD_REQUEST = "Bad request";

    final private static String OX_ERRORTEXT_READINESS_ERROR = "Readiness check failed";

    final private static String OX_ERRORTEXT_GENERAL_ERROR = "General error";

    final private static String OX_ERRORTEXT_QUEUE_COUNT_LIMIT_REACHED = "JobCountInQueue too high";

    final private static String OX_ERRORTEXT_QUEUE_TIMEOUT = "JobQueueTimeout reached";

    final private static String OX_ERRORTEXT_READERENGINE_NOT_RUNNING = "DC reader engine is not running. Please check your configuration...";

    final private static long INITIAL_PROBE_TIMER_DELAY_MILLIS = 5000;

    final private static long UPDATE_PROBE_PERIOD_MILLIS = 1000;

    final private static long serialVersionUID = -350408423693408833L;

    /**
     * Initializes a new {@link DocumentConverterServlet}.
     */
    public DocumentConverterServlet(@NonNull ServerManager serverManager, @NonNull final Cache cache) {
        super();

        m_serverManager = serverManager;
        m_serverConfig = m_serverManager.getServerConfig();
        m_cache = cache;

        m_readyDownDurationMillis = (m_serverConfig.READINESS_DOWN_AFTER_USED_SERVICE_UNAVAILABILITY_SECONDS > 0) ?
            (m_serverConfig.READINESS_DOWN_AFTER_USED_SERVICE_UNAVAILABILITY_SECONDS * 1000) :
                -1;

        m_liveDownDurationMillis = ((m_readyDownDurationMillis > 0) && (m_serverConfig.LIVENESS_DOWN_AFTER_READINESS_DOWN_SECONDS > 0)) ?
            (m_readyDownDurationMillis + m_serverConfig.LIVENESS_DOWN_AFTER_READINESS_DOWN_SECONDS * 1000) :
                -1;

        m_probeUpdatePeriodMillis = Math.max(1, m_serverConfig.READINESS_PERIOD_SECONDS) * 1000L;

        m_probeTimerExecutor.scheduleWithFixedDelay(() -> implUpdateProbes(), INITIAL_PROBE_TIMER_DELAY_MILLIS, UPDATE_PROBE_PERIOD_MILLIS, TimeUnit.MILLISECONDS);

        ServerManager.logInfo("DC server started probe updates with a period of " + m_probeUpdatePeriodMillis + "ms");
    }

    /**
     *
     */
    public void shutdown() {
        if (m_running.compareAndSet(true, false)) {
            if (null != m_probeTimerExecutor) {
                m_probeTimerExecutor.shutdownNow();
            }
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        implProcessRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request The servlet request
     * @param response The servlet response
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        implProcessRequest(request, response);
    }

    /**
     * Handles the HTTP <code>PUT</code> method.
     *
     * @param request The servlet request
     * @param response The servlet response
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) {
        implProcessRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Open-Xchange DC";
    }

    // - Implementation --------------------------------------------------------

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     *
     * @param request The servlet request
     * @param response The servlet response
     */
    private void implProcessRequest(HttpServletRequest request, HttpServletResponse response) {
        DocumentConverterInformation documentConverterInformation = null;
        String responseText[] = OX_DEFAULT_RESPONSE_TEXT;
        String httpErrorMsg = OX_ERRORTEXT_GENERAL_ERROR;
        int httpErrorCode = 0;

        response.reset();

        if (null == m_serverManager) {
            httpErrorCode = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
            httpErrorMsg = OX_ERRORTEXT_READERENGINE_NOT_RUNNING;
        }

        if (0 == httpErrorCode) {
            final String requestPath = request.getPathInfo();

            if (PATH_STATUS.equalsIgnoreCase(requestPath)) {
                documentConverterInformation = ("true".equalsIgnoreCase(request.getParameter("metrics"))) ?
                    m_serverManager.getMBean() :
                        null;

                implWriteStatus(response, true, documentConverterInformation);
            } else {
                try {
                    final Executor executor = implGetExecutor(request, response);

                    if (null != executor) {
                        final Map<String, String> requestParams = executor.getRequestParams();

                        if ((null != requestParams) && (requestParams.size() > 0)) {
                            if ((requestParams.size() == 1) && "true".equals(requestParams.get("metrics"))) {
                                documentConverterInformation = m_serverManager.getMBean();
                            } else {
                                final HashMap<String, Object> resultProperties = new HashMap<>(8);

                                // execute the current request
                                if (!executor.execute(resultProperties)) {
                                    final Map<String, String> params = executor.getParams();
                                    final int errorCode = resultProperties.containsKey(Properties.PROP_RESULT_ERROR_CODE) ? ((Integer) resultProperties.get(Properties.PROP_RESULT_ERROR_CODE)).intValue() : HttpServletResponse.SC_BAD_REQUEST;

                                    if (FORMAT_JSON.equals((null != params) ? params.get("returntype") : null)) {
                                        responseText = new String[] {
                                            "WebService is running, but request didn't succeed.",
                                            "Error Code: " + Integer.toString(errorCode)
                                        };
                                    } else if (!response.isCommitted()) {
                                        if (JobError.MAX_QUEUE_COUNT.getErrorCode() == errorCode) {
                                            httpErrorCode = HttpServletResponse.SC_SERVICE_UNAVAILABLE;
                                            httpErrorMsg = OX_ERRORTEXT_QUEUE_COUNT_LIMIT_REACHED;
                                        } else if (JobError.QUEUE_TIMEOUT.getErrorCode() == errorCode) {
                                            httpErrorCode = HttpServletResponse.SC_SERVICE_UNAVAILABLE;
                                            httpErrorMsg = OX_ERRORTEXT_QUEUE_TIMEOUT;
                                        } else {
                                            if (HttpServletResponse.SC_BAD_REQUEST == errorCode) {
                                                httpErrorCode = HttpServletResponse.SC_BAD_REQUEST;
                                                httpErrorMsg = OX_ERRORTEXT_BAD_REQUEST;
                                            } else {
                                                httpErrorCode = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
                                                httpErrorMsg = OX_ERRORTEXT_GENERAL_ERROR;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } catch (InvalidParameterException e) {
                    httpErrorCode = HttpServletResponse.SC_BAD_REQUEST;
                    httpErrorMsg = OX_ERRORTEXT_BAD_REQUEST;

                    ServerManager.logExcp(e);
                } catch (Exception e) {
                    httpErrorCode = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
                    httpErrorMsg = OX_ERRORTEXT_GENERAL_ERROR;

                    ServerManager.logExcp(e);
                }
            }
        }

        // readiness probe is always preferred over all other errors;
        if (!m_probeIsReady.get()) {
            httpErrorCode = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
            httpErrorMsg = OX_ERRORTEXT_READINESS_ERROR;
        }

        if (0 != httpErrorCode) {
            try {
                response.sendError(httpErrorCode, httpErrorMsg);
            } catch (IOException e) {
                ServerManager.logExcp(e);
            }
        } else if (!response.isCommitted() && (null == response.getContentType()) && (null != responseText)) {
            implWritePage(response, responseText, documentConverterInformation);
        }
    }

    /**
     * @param response The response to be filled with the default page
     * @param defaultText
     */
    private void implWritePage(HttpServletResponse response, String[] defaultText, DocumentConverterInformation documentConverterInformation) {
        response.setContentType("text/html");

        try (final PrintWriter printWriter = response.getWriter()) {
            if (null != printWriter) {
                final StringBuilder htmlBuilder = new StringBuilder(2048);

                htmlBuilder.
                    append("<html>").append(LINE_SEPARATOR).
                    append("<head><meta charset=\"UTF-8\"><title>" + OX_DOCUMENTCONVERTER_TITLE_FIXED + "</title></head>").append(LINE_SEPARATOR).
                    append("<body><h1 align=\"center\">OX Software GmbH DC</h1>").append(LINE_SEPARATOR);

                for (int i = 0; i < defaultText.length;) {
                    htmlBuilder.append("<p>" + defaultText[i++] + "</p>").append(LINE_SEPARATOR);
                }

                // print server id
                htmlBuilder.append("<p>Id: ").append(DocumentConverterUtil.DOCUMENTCONVERTER_SERVER_ID).append("</p>").append(LINE_SEPARATOR);

                // print server name for compatibility reasons (old clients might watch for this name)
                htmlBuilder.append("<p>Name: documentconverter</p>").append(LINE_SEPARATOR);

                // print server API version
                htmlBuilder.append("<p>API: v").append(DOCUMENTCONVERTER_SERVER_API_VERSION).append("</p>").append(LINE_SEPARATOR);

                // remote cache and optional CacheService server usage
                final boolean isCacheServiceConfigured = (null != m_cache) && m_cache.isCacheServerConfigured();

                htmlBuilder.append("<p>Uses CacheService: ").append(Boolean.toString(isCacheServiceConfigured)).append("</p>").append(LINE_SEPARATOR);

                if (isCacheServiceConfigured) {
                    final boolean isCacheServerConfigured = (null != m_cache) && m_cache.isCacheServerConfigured();
                    final boolean isCacheServerEnabled = (null != m_cache) && m_cache.isCacheServerEnabled();
                    final String remoteCacheUrl = (null != m_cache) ? m_cache.getRemoteCacheURL() : null;


                    htmlBuilder.append("<p>RemoteCache configured: ").append(Boolean.toString(isCacheServerConfigured)).append("</p>").append(LINE_SEPARATOR);
                    htmlBuilder.append("<p>RemoteCache available: ").append(Boolean.toString(isCacheServerEnabled)).append("</p>").append(LINE_SEPARATOR);
                    htmlBuilder.append("<p>RemoteCache URLs: ").append((null != remoteCacheUrl) ? remoteCacheUrl : DocumentConverterUtil.STR_NOT_AVAILABLE).append("</p>").append(LINE_SEPARATOR);
                    htmlBuilder.append("<p>Readiness state: ").append(m_probeIsReady.get() ? "UP" : "DOWN").append("</p>").append(LINE_SEPARATOR);
                }

                if (null != documentConverterInformation) {
                    try {
                        final JSONObject jsonMetrics = implGetMetrics(documentConverterInformation);
                        final StringBuilder metricsBuilder = new StringBuilder(htmlBuilder.capacity());

                        metricsBuilder.append("<br />").append("<p><u>Metrics</u></p>").append(LINE_SEPARATOR);

                        for (final String curKey : jsonMetrics.keySet()) {
                            metricsBuilder.append("<p>").
                                append(curKey).append(": ").
                                append(jsonMetrics.getString(curKey)).
                                append("</p>").
                                append(LINE_SEPARATOR);
                        }

                        htmlBuilder.append(metricsBuilder).append(LINE_SEPARATOR);
                    } catch (JSONException e) {
                        ServerManager.logExcp(e);
                    }
                }

                htmlBuilder.append("</body></html>").append(LINE_SEPARATOR);

                printWriter.println(htmlBuilder.toString());
                printWriter.flush();
            }

            if (ServerManager.isLogTrace()) {
                ServerManager.logTrace("DC WebService returning status page");
            }
        } catch (final IOException e) {
            ServerManager.logExcp(e);
        }
    }

    /**
     * @param response The response to be filled with the default page
     * @param defaultText
     */
    private void implWriteStatus(HttpServletResponse response,
        boolean includeEngineStatus,
        @Nullable DocumentConverterInformation documentConverterInformation) {

        response.setContentType("application/json");

        try (PrintWriter printWriter = response.getWriter()) {
            if (null != printWriter) {
                final JSONObject json = new JSONObject();

                // general
                json.put("server", implGetServerStatus(includeEngineStatus));

                if (null != documentConverterInformation) {
                    // metrics
                    json.put("metrics", implGetMetrics(documentConverterInformation));
                }

                printWriter.println(json.toString());
                printWriter.flush();

                response.setStatus(HttpServletResponse.SC_OK);
            } else {
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            }
        } catch (final Exception e) {
            ServerManager.logExcp(e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @return
     * @throws JSONException
     */
    @NonNull private JSONObject implGetServerStatus(boolean includeEngineStatus) throws JSONException {
        final JSONObject jsonStatus = new JSONObject();
        final String  remoteCacheUrl =  (null != m_cache) ? m_cache.getRemoteCacheURL() : null;

        // general
        jsonStatus.put("id", DocumentConverterUtil.DOCUMENTCONVERTER_SERVER_ID);
        jsonStatus.put("name", "documentconverter");
        jsonStatus.put("api", DOCUMENTCONVERTER_SERVER_API_VERSION);
        jsonStatus.put("status", (null != m_serverManager) ? (m_serverManager.isRunning() ? "running" : "terminated") : "invalid");
        jsonStatus.put("remoteCacheConfigured", (null != m_cache) && m_cache.isCacheServerConfigured());
        jsonStatus.put("remoteCacheAvailable", (null != m_cache) && m_cache.isCacheServerEnabled());
        jsonStatus.put("remoteCacheUrl", (null != remoteCacheUrl) ? remoteCacheUrl : DocumentConverterUtil.STR_NOT_AVAILABLE);
        jsonStatus.put("readinessState", m_probeIsReady.get() ? "UP" : "DOWN");


        // engine status
        if (null != m_serverManager) {
            if (includeEngineStatus) {
                final Map<BackendType, EngineStatus> engineStatus = m_serverManager.getEngineStatus();
                final JSONObject jsonEngineStatus = new JSONObject();

                for (final BackendType curBackendType : engineStatus.keySet()) {
                    final EngineStatus curEngineStatus = engineStatus.get(curBackendType);

                    jsonEngineStatus.put(curBackendType.toString(),
                        (new JSONObject()).
                            put("totalCount", curEngineStatus.getTotalCount()).
                            put("runningCount", curEngineStatus.getRunningCount()));
                }

                jsonStatus.put("engineStatus", jsonEngineStatus);
            }

            // server messages
            jsonStatus.put("errorMessages", new JSONArray(m_serverManager.getServerErrors()));
        }

        return jsonStatus;
    }

    /**
     * @param documentConverterInformation
     * @return
     * @throws JSONException
     */
    @NonNull private JSONObject implGetMetrics(final DocumentConverterInformation documentConverterInformation) {
        final List<Method> methodList = Arrays.asList(documentConverterInformation.getClass().getMethods());
        final JSONObject jsonMetrics = new JSONObject();

        Collections.sort(methodList, (one, other) -> one.getName().compareTo(other.getName()));

        for (Method curMethod : methodList) {
            if (null != curMethod) {
                final String methodName = curMethod.getName();

                if (isNotEmpty(methodName) && StringUtils.startsWithIgnoreCase(methodName, "get") && (curMethod.getParameterTypes().length == 0)) {
                    final String metricsPropertyName = methodName.substring(3);
                    final boolean isRatioMetric = metricsPropertyName.contains("Ratio");

                    try {
                        final String numberString = new StringBuilder().append(curMethod.invoke(documentConverterInformation)).toString();

                        if (NumberUtils.isCreatable(numberString)) {
                            if (isRatioMetric) {
                                jsonMetrics.put(metricsPropertyName, Double.parseDouble(numberString));
                            } else {
                                jsonMetrics.put(metricsPropertyName, Long.parseLong(numberString));
                            }
                        }
                    } catch (@SuppressWarnings("unused") Exception e) {
                        //
                    }
                }
            }
        }

        return jsonMetrics;
    }

    /**
     * @param request
     * @param response
     * @return
     */
    @Nullable private Executor implGetExecutor(HttpServletRequest request, HttpServletResponse response) throws InvalidParameterException {
        if (null != request) {
            final String requestPath = request.getPathInfo();

            if ((null != requestPath) && requestPath.startsWith("/cache")) {
                return new CacheExecutor(m_serverManager, this, m_cache, request, response);
            }

            // create a documentconverter/imageserver job as default
            return new JobExecutor(m_serverManager, this, request, response);
        }

        return null;
    }

    /**
     * @return
     */
    private void implUpdateProbes() {
        if (m_probeUpdateRunning.compareAndSet(false, true)) {
            try {
                if (!m_isFirstProbeUpdate.get() && ((System.currentTimeMillis() - m_lastProbeEndTimestampMillis) < m_probeUpdatePeriodMillis)) {
                    return;
                }

                String remoteCacheUrl = null;
                boolean isCacheServerConfigured = false;
                boolean isCacheServerAvailable = false;
                boolean isCacheServerRecoverPeriodNotReached = false;

                if (null != m_cache) {
                    remoteCacheUrl = m_cache.getRemoteCacheURL();
                    isCacheServerConfigured = m_cache.isCacheServerConfigured();
                    isCacheServerAvailable = m_cache.isCacheServerAvailable();
                    isCacheServerRecoverPeriodNotReached = m_cache.isCacheServerRecoverPeriodNotReached();
                }

                if (ServerManager.isLogTrace()) {
                    ServerManager.logTrace("DC server performed probe update",
                        new LogData("remoteCacheConfigured", Boolean.toString(isCacheServerConfigured)),
                        new LogData("remoteCacheAvailable", Boolean.toString(isCacheServerAvailable)),
                        new LogData("remoteCacheRecoverPeriodNotReached", Boolean.toString(isCacheServerRecoverPeriodNotReached)),
                        new LogData("remoteCacheUrl", remoteCacheUrl));
                }

                if (isCacheServerConfigured) {
                    m_lastProbeEndTimestampMillis = System.currentTimeMillis();

                    if (isCacheServerRecoverPeriodNotReached) {
                        // the delayed shutdown is peformed only once within this call
                        implInitiateDelayedShutdown("Recover period not reached");
                    }

                    if (!m_delayedShutdownRunning.get() && isCacheServerAvailable) {
                        if (m_probeIsReady.compareAndSet(false, true)) {
                            ServerManager.logInfo("DC server set internal READY state to UP after remote connection to CacheService is available",
                                new LogData("remoteCacheConfigured", Boolean.toString(isCacheServerConfigured)),
                                new LogData("remoteCacheAvailable", Boolean.toString(isCacheServerAvailable)),
                                new LogData("remoteCacheUrl", remoteCacheUrl),
                                new LogData("remoteCacheRecoverPeriodNotReached", Boolean.toString(isCacheServerRecoverPeriodNotReached)),
                                new LogData("internalReadinessDowntime", (m_lastProbeEndTimestampMillis - m_lastReadyProbeSuccessTimestampMillis) + "ms"));
                        }

                        m_lastReadyProbeSuccessTimestampMillis = m_lastProbeEndTimestampMillis;
                        m_isFirstProbeUpdate.set(false);
                    } else {
                        if (m_isFirstProbeUpdate.compareAndSet(true,  false)) {
                            ServerManager.logWarn("DC server initial remote connection to CacheService failed",
                                new LogData("remoteCacheConfigured", Boolean.toString(isCacheServerConfigured)),
                                new LogData("remoteCacheAvailable", Boolean.toString(isCacheServerAvailable)),
                                new LogData("remoteCacheRecoverPeriodNotReached", Boolean.toString(isCacheServerRecoverPeriodNotReached)),
                                new LogData("remoteCacheUrl", remoteCacheUrl));
                        }

                        final long durationSinceLastSuccessMillis = (m_lastProbeEndTimestampMillis - m_lastReadyProbeSuccessTimestampMillis);

                        if ((m_readyDownDurationMillis > 0) && (durationSinceLastSuccessMillis >= m_readyDownDurationMillis)) {
                            if (m_probeIsReady.compareAndSet(true, false)) {
                                ServerManager.logError("DC server set READY state to DOWN due to unavailable remote connection to CacheService",
                                    new LogData("remoteCacheConfigured", Boolean.toString(isCacheServerConfigured)),
                                    new LogData("remoteCacheAvailable", Boolean.toString(isCacheServerAvailable)),
                                    new LogData("remoteCacheUrl", remoteCacheUrl),
                                    new LogData("remoteCacheRecoverPeriodNotReached", Boolean.toString(isCacheServerRecoverPeriodNotReached)),
                                    new LogData("readinessDownAfter", durationSinceLastSuccessMillis + "ms"));
                            }

                            if ((m_liveDownDurationMillis > 0) && (durationSinceLastSuccessMillis >= m_liveDownDurationMillis)) {
                                ServerManager.logError("DC server shutting down service after READY state went to DOWN due to unavailable remote connection to CacheService",
                                    new LogData("remoteCacheConfigured", Boolean.toString(isCacheServerConfigured)),
                                    new LogData("remoteCacheAvailable", Boolean.toString(isCacheServerAvailable)),
                                    new LogData("remoteCacheRecoverPeriodNotReached", Boolean.toString(isCacheServerRecoverPeriodNotReached)),
                                    new LogData("remoteCacheUrl", remoteCacheUrl),
                                    new LogData("shutdownDownAfter", durationSinceLastSuccessMillis + "ms"));

                                System.exit(1);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                ServerManager.logExcp(e);
            } finally {
                m_probeUpdateRunning.set(false);
            }
        }
    }

    /**
     *
     */
    private void implInitiateDelayedShutdown() {
        implInitiateDelayedShutdown(null);
    }

    /**
     * @param reasonText
     */
    private void implInitiateDelayedShutdown(@Nullable final String reasonText) {
        if (m_delayedShutdownRunning.compareAndSet(false, true)) {
            ServerManager.logWarn("DC server initiated delayed shutdown",
                new LogData("reason", (null != reasonText) ? reasonText : DocumentConverterUtil.STR_NOT_AVAILABLE),
                new LogData("delayTime", m_liveDownDurationMillis + "ms"));

            m_lastReadyProbeSuccessTimestampMillis = System.currentTimeMillis();
        }
    }

    // - Members -------------------------------------------------------------

    final private AtomicBoolean m_running = new AtomicBoolean(true);

    final private AtomicBoolean m_probeUpdateRunning = new AtomicBoolean(false);

    final private AtomicBoolean m_probeIsReady = new AtomicBoolean(true);

    final private AtomicBoolean m_isFirstProbeUpdate = new AtomicBoolean(true);

    final private AtomicBoolean m_delayedShutdownRunning = new AtomicBoolean(false);

    final private ServerManager m_serverManager;

    final private ServerConfig m_serverConfig;

    final private Cache m_cache;

    final private long m_probeUpdatePeriodMillis;

    final private long m_readyDownDurationMillis;

    final private long m_liveDownDurationMillis;

    final private ScheduledExecutorService m_probeTimerExecutor = Executors.newScheduledThreadPool(1);

    private long m_lastReadyProbeSuccessTimestampMillis = System.currentTimeMillis();

    private long m_lastProbeEndTimestampMillis = System.currentTimeMillis();
}
