/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.documentconverter.server.osgi;

import javax.management.ObjectName;
import org.eclipse.microprofile.health.HealthCheck;
import org.osgi.framework.ServiceReference;
import org.osgi.service.http.HttpService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.config.ConfigurationService;
import com.openexchange.documentconverter.HttpHelper;
import com.openexchange.documentconverter.Nullable;
import com.openexchange.documentconverter.impl.CacheServiceClientHealthCheck;
import com.openexchange.documentconverter.impl.DocumentConverterInformation;
import com.openexchange.documentconverter.impl.DocumentConverterMetrics;
import com.openexchange.documentconverter.impl.LTStatistics;
import com.openexchange.documentconverter.impl.ServerConfig;
import com.openexchange.documentconverter.impl.ServerManager;
import com.openexchange.documentconverter.impl.api.IDocumentConverterLongTermInformation;
import com.openexchange.documentconverter.server.impl.DocumentConverterServlet;
import com.openexchange.exception.OXException;
import com.openexchange.management.ManagementService;
import com.openexchange.net.ssl.SSLSocketFactoryProvider;
import com.openexchange.osgi.HousekeepingActivator;
import com.openexchange.osgi.SimpleRegistryListener;

/**
 * {@link DocumentConverterServerActivator}
 *
 * @author <a href="mailto:kai.ahrens@open-xchange.com">Kai Ahrens</a>
 */
public class DocumentConverterServerActivator extends HousekeepingActivator {

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.DeferredActivator#getNeededServices()
     */
    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] {
            ConfigurationService.class,
            HttpService.class,
            SSLSocketFactoryProvider.class
        };
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.DeferredActivator#startBundle()
     */
    @Override
    protected void startBundle() {
        if (LOG.isInfoEnabled()) {
            LOG.info("DC starting bundle: " + SERVICE_NAME);
        }

        // get configuration service
        final ConfigurationService configService = getService(ConfigurationService.class);

        // create HttpHelper singleton with SSL socket factory provider
        final HttpHelper httpHelper = HttpHelper.newInstance(getService(SSLSocketFactoryProvider.class));

        // create ServerManager singleton (might be null)
        final ServerManager serverManager = (null != configService) ?
            ServerManager.newInstance(new ServerConfig(configService, httpHelper.getSSLContext())) :
                null;

        // create register DocumentConverter servlet
        implRegisterHttpServlet(serverManager);

        // register information MBean
        implRegisterInformationMBean(serverManager);

        registerService(HealthCheck.class, new CacheServiceClientHealthCheck(serverManager));

        // set shutdown hook
        Runtime.getRuntime().addShutdownHook(new Thread(() -> implShutdown()));

        // register information bean if/when ManagementService is available
        openTrackers();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.HousekeepingActivator#stopBundle()
     */
    @Override
    public void stopBundle() throws Exception {
        if (LOG.isInfoEnabled()) {
            LOG.info("DC stopping bundle: " + SERVICE_NAME);
        }

        implShutdown();

        final HttpService httpService = getService(HttpService.class);

        if (null != httpService) {
            if (LOG.isInfoEnabled()) {
                LOG.info("DC unregistering HttpServlet");
            }

            httpService.unregister(DocumentConverterServlet.PATH_SERVLET);
        }

        super.stopBundle();

        if (LOG.isInfoEnabled()) {
            LOG.info("DC successfully unregistered bundles");
        }
    }

    // -------------------------------------------------------------------------

    /**
     *
     */
    private void implShutdown() {
        final ServerManager serverManager = ServerManager.get();

        if (null != serverManager) {
            serverManager.shutdown();
        }

        if (null != m_documentConverterServlet) {
            m_documentConverterServlet.shutdown();
        }
    }

    /**
     * implRegisterInformationMBean
     *
     * @param serverManager
     */
    private void implRegisterInformationMBean(@Nullable final ServerManager serverManager) {
        try {
            final String className = DocumentConverterInformation.class.getName();
            final int pos = className.lastIndexOf('.');
            final ObjectName dcMBeanObjectName =
                new ObjectName("com.openexchange.documentconverter", "name", (-1 == pos) ? className : className.substring(pos + 1));
            final LTStatistics statistics = (null != serverManager) ? serverManager.getStatistics() : null;

            if ((null != serverManager) && (null != statistics)) {
                final DocumentConverterInformation dcMBean = new DocumentConverterInformation(statistics, IDocumentConverterLongTermInformation.class);

                (new DocumentConverterMetrics(dcMBean)).registerMetrics();
                serverManager.setMBean(dcMBean);

                track(ManagementService.class, new SimpleRegistryListener<ManagementService>() {
                    /**
                     *
                     */
                    @Override
                    public void added(ServiceReference<ManagementService> ref, ManagementService service) {
                        try {
                            service.registerMBean(dcMBeanObjectName, dcMBean);
                        } catch (final OXException e) {
                            LOG.error(e.getMessage());
                        }
                    }

                    /**
                     *
                     */
                    @Override
                    public void removed(ServiceReference<ManagementService> ref, ManagementService service) {
                        try {
                            service.unregisterMBean(dcMBeanObjectName);
                        } catch (final OXException e) {
                            LOG.error(e.getMessage());
                        }
                    }
                });
            }
        } catch (final Exception e) {
            LOG.error(e.getMessage());
        }
    }

    /**
     * implRegisterHttpServlet
     *
     * @param serverManager
     */
    private void implRegisterHttpServlet(@Nullable final ServerManager serverManager) {
        final HttpService httpService = getService(HttpService.class);

        if (LOG.isInfoEnabled()) {
            LOG.info("DC starting bundle to register HttpServlet");
        }

        if ((null != serverManager) && (null != httpService)) {
            try {
                m_documentConverterServlet = new DocumentConverterServlet(serverManager, serverManager.getCache());

                httpService.registerServlet(DocumentConverterServlet.PATH_SERVLET, m_documentConverterServlet, null, null);

                if (LOG.isInfoEnabled()) {
                    LOG.info("DC successfully registered HttpServlet at HttpService");
                }
            } catch (final Exception e) {
                if (LOG.isErrorEnabled()) {
                    LOG.info("DC unable to register HttpServlet: " + e.getMessage());
                }
            }
        } else if (LOG.isErrorEnabled()) {
            LOG.error("DC unable to register HttpServlet due to unavailability of DC ServerManager or HttpService");
        }
    }

    // - Static Members --------------------------------------------------------

    final private static Logger LOG = LoggerFactory.getLogger(ServerManager.class);

    final private static String SERVICE_NAME = "com.openexchange.documentconverter";

    // - Members ---------------------------------------------------------------

    private volatile DocumentConverterServlet m_documentConverterServlet = null;
}
