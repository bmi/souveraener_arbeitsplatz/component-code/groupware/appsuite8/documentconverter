{{- define "core-documentconverter.ingressPathMappings" -}}
paths:
  - pathType: Prefix
    path: /documentconverterws
    targetPath: /documentconverterws/
    targetPort:
      name: http
  - pathType: Exact
    path: /documentconverterws-metrics
    targetPath: /metrics
    targetPort:
      name: http
{{- end -}}
