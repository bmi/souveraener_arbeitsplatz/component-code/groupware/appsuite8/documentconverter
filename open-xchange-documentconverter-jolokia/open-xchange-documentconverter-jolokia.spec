Name:          open-xchange-documentconverter-jolokia
BuildArch:     noarch
BuildRequires: java-1.8.0-openjdk-devel
BuildRequires: ant
Version:       @OXVERSION@
%define        ox_release 3
Release:       %{ox_release}_<CI_CNT>.<B_CNT>
Group:         Applications/Productivity
License:       GNU General Public License (GPL)
BuildRoot:     %{_tmppath}/%{name}-%{version}-build
URL:           http://www.open-xchange.com/
Source:        %{name}_%{version}.orig.tar.bz2
Summary:       Open-Xchange DocumentConverter Munin scripts
Requires:      open-xchange-documentconverter-server >= @OXVERSION@
Requires:      munin-node, perl-JSON, perl-libwww-perl

%description
Munin is a highly flexible and powerful solution used to create graphs of
virtually everything imaginable throughout your network, while still
maintaining a rattling ease of installation and configuration.

This package contains Open-Xchange DocumentConverter plugins for the Munin node.

Munin is written in Perl, and relies heavily on Tobi Oetiker's excellent
RRDtool. To see a real example of Munin in action, you can follow a link
from <http://munin.projects.linpro.no/> to a live installation.

Authors:
--------
    Open-Xchange

%prep
%setup -q

%build

%install
export NO_BRP_CHECK_BYTECODE_VERSION=true
ant -lib build/lib -Dbasedir=build -DdestDir=%{buildroot} -DpackageName=%{name} -f build/build.xml clean build

%post
TMPFILE=`mktemp /tmp/munin-node.configure.XXXXXXXXXX`
munin-node-configure --libdir /usr/share/munin/plugins/ --shell > $TMPFILE || :
if [ -f $TMPFILE ] ; then
  sh < $TMPFILE
  rm -f $TMPFILE
fi
/etc/init.d/munin-node restart || :







exit 0

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root)
%dir /usr/share/munin
/usr/share/munin/plugins/

%changelog
* Thu Nov 28 2019 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candidate of 7.10.3 release
* Thu Nov 21 2019 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate of 7.10.3 release
* Thu Oct 17 2019 Kai Ahrens <kai.ahrens@open-xchange.com>
First preview of 7.10.3 release
* Tue Jun 18 2019 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.10.3 release
* Sat May 11 2019 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candidate of 7.10.2 release
* Fri May 10 2019 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate of 7.10.2 release
* Thu May 02 2019 Kai Ahrens <kai.ahrens@open-xchange.com>
Second preview of 7.10.2 release
* Thu Mar 28 2019 Kai Ahrens <kai.ahrens@open-xchange.com>
First preview of 7.10.2 release
* Mon Mar 11 2019 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.10.2
* Fri Nov 23 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
RC 1 for 7.10.1 release
* Fri Nov 02 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
Second preview for 7.10.1 release
* Thu Oct 11 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.10.1 release
* Mon Sep 10 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.10.1
* Tue Jun 26 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candidate for 7.10.0 release
* Mon Jun 11 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.10.0 release
* Fri May 18 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
Sixth preview of 7.10.0 release
* Thu Apr 19 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
Fifth preview of 7.10.0 release
* Tue Apr 03 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
Fourth preview of 7.10.0 release
* Tue Feb 20 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
Third preview of 7.10.0 release
* Fri Feb 02 2018 Kai Ahrens <kai.ahrens@open-xchange.com>
Second preview of 7.10.0 release
* Wed Dec 06 2017 Kai Ahrens <kai.ahrens@open-xchange.com>
First preview for 7.10.0 release
* Mon Oct 16 2017 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.10.0 release
* Fri May 19 2017 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.8.4 release
* Thu May 04 2017 Kai Ahrens <kai.ahrens@open-xchange.com>
Second preview of 7.8.4 release
* Mon Apr 03 2017 Kai Ahrens <kai.ahrens@open-xchange.com>
First preview of 7.8.4 release
* Fri Dec 02 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.8.4 release
* Fri Nov 25 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
Second release candidate for 7.8.3 release
* Thu Nov 24 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
First release candidate for 7.8.3 release
* Tue Nov 15 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
Third preview for 7.8.3 release
* Sat Oct 29 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
Second preview for 7.8.3 release
* Fri Oct 14 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
First preview of 7.8.3 release
* Tue Sep 06 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.8.3 release
* Tue Jul 12 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candidate for 7.8.2 release
* Wed Jul 06 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.8.2 release
* Wed Jun 29 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candidate for 7.8.2 release
* Wed Jun 15 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.8.2 release
* Thu Apr 07 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.8.2 release
* Wed Mar 30 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candiate for 7.8.1 release
* Thu Mar 24 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
First candiate for 7.8.1 release
* Tue Mar 15 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
Fifth preview of 7.8.1 release
* Fri Mar 04 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
Fourth preview of 7.8.1 release
* Sat Feb 20 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
Third candidate for 7.8.1 release
* Tue Feb 02 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candidate for 7.8.1 release
* Tue Jan 26 2016 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.8.1 release
* Fri Oct 09 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.8.1 release
* Fri Oct 02 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Sixth candidate for 7.8.0 release
* Fri Sep 25 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Fifth candidate for 7.8.0 release
* Fri Sep 18 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Fourth candidate for 7.8.0 release
* Mon Sep 07 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Third candidate for 7.8.0 release
* Fri Aug 21 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candidate for 7.8.0 release
* Thu Aug 06 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.8.0 release
* Wed Jul 29 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Build for patch 2015-08-06 (2661)
* Mon Mar 16 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.8.0 release
* Mon Mar 09 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Twelfth candidate for 7.6.2 release
* Fri Mar 06 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Eleventh candidate for 7.6.2 release
* Wed Mar 04 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Tenth candidate for 7.6.2 release
* Tue Mar 03 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Ninth candidate for 7.6.2 release
* Tue Feb 24 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Eighth candidate for 7.6.2 release
* Wed Feb 11 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.8.0 release
* Fri Jan 30 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Sixth candidate for 7.6.2 release
* Tue Jan 27 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Fifth candidate for 7.6.2 release
* Mon Jan 05 2015 Kai Ahrens <kai.ahrens@open-xchange.com>
Build for patch 2015-01-12
* Fri Dec 12 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Fourth candidate for 7.6.2 release
* Fri Dec 05 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Third candidate for 7.6.2 release
* Fri Nov 21 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candidate for 7.6.2 release
* Mon Nov 10 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Build for patch 2014-11-17
* Fri Oct 31 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.6.2 release
* Tue Oct 14 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Fifth candidate for 7.6.1 release
* Mon Oct 13 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.6.2 release
* Fri Oct 10 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Fourth candidate for 7.6.1 release
* Thu Oct 02 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Third candidate for 7.6.1 release
* Tue Sep 16 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candidate for 7.6.1 release
* Mon Sep 08 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.6.1 release
* Tue Aug 26 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Build for patch 2014-08-25
* Tue Aug 26 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Build for patch 2014-08-25
* Wed Jul 23 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Build for patch 2014-07-30
* Mon Jul 21 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.6.1
* Thu Jul 17 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Build for patch 2014-07-21
* Mon Jun 23 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Seventh candidate for 7.6.0 release
* Fri Jun 20 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Sixth candidate for 7.6.0 release
* Fri Jun 13 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Fifth candidate for 7.6.0 release
* Fri May 30 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Fourth candidate for 7.6.0 release
* Fri May 16 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Third candidate for 7.6.0 release
* Fri May 02 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Build for patch 2014-05-05
* Wed Mar 26 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.4.2 Spreadsheet Final release
* Wed Feb 26 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.6.0 spreadsheet release
* Wed Feb 26 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.4.2 spreadsheet release
* Fri Feb 07 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Sixth candidate for 7.4.2 release
* Thu Feb 06 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Fifth candidate for 7.4.2 release
* Tue Feb 04 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Fourth candidate for 7.4.2 release
* Thu Jan 23 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Third candidate for 7.4.2 release
* Fri Jan 10 2014 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candidate for 7.4.2 release
* Mon Dec 23 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
First candidate for 7.4.2 release
* Thu Dec 19 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.4.2
* Wed Nov 20 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
Fifth candidate for 7.4.1 release
* Fri Nov 15 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
Fourth candidate for 7.4.1 release
* Thu Nov 07 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
Third candidate for 7.4.1 release
* Wed Oct 23 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
Second candidate for 7.4.1 release
* Fri Oct 18 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
prepare for 7.4.1 release
* Thu Oct 10 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
First sprint increment for 7.4.1 release
* Tue Sep 24 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
Eleventh candidate for 7.4.0 release
* Fri Sep 20 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
Tenth candidate for 7.4.0 release
* Thu Sep 12 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
Ninth candidate for 7.4.0 release
* Sun Sep 01 2013 Kai Ahrens <kai.ahrens@open-xchange.com>
Eighth candidate for 7.4.0 release
* Wed Dec 19 2012 Kai Ahrens <kai.ahrens@open-xchange.com>
Seventh candidate for 7.4.0 release
