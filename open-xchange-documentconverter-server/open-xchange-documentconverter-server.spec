Name:          open-xchange-documentconverter-server
#!BuildIgnore: post-build-checks
BuildRequires: java-1.8.0-openjdk-devel
BuildRequires: ant
BuildRequires: open-xchange-grizzly >= @OXVERSION@
BuildRequires: open-xchange-documentconverter-api >= @OXVERSION@
Version:       @OXVERSION@
%define        ox_release 3
Release:       %{ox_release}_<CI_CNT>.<B_CNT>
Group:         Applications/Productivity
License:       GPL-2.0
BuildRoot:     %{_tmppath}/%{name}-%{version}-build
URL:           http://www.open-xchange.com/
Source:        %{name}_%{version}.orig.tar.bz2
Source2:       open-xchange-documentconverter-server.service
%define        dropin_dir /etc/systemd/system/open-xchange-documentconverter-server.service.d
%define        dropin_example limits.conf
Summary:       The Open-Xchange backend webservice for office document conversion
AutoReqProv:   no
Requires:      coreutils
Requires:      open-xchange-grizzly >= @OXVERSION@
Requires:      open-xchange-documentconverter-api >= @OXVERSION@
Requires:      open-xchange-admin >= @OXVERSION@
Requires:      open-xchange-pdftool
Requires:      readerengine
Provides:      open-xchange-documentconverter-webservice = %{version}
Obsoletes:     open-xchange-documentconverter-webservice <= %{version}
Requires(pre):    systemd
Requires(post):   systemd
Requires(preun):  systemd
Requires(postun): systemd

%description
This package contains the backend components for the documentconverter web service

Authors:
--------
    Open-Xchange

%prep
%setup -q

%build

%install
export NO_BRP_CHECK_BYTECODE_VERSION=true
ant -lib build/lib -Dbasedir=build -DdestDir=%{buildroot} -DpackageName=%{name} -f build/build.xml clean build
mkdir -p %{buildroot}/var/log/open-xchange/documentconverter
mkdir -p %{buildroot}/var/spool/open-xchange/documentconverter
%__install -D -m 444 %{SOURCE2} %{buildroot}/usr/lib/systemd/system/open-xchange-documentconverter-server.service

# On Redhat and SuSE start scripts are not automatically added to system start. This is wanted behavior and standard.

%post
if [ ! -f %{dropin_dir}/%{dropin_example} ]
then
  install -D -m 644 %{_defaultdocdir}/%{name}-%{version}/%{dropin_example} %{dropin_dir}/%{dropin_example}
fi

drop_in=%{dropin_dir}/%{dropin_example}
if [ -f ${drop_in} ] && grep -q "^#LimitNOFILE=16384$" ${drop_in}
then
  sed -i 's/^#LimitNOFILE=16384$/#LimitNOFILE=65536/' ${drop_in}
fi

drop_in=%{dropin_dir}/%{dropin_example}
if [ -f ${drop_in} ] && ! grep -q LimitNPROC ${drop_in}
then
  sed -i '/^\[Service\]$/a #LimitNPROC=65536' ${drop_in}
fi

# Trigger a service definition/config reload
systemctl daemon-reload &> /dev/null || :

if [ ${1:-0} -eq 2 ]; then
    # only when updating
    . /opt/open-xchange/lib/oxfunctions.sh

    # prevent bash from expanding, see bug 13316
    GLOBIGNORE='*'

    # SCR-188
    pfile=/opt/open-xchange/documentconverter/etc/readerengine.blacklist
    in=$(quote_s_in "file://.*")
    re=$(quote_s_re ".*")
    if grep -q ${in} ${pfile}; then
      sed -i -e "s/^${in}$/${re}/" ${pfile}
    fi

    SCR=SCR-596
    if ox_scr_todo ${SCR}
    then
      pfile=/opt/open-xchange/documentconverter/etc/overwrite.properties
      pkey=com.openexchange.hazelcast.enabled
      ox_add_property ${pkey} false ${pfile}
      ox_scr_done ${SCR}
    fi
fi

%preun

%postun

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root)
%dir /opt/open-xchange/documentconverter/bundles/
/opt/open-xchange/documentconverter/bundles/*
%dir %attr(750, open-xchange, root) /opt/open-xchange/documentconverter/osgi/
/opt/open-xchange/documentconverter/osgi/config.ini.template
%dir /opt/open-xchange/documentconverter/osgi/bundle.d/
/opt/open-xchange/documentconverter/osgi/bundle.d/*
%dir /opt/open-xchange/documentconverter/lib/
/opt/open-xchange/documentconverter/lib/*
%dir /opt/open-xchange/documentconverter/etc/
%config(noreplace) /opt/open-xchange/documentconverter/etc/*
%dir /opt/open-xchange/documentconverter/sbin/
/opt/open-xchange/documentconverter/sbin/open-xchange-documentconverter
%dir %attr(750, open-xchange, root) /var/log/open-xchange/documentconverter
%dir %attr(750, open-xchange, root) /var/spool/open-xchange/documentconverter
/usr/lib/systemd/system/open-xchange-documentconverter-server.service
%doc docs/%{dropin_example}

%changelog
* Thu Nov 28 2019 Marcus Klein <marcus.klein@open-xchange.com>
Second candidate of 7.10.3 release
* Thu Nov 21 2019 Marcus Klein <marcus.klein@open-xchange.com>
First candidate of 7.10.3 release
* Thu Oct 17 2019 Marcus Klein <marcus.klein@open-xchange.com>
First preview of 7.10.3 release
* Tue Jun 18 2019 Marcus Klein <marcus.klein@open-xchange.com>
prepare for 7.10.3 release
* Sat May 11 2019 Marcus Klein <marcus.klein@open-xchange.com>
Second candidate of 7.10.2 release
* Fri May 10 2019 Marcus Klein <marcus.klein@open-xchange.com>
First candidate of 7.10.2 release
* Thu May 02 2019 Marcus Klein <marcus.klein@open-xchange.com>
Second preview of 7.10.2 release
* Thu Mar 28 2019 Marcus Klein <marcus.klein@open-xchange.com>
First preview of 7.10.2 release
* Mon Mar 11 2019 Marcus Klein <marcus.klein@open-xchange.com>
prepare for 7.10.2
* Fri Nov 23 2018 Marcus Klein <marcus.klein@open-xchange.com>
RC 1 for 7.10.1 release
* Fri Nov 02 2018 Marcus Klein <marcus.klein@open-xchange.com>
Second preview for 7.10.1 release
* Thu Oct 11 2018 Marcus Klein <marcus.klein@open-xchange.com>
First candidate for 7.10.1 release
* Mon Sep 10 2018 Marcus Klein <marcus.klein@open-xchange.com>
prepare for 7.10.1
* Tue Jun 26 2018 Marcus Klein <marcus.klein@open-xchange.com>
Second candidate for 7.10.0 release
* Mon Jun 11 2018 Marcus Klein <marcus.klein@open-xchange.com>
First candidate for 7.10.0 release
* Fri May 18 2018 Marcus Klein <marcus.klein@open-xchange.com>
Sixth preview of 7.10.0 release
* Thu Apr 19 2018 Marcus Klein <marcus.klein@open-xchange.com>
Fifth preview of 7.10.0 release
* Tue Apr 03 2018 Marcus Klein <marcus.klein@open-xchange.com>
Fourth preview of 7.10.0 release
* Tue Feb 20 2018 Marcus Klein <marcus.klein@open-xchange.com>
Third preview of 7.10.0 release
* Fri Feb 02 2018 Marcus Klein <marcus.klein@open-xchange.com>
Second preview of 7.10.0 release
* Wed Dec 06 2017 Marcus Klein <marcus.klein@open-xchange.com>
First preview for 7.10.0 release
* Mon Oct 16 2017 Marcus Klein <marcus.klein@open-xchange.com>
prepare for 7.10.0 release
* Fri May 19 2017 Marcus Klein <marcus.klein@open-xchange.com>
First candidate for 7.8.4 release
* Thu May 04 2017 Marcus Klein <marcus.klein@open-xchange.com>
Second preview of 7.8.4 release
* Mon Apr 03 2017 Marcus Klein <marcus.klein@open-xchange.com>
First preview of 7.8.4 release
* Fri Dec 02 2016 Marcus Klein <marcus.klein@open-xchange.com>
prepare for 7.8.4 release
* Fri Nov 25 2016 Marcus Klein <marcus.klein@open-xchange.com>
Second release candidate for 7.8.3 release
* Thu Nov 24 2016 Marcus Klein <marcus.klein@open-xchange.com>
First release candidate for 7.8.3 release
* Tue Nov 15 2016 Marcus Klein <marcus.klein@open-xchange.com>
Third preview for 7.8.3 release
* Sat Oct 29 2016 Marcus Klein <marcus.klein@open-xchange.com>
Second preview for 7.8.3 release
* Fri Oct 14 2016 Marcus Klein <marcus.klein@open-xchange.com>
First preview of 7.8.3 release
* Tue Sep 06 2016 Marcus Klein <marcus.klein@open-xchange.com>
prepare for 7.8.3 release
* Tue Jul 12 2016 Marcus Klein <marcus.klein@open-xchange.com>
Second candidate for 7.8.2 release
* Wed Jul 06 2016 Marcus Klein <marcus.klein@open-xchange.com>
First candidate for 7.8.2 release
* Wed Jun 29 2016 Marcus Klein <marcus.klein@open-xchange.com>
Second candidate for 7.8.2 release
* Wed Jun 15 2016 Marcus Klein <marcus.klein@open-xchange.com>
First candidate for 7.8.2 release
* Wed May 11 2016 Marcus Klein <marcus.klein@open-xchange.com>
initial packaging for open-xchange-documentconverter-server
